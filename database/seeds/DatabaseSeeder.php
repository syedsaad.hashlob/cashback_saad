<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void\DatabaseSeeder.php
     */
    public function run()
    {
        $this->call(AdminsTableSeeder::class);
    }
}
