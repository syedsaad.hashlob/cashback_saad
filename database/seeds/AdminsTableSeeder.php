<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert(array(
        	array(
                'name' => "admin",
                'email' => 'admin@admin.com',
                'password' => bcrypt('karachi123'),
        	)
        ));

        DB::table('users')->insert(array(
        	array(
                'name' => "saad",
                'email' => 'saad@gmail.com',
                'user_account_id' => '16185971',
                'password' => bcrypt('admin@0101'),
        	)
        ));
    }
}
