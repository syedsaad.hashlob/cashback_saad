<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTraderTradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trader_trades', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->string('Trade');
            $table->string('MT4_MT5_ID');
            $table->string('Account_Type');
            $table->string('Account_Currency');
            $table->string('Campaign');
            $table->string('Open_Time');
            $table->string('Close_Time');
            $table->string('Trade_Category');
            $table->string('Trade_Type');
            $table->string('Instrument');
            $table->string('Instrument_Group');
            $table->string('Lots');
            $table->string('Total_Comm');
            $table->string('Affiliate_Comm');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trader_trades');
    }
}
