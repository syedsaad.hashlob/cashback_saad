<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLotRebatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lot_rebates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->string('Period');
            $table->string('Clicks');
            $table->string('sub_aff_reg');
            $table->string('Real_Account');
            $table->string('NDAs');
            $table->string('NCR');
            $table->string('UNDC');
            $table->string('TNCR');
            $table->string('TNDC');
            $table->string('Conversion');
            $table->string('APC_Rate');
            $table->string('Active_Traders');
            $table->string('Lots');
            $table->string('Lot_Rebate');
            $table->string('Sub_Aff_Comm');
            $table->string('Adjustments');
            $table->string('Total_Comm');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lot_rebates');
    }
}
