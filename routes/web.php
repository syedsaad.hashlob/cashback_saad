<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/binary', 'BinaryController@index')->name('binary');

Route::get('/broker/{broker}', 'BrokerWebController@index')->name('web_broker');

Route::get('/contact', 'ContactController@index')->name('contact');

Route::get('/support', 'SupportController@index')->name('support');

Route::get('/features', 'FeaturesController@index')->name('features');

Route::get('/how-its-work', 'How_its_workController@index')->name('how-its-work');

Route::get('/terms-and-condition', 'Terms_conditionController@index')->name('terms');

Route::get('/privacy-policy', 'PrivacyController@index')->name('privacy');

Route::get('/mainpage', 'MainpageController@index')->name('mainpage');
Route::post('/withdraw-request/store', 'MainpageController@transactionStore')->name('transaction.store');
Route::post('/withdraw-request', 'MainpageController@withDrawRequestStatus')->name('withdraw_request_status');
Route::get('/', 'WelcomeController@index')->name('Homepage');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth'])->group(function () {
    Route::get('/myaccount', ('AccountController@index'))->name('my_account');
    Route::post('/update-broker-id', ('AccountController@updateBrokerAccountId'))->name('update_broker_account');
    Route::get('/refered_user', ('ReferedUserController@index'))->name('refered_user');
    Route::get('/withdrawl', ('WithdrawlController@index'))->name('withdrawl');
    Route::get('/leaderboard', ('LeaderboardController@index'))->name('leaderboard');
    Route::post('/popup-status-update', ('AccountController@popUpStatusUpdate'))->name('popup_update_status');
});

Route::namespace("Admin")->prefix('admin')->group(function(){

    Route::resource('/broker', BrokerController::class);
    Route::resource('/payment', PaymentController::class);
    Route::get('/trade', 'Trade@index')->name('Trade.index');
    Route::get('/verify-booker-user', 'BrokerController@verifyBooker')->name('verify.user.booker');
    Route::get('/verify-booker-user-status/{user_account}', 'BrokerController@approvedBookerAccount')->name('verify.user.booker.update');
    Route::post('/trade', 'Trade@store')->name('Trade.store');
	Route::get('/', 'HomeController@index')->name('admin.home');
    Route::get('/users', 'UserController@index')->name('user.index');
    Route::get('/user/{user}/action-history', 'UserController@actionHistory')->name('user_action.history');
    Route::get('/user/{user}/export', 'UserController@actionHistoryExport')->name('user_action.history_export');
    Route::get('/user/{user}/withdraw/export', 'UserController@withdrawHistoryExport')->name('user_action.withdraw_export');
    Route::get('/user/{user}/status', 'UserController@status')->name('user.status');
    Route::get('/user/{user}/delete', 'UserController@delete')->name('user.delete');
    Route::get('/user/{user}/transactiondelete', 'UserController@transactiondelete')->name('user.transactiondelete');
    Route::get('/user/{user}/referal', 'UserController@referal')->name('user.referals');
    Route::get('/user/{user}/summary', 'UserController@summary')->name('user.summary');
	Route::get('/withdraw-request', 'HomeController@transaction')->name('transaction.index');
    Route::get('/setting', 'HomeController@Setting')->name('settings');
    Route::post('/setting/update', 'HomeController@settingUpdate')->name('settings.update');
    Route::get('/activity-logs', 'UserController@activityLogs')->name('activity-logs');
    Route::get('/user-summary-report', 'UserController@userSummaryReport')->name('user-report-account');
    Route::get('/user-site-summary-report', 'UserController@withUserSiteSummaryExport')->name('user-site-summary-report');
    Route::resource('/accounts', AccountController::class);
    Route::get('monthly-level-down', 'UserController@monthlyLevelDown')->name('monthly-level-down');

    Route::namespace('Auth')->group(function(){
        Route::get('/login', 'LoginController@showLoginForm')->name('admin.login');
        Route::post('/login', 'LoginController@login');
        Route::post('logout', 'LoginController@logout')->name('admin.logout');
    });
});

Route::get('referal/{account_id}/register', 'ReferedUserController@referredView')->name('user_referred');

Route::get('/email', function () {
    $data = [
        'name' => 'Shehryar',    
        'request_amount' => 125.50,    
        'date' => \Carbon\Carbon::now()->format('Y-m-d h:i a'),    
    ];
    Mail::send('email/withdrawl-request-email', ['data'=>$data ], function ($message) use ($data) {
        $message->to('shehryarhaider0316@gmail.com')
            ->from('info@fxcashbacks.com','FXCASHBACKS')
            ->subject("Welcome to FxCashbacks");
    });
    return "DOne";
});
Route::get('reset', function (){
    Artisan::call('route:clear');
    Artisan::call('view:clear');
    // Artisan::call('route:cache');
    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    Artisan::call('config:cache');
    // Artisan::call('optimize'x);
    echo "cache Clear";
});
