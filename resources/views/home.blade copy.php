
<link href="{{ url('')}}/assets/img/favicon.png" rel="icon">
<link href="{{ url('')}}/assets/img/apple-touch-icon.png" rel="apple-touch-icon">
@extends('layouts.dashboard')
@section('content')


<section style="padding-top:50px">



    <div class="container">

        <div class="row">

            <div class="col-md-6 profile_box mr_box">

                <div class="scrollbar" id="style-2">
                    <div class="force-overflow">

                    <div class="">
                        <p class="profile_box_p_ra">Your Profile</p>
                        <p class="balance_hed">Balance</p>
                        <p class="amount_pro">  ${{$trade_cont_value}}  </p>
                        <p class="pendind_with_draw">Pending withdrawal: ${{$pending_withdraw}}</p>
                    <div class="seprater_profile_box">
                    </div>

                    @foreach($Trader_trades as $Trader_trade)

                    <div class="row pd_top_for_detail">

                        <div class="col-md-8 ">
                            <p class="details_profile"> Cashback on {{ $Trader_trade->broker->name}} account {{ $Trader_trade->MT4_MT5_ID}} at {{$Trader_trade->date}} </p>

                        </div>
                        <div class="col-md-1"><img src="images/men.png"></div>
                        <div class="col-md-3">
                            <p class="remaining_amount">
                                ${{ $Trader_trade->Total_Comm}}</p>


                        </div>
                    </div>
                    @endforeach
                </div>

            </div>
        </div>

            </div>



                <div class="col-md-6">
                <div class="profile_box">
                    <div class="row">
                        <div class="col-md-6">
                            <p class="profile_box_p_ra">% Your cashback rate</p>
                        </div>
                        <div class="col-md-6">
                            <p style="text-align: left;

                            letter-spacing: 0px;
                            color: #096DA7;
                            opacity: 1;">Boost your cashback rate</p>
                        </div>
                    </div>
                    <p class="balance_hed">Cashback rate</p>
                    <p class="amount_pro">
                        @if(Auth::check())
                            @if (currentPoints(Auth::user()->id) < getScoringPoints('bronze')['to'])
                                {{getScoringPercentage('percentage_bronze')}}%

                            @elseif(currentPoints(Auth::user()->id) > getScoringPoints('bronze')['to'] && currentPoints(Auth::user()->id) < getScoringPoints('silver')['to'])
                                {{getScoringPercentage('percentage_silver')}}%

                            @elseif(currentPoints(Auth::user()->id) > getScoringPoints('silver')['to'] && currentPoints(Auth::user()->id) < getScoringPoints('gold')['to'])
                                {{getScoringPercentage('percentage_gold')}}%

                            @elseif(currentPoints(Auth::user()->id) > getScoringPoints('gold')['to'] && currentPoints(Auth::user()->id) < getScoringPoints('platinum')['to'])
                                {{getScoringPercentage('percentage_platinum')}}%

                            @elseif(currentPoints(Auth::user()->id) > getScoringPoints('platinum')['to'] && currentPoints(Auth::user()->id) < getScoringPoints('diamond')['to'])
                                {{getScoringPercentage('percentage_diamond')}}%
                            @else
                                {{getScoringPercentage('percentage_emerald')}}%
                            @endif
                        @endif
                        @php
                        $trading_amount         = \App\Trader_trades::where([['user_id',Auth::user()->id]])->pluck('Total_Comm')->toArray();   
                        $user_total_earnings    = array_sum($trading_amount);
                        
                        if(intval($user_total_earnings) >= 50){
                            $points = intval($user_total_earnings) / 50;
                            if(intval($user_total_earnings) == 50){
                                $points = 1;
                            }
                        }else{
                            $points = 0;
                        }   
                        @endphp
                        <p>Current Points: <b>{{$points}}</b></p>
                    </p>

                    <div class="seprater_profile_box">
                    </div>
                    <div class="">
                        <p>Cashback rate from your level</p>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p style="color:#096DA7;">
                                {{-- {{dd(currentPoints(Auth::user()->id))}} --}}
                            @if(Auth::check())
                                @if (currentPoints(Auth::user()->id) < getScoringPoints('bronze')['to'])
                                    Bronze
    
                                @elseif(currentPoints(Auth::user()->id) > getScoringPoints('bronze')['to'] && currentPoints(Auth::user()->id) < getScoringPoints('silver')['to'])
                                    Silver
    
                                @elseif(currentPoints(Auth::user()->id) > getScoringPoints('silver')['to'] && currentPoints(Auth::user()->id) < getScoringPoints('gold')['to'])
                                    Gold
    
                                @elseif(currentPoints(Auth::user()->id) > getScoringPoints('gold')['to'] && currentPoints(Auth::user()->id) < getScoringPoints('platinum')['to'])
                                    Platinum
    
                                @elseif(currentPoints(Auth::user()->id) > getScoringPoints('platinum')['to'] && currentPoints(Auth::user()->id) < getScoringPoints('diamond')['to'])
                                    Diamond
                                @else
                                    Emerald
                                @endif
                            @endif

                            </p>
                        </div>
                        <div class="col-md-3">

                        </div>
                        <div class="col-md-3">
                            <p>
                                @if(Auth::check())
                                    @if (currentPoints(Auth::user()->id) < getScoringPoints('bronze')['to'])
                                        {{-- {{dd(getScoringPoints('bronze')['to']) }} --}}
                                        {{round(currentPoints(Auth::user()->id) * 100 / getScoringPoints('bronze')['to'])}}%

                                    @elseif(currentPoints(Auth::user()->id) > getScoringPoints('bronze')['to'] && currentPoints(Auth::user()->id) < getScoringPoints('silver')['to'])
                                        {{-- {{dd(getScoringPoints('silver')['to']) }} --}}
                                        {{round(currentPoints(Auth::user()->id) * 100 / getScoringPoints('silver')['to'])}}%

                                    @elseif(currentPoints(Auth::user()->id) > getScoringPoints('silver')['to'] && currentPoints(Auth::user()->id) < getScoringPoints('gold')['to'])
                                        {{-- {{dd(getScoringPoints('gold')['to']) }} --}}
                                        {{round(currentPoints(Auth::user()->id) * 100 / getScoringPoints('gold')['to'])}}%

                                    @elseif(currentPoints(Auth::user()->id) > getScoringPoints('gold')['to'] && currentPoints(Auth::user()->id) < getScoringPoints('platinum')['to'])
                                        {{-- {{dd(getScoringPoints('platinum')['to']) }} --}}
                                        {{round(currentPoints(Auth::user()->id) * 100 / getScoringPoints('platinum')['to'])}}%

                                    @elseif(currentPoints(Auth::user()->id) > getScoringPoints('platinum')['to'] && currentPoints(Auth::user()->id) < getScoringPoints('diamond')['to'])
                                        {{-- {{dd(getScoringPoints('diamond')['to']) }} --}}
                                        {{round(currentPoints(Auth::user()->id) * 100 / getScoringPoints('diamond')['to'])}}%
                                    @else
                                        {{-- {{dd(getScoringPoints('emerald')['from']) }} --}}
                                        {{round(currentPoints(Auth::user()->id) * 100 / getScoringPoints('emerald')['from'])}}%
                                    @endif
                                @endif
                                {{-- 80% / 85% --}}
                            </p>
                        </div>
                    </div>

                    <div class="progress">
                        {{-- {{dd(getScoringPoints('silver')['to'])}} --}}
                        {{-- <div class="progress-bar" role="progressbar" aria-valuenow="@if(Auth::check()) @if (currentPoints(Auth::user()->id) < getScoringPoints('bronze')['to']) {{currentPoints(Auth::user()->id) * 100 / getScoringPoints('bronze')['to']}}% @elseif(currentPoints(Auth::user()->id) > getScoringPoints('bronze')['to'] && currentPoints(Auth::user()->id) < getScoringPoints('silver')['to']) {{currentPoints(Auth::user()->id) * 100 / getScoringPoints('silver')['to']}}% @elseif(currentPoints(Auth::user()->id) > getScoringPoints('silver')['to'] && currentPoints(Auth::user()->id) < getScoringPoints('gold')['to']) {{currentPoints(Auth::user()->id) * 100 / getScoringPoints('gold')['to']}}% @elseif(currentPoints(Auth::user()->id) > getScoringPoints('gold')['to'] && currentPoints(Auth::user()->id) < getScoringPoints('platinum')['to']) {{currentPoints(Auth::user()->id) * 100 / getScoringPoints('platinum')['to']}}% @elseif(currentPoints(Auth::user()->id) > getScoringPoints('platinum')['to'] && currentPoints(Auth::user()->id) < getScoringPoints('diamond')['to']) {{currentPoints(Auth::user()->id) * 100 / getScoringPoints('diamond')['to']}}% @else {{currentPoints(Auth::user()->id) * 100 / getScoringPoints('emerald')['to']}}% @endif @endif" aria-valuemin="0" aria-valuemax="100" style="width:@if(Auth::check()) @if (currentPoints(Auth::user()->id) < getScoringPoints('bronze')['to']) {{currentPoints(Auth::user()->id) * 100 / getScoringPoints('bronze')['to']}}% @elseif(currentPoints(Auth::user()->id) > getScoringPoints('bronze')['to'] && currentPoints(Auth::user()->id) < getScoringPoints('silver')['to']) {{currentPoints(Auth::user()->id) * 100 / getScoringPoints('silver')['to']}}% @elseif(currentPoints(Auth::user()->id) > getScoringPoints('silver')['to'] && currentPoints(Auth::user()->id) < getScoringPoints('gold')['to']) {{currentPoints(Auth::user()->id) * 100 / getScoringPoints('gold')['to']}}% @elseif(currentPoints(Auth::user()->id) > getScoringPoints('gold')['to'] && currentPoints(Auth::user()->id) < getScoringPoints('platinum')['to']) {{currentPoints(Auth::user()->id) * 100 / getScoringPoints('platinum')['to']}}% @elseif(currentPoints(Auth::user()->id) > getScoringPoints('platinum')['to'] && currentPoints(Auth::user()->id) < getScoringPoints('diamond')['to']) {{currentPoints(Auth::user()->id) * 100 / getScoringPoints('diamond')['to']}}% @else {{currentPoints(Auth::user()->id) * 100 / getScoringPoints('emerald')['to']}}% @endif @endif">
                        </div> --}}

                    </div>
                    @php
                        if(Auth::check()){
                            if (currentPoints(Auth::user()->id) < getScoringPoints('bronze')['to'])
                                $pkg = 'silver';

                            elseif(currentPoints(Auth::user()->id) > getScoringPoints('bronze')['to'] && currentPoints(Auth::user()->id) < getScoringPoints('silver')['to'])
                                $pkg = 'gold';

                            elseif(currentPoints(Auth::user()->id) > getScoringPoints('silver')['to'] && currentPoints(Auth::user()->id) < getScoringPoints('gold')['to'])
                                $pkg = 'platinuim';

                            elseif(currentPoints(Auth::user()->id) > getScoringPoints('gold')['to'] && currentPoints(Auth::user()->id) < getScoringPoints('platinum')['to'])
                                $pkg = 'diamond';

                            elseif(currentPoints(Auth::user()->id) > getScoringPoints('platinum')['to'] && currentPoints(Auth::user()->id) < getScoringPoints('diamond')['to'])
                                $pkg = 'emerald';
                            else
                                $pkg = 'emerald';
                        }
                    @endphp
                    @if ($pkg == 'silver')
                        
                    <div class="row">
                        <div class="col-md-6">
                            <p class="points_Detail">Points to reach the next level</p>
                        </div>
                        <div class="col-md-2">

                        </div>
                        <div class="col-md-4">
                            <p class="points_fig">
                                {{getScoringPoints('bronze')['to']}} / {{getScoringPoints('silver')['from']}}
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="points_Detail">Points to reach the next level</p>
                        </div>
                        <div class="col-md-2">

                        </div>
                        <div class="col-md-4">
                            <p class="points_fig">
                                {{getScoringPoints('silver')['to']}} / {{getScoringPoints('gold')['from']}}
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="points_Detail">Points to reach the next level</p>
                        </div>
                        <div class="col-md-2">

                        </div>
                        <div class="col-md-4">
                            <p class="points_fig">
                                {{getScoringPoints('gold')['to']}} / {{getScoringPoints('platinum')['from']}}
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="points_Detail">Points to reach the next level</p>
                        </div>
                        <div class="col-md-2">

                        </div>
                        <div class="col-md-4">
                            <p class="points_fig">
                                {{getScoringPoints('platinum')['to']}} / {{getScoringPoints('diamond')['from']}}
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="points_Detail">Points to reach the next level</p>
                        </div>
                        <div class="col-md-2">

                        </div>
                        <div class="col-md-4">
                            <p class="points_fig">
                                {{getScoringPoints('diamond')['to']}} / {{getScoringPoints('emerald')['from']}}
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="points_Detail">Points to reach the next level</p>
                        </div>
                        <div class="col-md-2">

                        </div>
                        <div class="col-md-4">
                            <p class="points_fig">
                                {{getScoringPoints('emerald')['from']}}
                            </p>
                        </div>
                    </div>
                    @elseif ($pkg == 'gold')
                    <div class="row">
                        <div class="col-md-6">
                            <p class="points_Detail">Points to reach the next level</p>
                        </div>
                        <div class="col-md-2">

                        </div>
                        <div class="col-md-4">
                            <p class="points_fig">
                                {{getScoringPoints('gold')['to']}} / {{getScoringPoints('platinum')['from']}}
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="points_Detail">Points to reach the next level</p>
                        </div>
                        <div class="col-md-2">

                        </div>
                        <div class="col-md-4">
                            <p class="points_fig">
                                {{getScoringPoints('platinum')['to']}} / {{getScoringPoints('diamond')['from']}}
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="points_Detail">Points to reach the next level</p>
                        </div>
                        <div class="col-md-2">

                        </div>
                        <div class="col-md-4">
                            <p class="points_fig">
                                {{getScoringPoints('diamond')['to']}} / {{getScoringPoints('emerald')['from']}}
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="points_Detail">Points to reach the next level</p>
                        </div>
                        <div class="col-md-2">

                        </div>
                        <div class="col-md-4">
                            <p class="points_fig">
                                {{getScoringPoints('emerald')['from']}}
                            </p>
                        </div>
                    </div>
                    @elseif ($pkg == 'platinum')
                    <div class="row">
                        <div class="col-md-6">
                            <p class="points_Detail">Points to reach the next level</p>
                        </div>
                        <div class="col-md-2">

                        </div>
                        <div class="col-md-4">
                            <p class="points_fig">
                                {{getScoringPoints('platinum')['to']}} / {{getScoringPoints('diamond')['from']}}
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="points_Detail">Points to reach the next level</p>
                        </div>
                        <div class="col-md-2">

                        </div>
                        <div class="col-md-4">
                            <p class="points_fig">
                                {{getScoringPoints('diamond')['to']}} / {{getScoringPoints('emerald')['from']}}
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="points_Detail">Points to reach the next level</p>
                        </div>
                        <div class="col-md-2">

                        </div>
                        <div class="col-md-4">
                            <p class="points_fig">
                                {{getScoringPoints('emerald')['from']}}
                            </p>
                        </div>
                    </div>
                    @elseif ($pkg == 'diamond')
                    <div class="row">
                        <div class="col-md-6">
                            <p class="points_Detail">Points to reach the next level</p>
                        </div>
                        <div class="col-md-2">

                        </div>
                        <div class="col-md-4">
                            <p class="points_fig">
                                {{getScoringPoints('diamond')['to']}} / {{getScoringPoints('emerald')['from']}}
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="points_Detail">Points to reach the next level</p>
                        </div>
                        <div class="col-md-2">

                        </div>
                        <div class="col-md-4">
                            <p class="points_fig">
                                {{getScoringPoints('emerald')['from']}}
                            </p>
                        </div>
                    </div>
                    @elseif ($pkg == 'emerald')
                    <div class="row">
                        <div class="col-md-6">
                            <p class="points_Detail">Points to reach the next level</p>
                        </div>
                        <div class="col-md-2">

                        </div>
                        <div class="col-md-4">
                            <p class="points_fig">
                                {{getScoringPoints('emerald')['from']}}
                            </p>
                        </div>
                    </div>
                    @endif
                    <div class="row">
                        {{-- <div class="col-md-12">
                            <p class="points_Detail">You have collected {{currentPoints(Auth::user()->id)}} points so far. You need 10 million points to reach.<strong class="points_fig"> FX Club Membership.</strong></p>
                        </div> --}}


                    </div>



                </div>
                <div class="col-md-6">

                    {{-- <img class="banner_cover display_none_small" src="images/ban.PNG" > --}}

                </div>
            </div>

        </div> {{-- row-div --}}

    </div>  {{-- container-div --}}


{{-- second container start --}}
    <div class="container">
        <div class="row">
             <div class="col-md-6">

                {{-- <img class="banner_cover display_none_small" src="images/ban.PNG" style="width: 100%;"> --}}

            </div>
            @php
                $days = \Carbon\Carbon::parse(now()->subDays(-7))->format('Y-m-d H:i:s');
                $new_referral = Auth::user()->referrals->where('created_at','<=',$days);
            @endphp
            <div class="col-md-6">

                <div class="profile_box">
                    <input type="text" value="{{url('').'/referal/'.Auth::user()->referral.'/register'}}" id="myInput" class="inputlink">
                    <p class="profile_box_p_ra" style="word-wrap: break-word;">Your Referral Link: <span style="color: #2196f3; cursor: pointer;" onclick="myFunction()">{{url('').'/referal/'.Auth::user()->referral.'/register'}}</span></p>
                    <p class="profile_box_p_ra">Referral bonuses</p>
                    <p style="text-align: right;">Pending withdrawal: ${{$pending_withdraw}}</p>
                    <div class="row">

                        <div class="col-md-6">
                            <p class="amount_pro">$0</p>
                            {{-- <p class="amount_pro">${{$trade_cont_value}}</p> --}}
                        </div>
                        <div class="col-md-4 offset-md-2">
                            <p style=" lletter-spacing: 0px;
                            color: #B435AF;
                            opacity: 1;
                            font-size: 20px;
                            font-weight: 600;">{{$new_referral->count()}}New</p>
                        </div>
                    </div>


                    <div class="seprater_profile_box">
                    </div>
                    <h6>Referred users</h6>
                    {{-- {{dd(Auth::user()->referrals)}} --}}
                    <div class="row">
                        @foreach (Auth::user()->referrals as $referral)
                            <div class="col-md-3">{{$referral->referralUser->name}}</div>
                        @endforeach
                    </div>

                </div>
            </div>


        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="profile_box">
                    <div class="row">
                        <div class="col-md-6">
                            <p class="profile_box_p_ra">$ Cashback Earning</p>
                        </div>
                        {{-- <div class="col-md-6">
                            <form action="#">
                                <div class="form-group">

                                    <select class="form-control" id="sel1" name="sellist1">
                                    <option>In this Month</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                </select>
                            </form>
                        </div> --}}
                    </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    {{-- <div class="chartContainer" style="height: 300px; width: 100%;"></div> --}}
                        <p class="profile_box_p_ra pd_top_extra">Detailed cashback calendar by accounts</p>
                        <table id="companies_detail_table" class="table">
                            <tbody>
                                <tr>
                                    <th scope="row"><img src="{{url('')}}/images/logo.png" style="width: 120px; background: #003f54;height: 80px;"></th>
                                    <td class="detail_and_company">FX Cashbacks, ID: <b>{{Auth::user()->user_account_id}}</b></td>
                                </tr>
                                @foreach ($brokers as $broker)
                                @php
                                    $user_account_id = \App\UserBrokerAccount::where([['user_id',Auth::user()->id],['broker_id',$broker->broker->id],['approved',1]])->get();
                                @endphp
                                @if (isset($user_account_id))
                                    @foreach ($user_account_id as $item)

                                    <tr>
                                        <th scope="row"><img src="{{url('').'/uploads/'.$broker->broker->image}}" style="width: 120px;"></th>
                                        <td class="detail_and_company">Forex account, ID: <b>{{$item->user_account_id}}</b></td>
                                        <td>Amount: <b>${{brokerWiseAmount(Auth::user()->id, $broker->broker->id)}}</b></td>
                                        <td>Points: <b>{{brokerWisePoints(Auth::user()->id, $broker->broker->id)}}</b></td>
                                    </tr>
                                    @endforeach
                                @endif
                                @endforeach
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>  

<style>
.scrollbar
{

	float: left;
	height: 600px;
	width:100%;
	background:#ffffff;
	overflow-y: scroll;
	margin-bottom: 25px;
}
/*
 *  STYLE 2
*/

 #style-2::-webkit-scrollbar-track
{
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	border-radius: 10px;
	background-color: #ffffff;
}

#style-2::-webkit-scrollbar
{
	width: 12px;
	background-color: #ffffff;
}

#style-2::-webkit-scrollbar-thumb
{
	border-radius: 10px;
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
	background-color: #007bff;
}
.force-overflow
{
	min-height: 450px;
}
.inputlink{
    visibility: hidden;
}
/* Graph */
.column-chart {
  position: relative;
  z-index: 20;
  bottom: 0;
  left: 50%;
  width: 100%;
  height: 220px;
  margin-top: 40px;
  margin-left: -50%;
}

@media (min-width: 568px) {
  .column-chart {
    width: 80%;
    margin-left: -40%;
  }
}

@media (min-width: 768px) {
  .column-chart {
    width: 60%;
    margin-left: -30%;
  }

}

@media (min-width: 992px) {
  .column-chart {
    width: 40%;
    margin-left: -20%;
  }
}

@media (min-width: 1024px) {
  .column-chart {
    width: 36%;
    margin-left: -18%;
  }
}
@media (max-width: 786px) {
.display_none_small{
display: none;
  }
}
.column-chart:before,
.column-chart:after {
  position: absolute;
  content: '';
  top: 0;
  left: 0;
  width: calc(100% + 30px);
  height: 25%;
  margin-left: -15px;
  border-top: 1px dashed #b4b4b5;
  border-bottom: 1px dashed #b4b4b5;
}

.column-chart:after {
  top: 50%;
}

.column-chart > .legend {
  position: absolute;
  z-index: -1;
  top: 0;
}

.column-chart > .legend.legend-left {
  left: 0;
  width: 25px;
  height: 75%;
  margin-left: -55px;
  border: 1px solid #b4b4b5;
  border-right: none;
}

.column-chart > .legend.legend-left > .legend-title {
  display: block;
  position: absolute;
  top: 50%;
  left: 0;
  width: 65px;
  height: 50px;
  line-height: 50px;
  margin-top: -25px;
  margin-left: -60px;
  font-size: 28px;
  letter-spacing: 1px;
}

.column-chart > .legend.legend-right {
  right: 0;
  width: 100px;
  height: 100%;
  /* margin-right: -115px; */
}

.column-chart > .legend.legend-right > .item {
  position: relative;
  width: 100%;
  height: 25%;
}

.column-chart > .legend.legend-right > .item > h4 {
  display: block;
  position: absolute;
  top: 0;
  right: 0;
  width: 100px;
  height: 40px;
  line-height: 40px;
  margin-top: -20px;
  font-size: 16px;
  text-align: right;
}

.column-chart > .chart {
  position: relative;
  z-index: 20;
  bottom: 0;
  left: 50%;
  width: 98%;
  height: 100%;
  margin-left: -49%;
}

.column-chart > .chart > .item {
  position: relative;
  float: left;
  height: 100%;
}

.column-chart > .chart > .item:before {
  position: absolute;
  z-index: -1;
  content: '';
  bottom: 0;
  left: 50%;
  width: 1px;
  height: calc(100% + 15px);
  border-right: 1px dashed #b4b4b5;
}

.column-chart > .chart > .item > .bar {
  position: absolute;
  bottom: 0;
  left: 3px;
  width: 94%;
  height: 100%;
}

.column-chart > .chart > .item > .bar > span.percent {
  display: block;
  position: absolute;
  z-index: 25;
  bottom: 0;
  left: 0;
  width: 100%;
  height: 26px;
  line-height: 26px;
  color: #fff;
  background-color: #3e50b4;
  font-size: 14px;
  font-weight: 700;
  text-align: center;
  letter-spacing: 1px;
}

.column-chart > .chart > .item > .bar > .item-progress {
  position: absolute;
  bottom: 0;
  left: 0;
  width: 100%;
  height: 20%;
  color: #fff;
  background-color: #ff4081;
}

.column-chart > .chart > .item > .bar > .item-progress > .title {
  position: absolute;
  top: calc(50% - 13px);
  left: 50%;
  font-size: 14px;
  text-align: center;
  text-transform: uppercase;
  letter-spacing: 2px;
  -moz-transform: translateX(-50%) translateY(-50%) rotate(-90deg);
  -webkit-transform: translateX(-50%) translateY(-50%) rotate(-90deg);
  transform: translateX(-50%) translateY(-50%) rotate(-90deg);
}

@media (min-width: 360px) {
  .column-chart > .chart > .item > .bar > .item-progress > .title {
    font-size: 16px;
  }
}

@media (min-width: 480px) {
  .column-chart > .chart > .item > .bar > .item-progress > .title {
    font-size: 18px;
  }
}
</style>
</section>
<script>
    function myFunction() {
        /* Get the text field */
        var copyText = document.getElementById("myInput");

        /* Select the text field */
        copyText.select();
        copyText.setSelectionRange(0, 99999); /* For mobile devices */

        /* Copy the text inside the text field */
        navigator.clipboard.writeText(copyText.value);

        /* Alert the copied text */
        // alert("Copied the text: " + copyText.value);
    }

$(document).ready(function(){
    columnChart();

    function columnChart(){
        var item = $('.chart', '.column-chart').find('.item'),
        itemWidth = 100 / item.length;
        item.css('width', itemWidth + '%');

        $('.column-chart').find('.item-progress').each(function(){
            var itemProgress = $(this),
            itemProgressHeight = $(this).parent().height() * ($(this).data('percent') / 100);
            itemProgress.css('height', itemProgressHeight);
        });
    };
});

</script>

@endsection


