<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <title>FxCashbacks</title>

    <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/modern_business.css') }}" rel="stylesheet">
    <link href="{{ asset('css/media_query.css') }}" rel="stylesheet">
    <link href="{{ url('')}}/assets/img/favicon.png" rel="icon">
    <link href="{{ url('')}}/assets/img/apple-touch-icon.png" rel="apple-touch-icon">
 
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
<style>
    .navbar-light .navbar-nav .active>.nav-link, .navbar-light .navbar-nav .nav-link.active, .navbar-light .navbar-nav .nav-link.show, .navbar-light .navbar-nav .show>.nav-link {
        color: white !important;
    }
</style>
   <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-249312996-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'UA-249312996-1');
    </script>

</head>

<body>
    @php
    $all_brokers = getBrokers();
    @endphp
    <!-- Navigation -->
    <section class="dashborad_page_top_menu">
        <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
            <div class="container">
                <a class="navbar-brand" href="{{url('')}}">
                    <img class='logo_f' src="{{url('')}}/images/logo.png"> </a>
                    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                    data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    {{-- <ul class="navbar-nav ml-auto">

                    </ul> --}}
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item {{ (request()->routeIs('mainpage')) ? '' : '' }}">
                            <a class="text-center nav-link {{ (request()->routeIs('mainpage')) ? '' : '' }}"
                                href="{{ route('mainpage') }}">FOREX</a>
                                  <p style="color: white;font-size:11px;font-weight: bold;" class="text-center">
                                      <!--(Click here to add your Unique Broker ID to FxCashbacks)-->
                                  </p>
                            </li>
                            {{-- <li class="nav-item {{ (request()->routeIs('binary')) ? 'active' : '' }}">
                                <a class="nav-link {{ (request()->routeIs('binary')) ? 'active' : '' }}"
                                    href="{{ route('binary') }}">>BINARY</a>
                                </li> --}}
                    </ul>
                    <ul class="navbar-nav ml-auto">
                        {{-- <li class="nav-item">
                            <a class="nav-link" href="#"><i class="fas fa-bell"></i></a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                EN
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">

                            </div>
                        </li> --}}
                        {{-- <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                REFERAL
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
                            </div>
                        </li> --}}
                        @auth 
                            {{-- <li class="nav-item">
                                <p class="dynamic_per_perpage">
                                    @if (currentPoints(Auth::user()->id) < 499) {{currentPoints(Auth::user()->id) * 100 / 500}}%
                                        @elseif(currentPoints(Auth::user()->id) > 499 && currentPoints(Auth::user()->id) < 2499)
                                            {{currentPoints(Auth::user()->id) * 100 / 2500}}% @elseif(Auth::user()->
                                            current_points > 2499 && currentPoints(Auth::user()->id) < 9999)
                                                {{currentPoints(Auth::user()->id) * 100 / 10000}}% @elseif(Auth::user()->
                                                current_points > 9999 && currentPoints(Auth::user()->id) < 24999)
                                                    {{currentPoints(Auth::user()->id) * 100 / 25000}}% @elseif(Auth::user()->
                                                    current_points > 24999 && currentPoints(Auth::user()->id) < 99999)
                                                        {{currentPoints(Auth::user()->id) * 100 / 100000}}% @else 100%
                                                        @endif
                                </p> </a> 
                            </li>  --}}
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog"
                                    data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false">
                                    {{ Auth::user()->name }}
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
                                    <a class="dropdown-item" href="{{route('home')}}">
                                        Dashboard
                                    </a>
                                    <a class="dropdown-item" href="#"
                                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}"
                                        method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </div>
                            </li>
                            @endauth

                    </ul>
                    
                </div>
            </div>
        
          
        </nav>

    </section>

    <section id="nav_dashboard" style="margin-top: 20px;"></section>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav w-100 justify-content-center px-3">
                <li class="nav-item active {{ (request()->routeIs('Homepage')) ? 'active' : '' }}">
                    <a class="nav-link {{ (request()->routeIs('Homepage')) ? 'active' : '' }}"
                        href="{{ route('Homepage') }}">HOME</a>
                </li>

                <li class="nav-item {{ (request()->routeIs('how-its-work')) ? 'active' : '' }}">
                    <a class="nav-link {{ (request()->routeIs('how-its-work')) ? 'active' : '' }}"
                        href="{{ route('how-its-work') }}">HOW IT WORKS</a>
                </li>


                <li class="nav-item {{ (request()->routeIs('features')) ? 'active' : '' }}">
                    <a class="nav-link {{ (request()->routeIs('features')) ? 'active' : '' }}"
                        href="{{ route('features') }}">FEATURES</a>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        BROKERS
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
                        @foreach ($all_brokers as $all_broker)
                        <a class="dropdown-item"
                            href="{{route('web_broker',$all_broker->id)}}">{{$all_broker->name}}</a>

                        @endforeach
                    </div>
                </li>

                <li class="nav-item {{ (request()->routeIs('support')) ? 'active' : '' }}">
                    <a class="nav-link {{ (request()->routeIs('support')) ? 'active' : '' }}"
                        href="{{ route('support') }}">SUPPORT</a>
                </li>


                <li class="nav-item {{ (request()->routeIs('contact')) ? 'active' : '' }}">
                    <a class="nav-link {{ (request()->routeIs('contact')) ? 'active' : '' }}"
                        href="{{ route('contact') }}">CONTACT</a>
                </li>


            </ul>
        </div>
    </nav>
    </section>

    <section id="mainpage_banner">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="index_fold_content">FOREX CASHBACK ON EVERY TRADE</h3>
                    <p>FOREX CASHBACK ON EVERY TRADE
                        No matter you win or lose on a trade, you gain rebate. Increase your forex trading profit with
                        the highest forex rebates. It's your money, we take it seriously.</p>

                    <div class="row">
                        <div class="col-md-8">
                              <a href="" style="margin: 0 auto" class="btn btn-lg btn-primary"  data-toggle="modal" data-target="#staticBackdrop"><i
                        class="far fa-play-circle"></i>SEE HOW IT WORKS</a>
                        <!--<a href="#myModal" class="btn btn-lg btn-primary" data-toggle="modal">See How It Works</a>-->
    
    <!-- Modal HTML -->
    <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">                    
                    <div class="embed-responsive embed-responsive-16by9">
                     <iframe width="600" height="415" src="https://www.youtube.com/embed/GNNm2C8j1WU?autoplay=" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
                            <ul>
                               <!-- <li style="display: inline-flex;">
                                    <iframe width="150" height="100" src="https://www.youtube.com/embed/tgbNymZ7vqY?autoplay=1&mute=1"></iframe>
                                    <span class="span_link">Interested in how this can work?
                                        Watch it on video (1:20)</span>
                                </li>---->


                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="index_fold_brokers">
                        <ul>
                            @foreach ($brokers as $broker)
                            <li>
                                <a href="{{ route('web_broker', $broker->id) }}">
                                    <img class='brokers_icons' src="{{url('')}}/uploads/{{$broker->image}}">
                                </a>
                            </li>
                            @endforeach
                            {{-- <li>
                        <a href="#">
                            <img class='brokers_icons' src="images/fbs1 (3).png" >
                        </a>
                    </li>

                    <li>
                        <a href="#">
                            <img class='brokers_icons' src="images/exnes2.png" >
                        </a>
                    </li>   <li>
                        <a href="#">
                            <img class='brokers_icons' src="images/fbs2.png" >
                        </a>
                    </li> --}}
                        </ul>
                    </div>
                </div>
            </div>

    </section>






    <h2 class="head">It’s your money. We take it seriously.</h2>
    <section id="indexHiw">

        <div class="container">

            <hr class="underHead">

            <div id="hiwSteps">
                <ul>
                    <li class="hsReg">
                        <h4>Register an account</h4>
                        <p>
                            <a href="//fxCashbacks.co/register"><strong>Register a fxCashbacks account for
                                    free.</strong></a> No credit card, no account information needs to be provided.
                        </p>
                    </li>
                    <li class="hsConnect">
                        <h4>Connect to a broker</h4>
                        <p>Select a broker suitable for you. You can open new accounts at <a href="/brokers">more than
                                30 brokers</a> or even add existing accounts at some of them.</p>
                    </li>
                    <li class="hsTrade">
                        <h4>Trade, trade, trade</h4>
                        <p>Once you start trading your cashback will automatically arrive on your account. Plus, if you
                            trade more, you get more. And don’t forget, <strong>you get rebate on EVERY trade, no matter
                                you win or lose</strong>.</p>
                    </li>
                    <li class="hsMoney">
                        <h4>Get your money</h4>
                        <p>After collecting cashback you can withdraw your money to NETELLER, Skrill, PayPal, EcoPayz,
                            FasaPay and Wire Transfer. Your money should arrive in 3 business days.</p>
                    </li>
                </ul>
                <div class="paymentIcons">
                    <i data-bg="url(/common/images/payments/payments.png), linear-gradient(#fff, #fff)"
                        class="lazy sprite-payments payment-neteller" title="Neteller" data-was-processed="true"
                        style="background-image: url(&quot;/common/images/payments/payments.png&quot;), linear-gradient(rgb(255, 255, 255), rgb(255, 255, 255));"></i>
                    <i data-bg="url(/common/images/payments/payments.png), linear-gradient(#fff, #fff)"
                        class="lazy sprite-payments payment-paypal" title="PayPal" data-was-processed="true"
                        style="background-image: url(&quot;/common/images/payments/payments.png&quot;), linear-gradient(rgb(255, 255, 255), rgb(255, 255, 255));"></i>
                    <i data-bg="url(/common/images/payments/payments.png), linear-gradient(#fff, #fff)"
                        class="lazy sprite-payments payment-skrill" title="Skrill" data-was-processed="true"
                        style="background-image: url(&quot;/common/images/payments/payments.png&quot;), linear-gradient(rgb(255, 255, 255), rgb(255, 255, 255));"></i>
                    <i data-bg="url(/common/images/payments/payments.png), linear-gradient(#fff, #fff)"
                        class="lazy sprite-payments payment-ecopayz" title="EcoPayz" data-was-processed="true"
                        style="background-image: url(&quot;/common/images/payments/payments.png&quot;), linear-gradient(rgb(255, 255, 255), rgb(255, 255, 255));"></i>
                    <i data-bg="url(/common/images/payments/payments.png), linear-gradient(#fff, #fff)"
                        class="lazy sprite-payments payment-fasapay" title="FasaPay" data-was-processed="true"
                        style="background-image: url(&quot;/common/images/payments/payments.png&quot;), linear-gradient(rgb(255, 255, 255), rgb(255, 255, 255));"></i>
                    <i data-bg="url(/common/images/payments/payments.png), linear-gradient(#fff, #fff)"
                        class="lazy sprite-payments payment-wiretransfer" title="Wiretransfer" data-was-processed="true"
                        style="background-image: url(&quot;/common/images/payments/payments.png&quot;), linear-gradient(rgb(255, 255, 255), rgb(255, 255, 255));"></i>
                </div>
            </div>
            <div id="hiwFigureContainer">
                <div class="hiwFigure">
                    <div class="hf hfTop">
                        <h5 class="fcbc">Forex<br><strong>fx<br>Cashbacks</strong></h5>
                        <p>We verify your account and you’ll be put under our affiliate network.</p>
                    </div>
                    <div class="hf hfRight">
                        <h5>Broker</h5>
                        <p>gives us money from all spreads and commissions you pay them</p>
                    </div>
                    <div class="hf hfBottom">
                        <p>We share the majority of our revenue with you, paying you a cash rebate for each trade</p>
                        <h5 class="fcbc">Forex<br><strong>Fxcashbacks</strong></h5>
                    </div>
                    <div class="hf hfLeft">
                        <h5>You</h5>
                        <p>open an account through us or connect your existing account</p>
                    </div>
                </div>

                <div class="hiwText">
                    If you open an account through fxCashbacks, the brokers pay part of their spreads and commissions to
                    us on every trade you make. We share most of this revenue with you, paying you forex rebate on each
                    trade. <strong>Your spreads and trading conditions remain the same as if you had opened an account
                        directly with the broker.</strong> However, as our client you earn extra cash on every trade.
                </div>

            </div>
            <div class="clear"></div>
            {{-- <div class="endButton">
                <a href="//fxCashbacks.com/register?r=forex" class="indexButton green">Get started now</a>
            </div> --}}

        </div>
    </section>


    <section id="indexBrokers" class="gradientBg">
        <div class="container">
            <h2 class="head">Forex cashback deals for lots of <strong>brokers</strong></h2>
            <hr class="underHead">

            <div class="featuredContainer">
                @foreach ($brokers as $broker)
                    
              
                <a class="oneFeatured" href="{{route('web_broker',$broker->id)}}" id="oneFeatured1">
                    <img class="brokerLogo lazy loaded"
                    src="{{url('').'/uploads/'.$broker->image}}" alt="Exness" title="Exness" width="100" height="100" >
                    <span class="textContainer"><em><strong>{{$broker->name}}</strong></em>
                        <span class="statRow">
                            Cashback on EUR/USD 
                        </span>
                    </span>
                </a>
                @endforeach
                        {{-- <a class="oneFeatured" href="/brokers/fortfs"
                    id="oneFeatured2"><img class="brokerLogo lazy loaded" src="images/Assets/Group 474.png"
                        alt="Fort Financial Services" title="Fort Financial Services" width="100" height="100"
                        src="images/Assets/Group 474.png" data-was-processed="true"><span
                        class="textContainer"><em><strong>Fort Financial Services</strong></em><span class="statRow">
                            Cashback on EUR/USD <big><b>up to $9.35 / lot</b></big></span><span
                            class="statRow">fxCashbacks rating <big><i
                                    class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                    class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                    class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                    class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                    class="fa fa-fw fa-star font-size-16 color-star"></i></big></span><span
                            class="statRow"><i class="fa fa-check color-green margin-right-5"></i>Available for existing
                            accounts</span></span></a><a class="oneFeatured" href="/brokers/fp-markets"
                    id="oneFeatured3"><img class="brokerLogo lazy loaded" data-src="images/Assets/Group 476.png"
                        alt="FP Markets" title="FP Markets" width="100" height="100" src="images/Assets/Group 476.png"
                        data-was-processed="true"><span class="textContainer"><em><strong>FP Markets</strong></em><span
                            class="statRow">
                            Cashback on EUR/USD <big><b>up to $4.25 / lot</b></big></span><span
                            class="statRow">fxCashbacks rating <big><i
                                    class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                    class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                    class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                    class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                    class="fa fa-fw fa-star font-size-16 color-star"></i></big></span><span
                            class="statRow"><i class="fa fa-times color-red margin-right-5"></i>No existing
                            accounts</span></span></a><a class="oneFeatured" href="/brokers/spectre-ai"
                    id="oneFeatured4"><img class="brokerLogo lazy loaded" data-src="images/Assets/Group 477.png"
                        alt="Spectre.ai" title="Spectre.ai" width="100" height="100" src="images/Assets/Group 477.png"
                        data-was-processed="true"><span class="textContainer"><em><strong>Spectre.ai</strong></em><span
                            class="statRow">
                            Cashback<big><b> Up to 42% cashback f...</b></big></span><span class="statRow">fxCashbacks
                            rating <big><i class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                    class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                    class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                    class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                    class="fa fa-fw fa-star font-size-16 color-star"></i></big></span><span
                            class="statRow"><i class="fa fa-check color-green margin-right-5"></i>Available for existing
                            accounts</span></span>
                </a> --}}
            </div>

            <div class="brokersContainer">

                <a href="/brokers/4xcube"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-4xcube" title="4XC"
                        data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/alpari"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-alpari" title="Alpari"
                        data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/amega-fx"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-amega-fx" title="AMEGA"
                        data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/axiory"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-axiory" title="Axiory"
                        data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/axitrader"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-axitrader" title="Axi"
                        data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/bdswiss"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-bdswiss" title="BDSwiss"
                        data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/bigboss"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-bigboss" title="Big Boss"
                        data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/blackbull-markets"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-blackbull-markets"
                        title="BlackBull Markets" data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/blackwell-global-uk"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-blackwell-global-uk"
                        title="Blackwell Global UK" data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/ck-markets"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-ck-markets"
                        title="CK Markets" data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/errante"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-errante" title="Errante"
                        data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/fbs"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-fbs" title="FBS"
                        data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/forex-chief"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-forex-chief"
                        title="ForexChief" data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/forex-mart"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-forex-mart"
                        title="ForexMart" data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/fxcc"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-fxcc" title="FXCC"
                        data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/fxprimus"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-fxprimus" title="FxPrimus"
                        data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/fxpro-cysec"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-fxpro-cysec" title="FxPro"
                        data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/fxtm"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-fxtm" title="FXTM"
                        data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/gkfxprime"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-gkfxprime"
                        title="GKFX Prime" data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/gkfxprime-cpa"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-gkfxprime-cpa"
                        title="GKFX Prime" data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/hotforex"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-hotforex" title="HotForex"
                        data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/icmarkets"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-icmarkets"
                        title="IC Markets" data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/instaforex"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-instaforex"
                        title="InstaForex" data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/justforex"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-justforex"
                        title="JustForex" data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/landfx"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-landfx" title="LAND-FX"
                        data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/lmfx"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-lmfx" title="LMFX"
                        data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/nordfx"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-nordfx" title="NordFX"
                        data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/octafx"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-octafx" title="OctaFX"
                        data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/paxforex"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-paxforex" title="PaxForex"
                        data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/pepperstone"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-pepperstone"
                        title="Pepperstone" data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/roboforex"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-roboforex"
                        title="Roboforex" data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/royal-financial-trading"><i
                        data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-royal-financial-trading"
                        title="Royal" data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/thinkmarkets"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-thinkmarkets"
                        title="ThinkMarkets" data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/tickmill"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-tickmill" title="Tickmill"
                        data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/tickmill-uk"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-tickmill-uk"
                        title="Tickmill UK" data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/valutrades"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-valutrades"
                        title="Valutrades" data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/valutrades-sc"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-valutrades-sc"
                        title="Valutrades-SC" data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/vantagefx"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-vantagefx"
                        title="Vantage FX" data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/varianse"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-varianse"
                        title="VDX Limited" data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/xm"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-xm" title="XM Group"
                        data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a><a
                    href="/brokers/zulutrade"><i data-bg="url(/common/images/brokers/forex-brokers-65.png)"
                        class="lazy brokerLogo border-radius-default sprite-broker-forex-65 s-zulutrade"
                        title="Zulutrade" data-was-processed="true"
                        style="background-image: url(&quot;/common/images/brokers/forex-brokers-65.png&quot;);"></i></a>
            </div>

            {{-- <div class="col-md-3 offset-md-4">
                <p class="getstarted_Button">Get Started</p>
            </div> --}}


        </div>
    </section>

    {{-- <section id="indexEwalletsContainer">

        <div class="container">

            <h2 class="head">And even more... With e-wallet promotions</h2>
            <hr class="underHead">

            <div class="indexEwallets row">

                <div class="ieLeft col-md-5">
                    <h3>Use e-wallets for cheaper transaction costs</h3>
                    <p>
                        Nearly all of forex traders and lots of our customers use e-wallets to send and receive money.
                        To be honest, it costs less and it is much faster than other payment methods.<br> Now with
                        ForexfxCashbacks.com, you can <strong>use the most popular e-wallet services much
                            cheaper.</strong> All you need to do, is register an e-wallet account under our referral
                        network and wait for your well deserved cashback.
                    </p>

                    <div class="buttons">
                        <a href="//fxCashbacks.co/ewallet-promotions-for-skrill-and-neteller"
                            class="readmore_Button green">Read more</a>
                    </div>

                </div>
                <div class="ieRight col-md-5 offset-md-1">
                    <a href="/redirect-to-url/http://wlskrill.adsrv.eacdn.com/C.ashx%3Fbtag=a_40384b_3923c_%26affid=38007%26siteid=40384%26adid=3923%26c="
                        target="_blank">
                        <img src="images/Assets/Group 537.png" alt="20% Cheaper Skrill transactions" class="lazy">
                        <span class="ieText">
                            <em>Skrill<small>, Receive money using Skrill</small></em>
                            <span class="textRow">Promotion period <big><i
                                        class="fa fa-fw fa-clock-o marginRight4"></i>Lifetime</big></span>
                            <span class="textRow">Cost reduction <big>up to 20% on transactions</big></span>
                        </span>
                    </a>
                    <a href="/redirect-to-neteller" target="_blank">
                        <img src="images/Assets/Group 539.png" alt="20% Cheaper NETELLER transactions"
                            class="lazy loaded" src="/common/images/index_ewalletcb_neteller.png"
                            data-was-processed="true">
                        <span class="ieText">
                            <em>NETELLER<small>, Receive money using NETELLER</small></em>
                            <span class="textRow">Promotion period <big><i
                                        class="fa fa-fw fa-clock-o marginRight4"></i>Lifetime</big></span>
                            <span class="textRow">Cost reduction <big>up to 20% on transactions</big></span>
                        </span>
                    </a>
                    <a rel="noreferrer"
                        href="https://secure.ecopayz.com/Registration.aspx?_atc=hhxw1hjsajyntc0qb1np2ffaw"
                        target="_blank">
                        <img src="images/Assets/Group 540.png" alt="30% Cheaper Ecopayz transactions"
                            class="lazy loaded" src="/common/images/index_ewalletcb_ecopayz.png"
                            data-was-processed="true">
                        <span class="ieText">
                            <em>Ecopayz<small>, Receive money using Ecopayz</small></em>
                            <span class="textRow">Promotion period <big><i
                                        class="fa fa-fw fa-clock-o marginRight4"></i>Lifetime</big></span>
                            <span class="textRow">Cost reduction <big>up to 30% on transactions</big></span>
                        </span>
                    </a>
                </div>
                <div class="col-md-3 offset-md-4">
                    <p class="getstarted_Button">Get Started</p>
                </div>

            </div>

        </div>

    </section> --}}


    {{-- <section id="success_stories">
        <h2>READ OUR SUCCESS STORIES</h2>
        <div class="container">

            <div class="row">

                <div class="col-md-6">
                    <p>We've met with a lot of types of traders throughout the years. Since 2012, we're offering a
                        competitive deal no matter if you're a scalper or a long term trader.
                    </p>
                    <p>
                        We always ask our users about what they think of us. Read these success stories to learn more
                        about our users' experience with us.</p>
                </div>
                <div class="col-md-6">

                    <div class="promo_right">
                        <img class='img_side_ban' src="images/Assets/succes.png">
                    </div>
                </div>
                <div class="col-md-4 offset-md-4 pd_sixty">
                    <p class="storyButton">Read Our Succes Stories</p>
                </div>

            </div>


        </div>



    </section> --}}



    @yield('content')


    <footer class="section footer-classic context-dark bg-image bg_footer_gra">
        <div class="container">
            <div class="row">

                <div class="col-md-7">
                    <h5 class="hed_foter"><a class="{{ (request()->routeIs('terms')) ? 'active' : '' }}" href="{{ route('terms') }}"> Terms of Service </a></h5>
                    <h5 class="hed_foter"><a class="{{ (request()->routeIs('privacy')) ? 'active' : '' }}" href="{{ route('privacy') }}"> Privacy Policy </a></h5>
                    <h5 class="hed_foter"><a class="{{ (request()->routeIs('support')) ? 'active' : '' }}" href="{{ route('support') }}"> FAQ </a></h5>
                    <h5 class="hed_foter"> <a class=" {{ (request()->routeIs('contact')) ? 'active' : '' }}"
                        href="{{ route('contact') }}"> Contact Us </a></h5>
                    {{-- <p class="para_foter">Reporting of Rebates High Rebate Rates<br> Highly Efficient custome</p> --}}
                </div>

                <div class="col-md-5">
                    <h5 class="hed_foter">Join Our Newsletter</h5>
                    <div class="input-group">
                        <input type="search" class="form-control rounded" placeholder="Search" aria-label="Search"
                            aria-describedby="search-addon" />
                        <button type="button" class="btn btn-outline-primary">Submit</button>
                    </div>
                </div>


                {{-- <div class="container">
                    <div class="row pd">
                        <div class="seprater"></div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">

                        <div class="col-md-6">
                            <a {{ (request()->routeIs('terms')) ? 'active' : '' }}" href="{{ route('terms') }}">
                                <p class="text-center">Terms of Service</p>

                        </div>



                        <div class="col-md-6">
                            <a {{ (request()->routeIs('privacy')) ? 'active' : '' }}" href="{{ route('privacy') }}">
                                <p class="text-center">Privacy Policy</p>


                        </div>
                    </div>
                </div> --}}

            </div>
        </div>

    </footer>
    <style>

    </style>
    <!-- Bootstrap core JavaScript -->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
     <!-- Modal -->
    <div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">How It Works!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <video controls>
                        <source src="{{url('')}}/homepage/vid/video.mp4" type="video/mp4">
                        Your browser does not support the video tag.
                    </video>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js"
        integrity="sha384-+sLIOodYLS7CIrQpBjl+C7nPvqq+FbNUBDunl/OZv93DB7Ln/533i8e/mZXLi/P+" crossorigin="anonymous">
    </script>
    <script src="https://kit.fontawesome.com/190961af7c.js" crossorigin="anonymous"></script>

    <script>
$(document).ready(function(){
    /* Get iframe src attribute value i.e. YouTube video url
    and store it in a variable */
    var url = $("#cartoonVideo").attr('src');
    
    /* Remove iframe src attribute on page load to
    prevent autoplay in background */
    $("#cartoonVideo").attr('src', '');
    
    /* Assign the initially stored url back to the iframe src
    attribute when modal is displayed */
    $("#myModal").on('shown.bs.modal', function(){
        $("#cartoonVideo").attr('src', url);
    });
    
    /* Assign empty url value to the iframe src attribute when
    modal hide, which stop the video playing */
    $("#myModal").on('hide.bs.modal', function(){
        $("#cartoonVideo").attr('src', '');
    });
});
</script>
 <style>
    .modal-content iframe{
        margin: 0 auto;
        display: block;
    }
    .modal-backdrop {
    position: relative;
    top: 0;
    left: 0;
    z-index: 1040;
    width: 100vw;
    height: 100vh;
    background-color: #000;
}
</style>
</body>

</html>
