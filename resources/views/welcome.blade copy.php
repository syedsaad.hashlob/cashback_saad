<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>FxCashbacks</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">


    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/modern_business.css" rel="stylesheet">
    <link href="css/media_query.css" rel="stylesheet">
    <!-- Favicon -->
    <link href="{{ url('')}}/assets/img/favicon.png" rel="icon">
    <link href="{{ url('')}}/assets/img/apple-touch-icon.png" rel="apple-touch-icon">

    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />

</head>


<body>
    @php
    $all_brokers = getBrokers();
    @endphp
    <!-- Navigation -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                <img class='logo_f' src="{{url('')}}/images/logo.png">
            </a>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active {{ (request()->routeIs('Homepage')) ? 'active' : '' }}">
                        <a class="nav-link {{ (request()->routeIs('Homepage')) ? 'active' : '' }}"
                            href="{{ route('Homepage') }}">HOME</a>
                    </li>

                    <li class="nav-item {{ (request()->routeIs('how-its-work')) ? 'active' : '' }}">
                        <a class="nav-link {{ (request()->routeIs('how-its-work')) ? 'active' : '' }}"
                            href="{{ route('how-its-work') }}">HOW IT WORKS</a>
                    </li>


                    <li class="nav-item {{ (request()->routeIs('features')) ? 'active' : '' }}">
                        <a class="nav-link {{ (request()->routeIs('features')) ? 'active' : '' }}"
                            href="{{ route('features') }}">FEATURES</a>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            BROKERS
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
                            @foreach ($all_brokers as $all_broker)
                            <a class="dropdown-item"
                                href="{{route('web_broker',$all_broker->id)}}">{{$all_broker->name}}</a>

                            @endforeach
                        </div>
                    </li>

                    {{-- <li class="nav-item {{ (request()->routeIs('features')) ? 'active' : '' }}">
                    <a class="nav-link {{ (request()->routeIs('features')) ? 'active' : '' }}"
                        href="{{ route('features') }}">BROKERS</a>

                    </li> --}}


                    <li class="nav-item {{ (request()->routeIs('support')) ? 'active' : '' }}">
                        <a class="nav-link {{ (request()->routeIs('support')) ? 'active' : '' }}"
                            href="{{ route('support') }}">SUPPORT</a>
                    </li>


                    <li class="nav-item {{ (request()->routeIs('contact')) ? 'active' : '' }}">
                        <a class="nav-link {{ (request()->routeIs('contact')) ? 'active' : '' }}"
                            href="{{ route('contact') }}">CONTACT</a>
                    </li>
                    <!-- Button trigger modal -->
                </ul>
                {{-- <ul>
                    @if (Route::has('login'))
                                <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
                        @auth
                                <a href="{{ route('home') }}"><button type="button"
                    class="btn btn-primary btn_sign_modal_nav">
                    DASHBOARD
                </button></a>
                @else
                <li>
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary btn_sign_modal_nav" data-toggle="modal"
                        data-target="#loginexampleModal">
                        login
                    </button>
                </li>
                <li>
                    <!-- Button trigger modal -->
                    <a href="{{route('register')}}">
                        <button type="button" class="btn btn-primary btn_sign_modal_nav" data-toggle="modal"
                            data-target="#loginexampleModal">
                            Register
                        </button>
                    </a>
                </li>
                @endauth
            </div>
            @endif

            </ul> --}}
        </div>
        </div>
    </nav>

    <section id="hero1" class="hero">

        <div class="inner">


            <div class="copy">
                <h1 class="banner_text">Earn With Every trade <br> You Make!</h1>

                <div class="container">
                    <div class="row">
                        @if (Auth::check())
                        <div class="col-md-6 offset-md-3">

                            @else

                            <div class="col-md-6 offset-md-4">
                                @endif
                                <div class="row margin_top_row">

                                    @if (Route::has('login'))
                                    <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
                                        @auth
                                        <a href="{{ route('home') }}"><button style="margin: 50px" type="button"
                                                class="btn btn-primary btn_sign_modal_nav dashboard_btn">
                                                DASHBOARD
                                            </button></a>

                                        <a href="#" style="margin: 50px"
                                            class="btn btn-primary btn_sign_modal_nav dashboard_btn"
                                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                            style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                        @else


                                        <!-- Button trigger modal -->
                                        <button type="button" class="btn btn-primary_login btn_sign_modal_nav"
                                            data-toggle="modal" data-target="#loginexampleModal">
                                            login
                                        </button>
                                    </div>
                                    <a href="{{route('register')}}">
                                        <button type="button"
                                            class="btn btn-primary_register btn_sign_modal_nav register_btn"
                                            data-toggle="modal" data-target="#loginexampleModal">
                                            Register
                                        </button>
                                    </a>


                                    


 

   
        
                                    @endauth

                                    <!-- Button trigger modal -->
                                    @endif

                                </div>




                            </div>

                        </div>
                    </div>



                </div>





    








    </section>

    {{-- @foreach($broker as $brokers)
    <tr>
        <th scope="row">1</th>
      <td><img src="{{ url('').'/uploads/'. $brokers->image}}" width="50" height="50" class="rounded-circle" /></td>
    <td>{{ $brokers->name}}</td>
    <td>{{ $brokers->description}}</td>
    </tr>
    @endforeach --}}

    <section id="work_box_sec">
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <div class="how_work">
                        <div class="row">
                            <div class="col-md-1"><img class="work_icon" src="images/Icon.png"></div>
                            <div class="col-md-5">
                                <p class="work_title">How FxCashBacks Works</p>
                            </div>
                        </div>
                        <p class="p_cls">When you open your Forex trading account (or connect an existing one) through
                            us, your broker pays us a rebate for every trade. We then pay you back the majority of this
                            rebate which you can withdraw at any time. Keep in mind that
                            your trading conditions (including spreads) remain exactly the same as if you had opened the
                            account directly with the broker, so in effect, you’re reducing trading costs and improving
                            profitability.</p>
                    </div>


                </div>
            </div>

    </section>
    <section id="feature_sec">
        <h1 class="heading_fea">FEATURES</h1>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="box_fea">
                        <img class="feature_icon_in_home" src="images/icon1.png">

                        <h3 class="fea_box_hed">Real-Time Reports</h3>
                        <p class="para_fea">View your earnings in real-time.</p>
                        <h6 class="get_start"><a href="{{ route('register') }}">GET STARTED</a></h6>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="box_fea">
                        <img class="feature_icon_in_home" src="images/speed.png">

                        <h3 class="fea_box_hed">Highest Rebate Rates</h3>
                        <p class="para_fea">We pay the best rebate rates.</p>
                        <h6 class="get_start"><a href="{{ route('register') }}">GET STARTED</a></h6>
                    </div>
                </div>


                <div class="col-md-4">
                    <div class="box_fea">
                        <img class="feature_icon_in_home" src="images/icon3.png">

                        <h3 class="fea_box_hed">Brought To You By Myfxbook.com</h3>
                        <p class="para_fea">Developed and maintained by Myfxbook, the leading social forex community
                            since 2009.</p>
                        <h6 class="get_start"><a href="{{ route('register') }}">GET STARTED</a></h6>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section id="broker_sec display_none_mobile_screen">
        <h1 class="heading_fea">BROKERS</h1>
        <div class="container">
            <div class="row">

                <div class="col-md-12">
                    <div class="bg_broker"></div>
                </div>


            </div>
        </div>

        <div class="container">

            <div class="row mg_top">
                @foreach($broker as $brokers)
                <div class="col-md-2 text-center">
                    <a href="{{route('web_broker',$brokers->id)}}" target="_blank">

                        <img class="broker_logo rounded" src="{{ url('').'/uploads/'. $brokers->image}}">
                        <h6 class="broker_title_h">{{ $brokers->name}}</h6>
                        <p class="broker_title_p">{{ $brokers->description}}</p>
                </div>
                </a>
                @endforeach
            </div>

        </div>



    </section>


    <!-- Login Modal -->
    <section id="login_modal">


        <div class="modal fade" id="loginexampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-primary">
                                        <div class="panel-body">
                                            @if (session('error'))
                                                <p class="alert alert-danger">{{session('error')}}</p>
                                            @endif
                                            <form method="POST" action="{{ route('login') }}" role="form">
                                                @csrf
                                                <div class="form-group">
                                                    <h2>SIGN IN </h2>
                                                </div>
                                                <div class="form-group">
                                                    <input id="email" placeholder="Email Address or username"
                                                        type="email"
                                                        class="form-control @error('email') is-invalid @enderror"
                                                        name="email" value="{{ old('email') }}" required
                                                        autocomplete="email" autofocus>
                                                    @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <input id="password" placeholder="Enter Your Password"
                                                        type="password"
                                                        class="form-control @error('password') is-invalid @enderror"
                                                        name="password" required autocomplete="current-password">

                                                    @error('password')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-6 ">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="checkbox"
                                                                name="remember" id="remember"
                                                                {{ old('remember') ? 'checked' : '' }}>

                                                            <label class="form-check-label" for="remember">
                                                                {{ __('Remember Me') }}
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-6 offset-md-3">
                                                        <button id="signupSubmit" type="submit"
                                                            class="btn btn-info btn-block">{{ __('Login') }}</button>
                                                        @if (Route::has('password.request'))
                                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                                            {{ __('Forgot Your Password?') }}
                                                        </a>
                                                        @endif
                                                    </div>
                                                </div>
                                                <p class="text-Center">Use Social Network to Sign In</p>
                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <button type="submit"
                                                                class="facebook-btn btn btn-info btn-block"><i
                                                                    class="fab fa-facebook-f"></i> FaceBook
                                                                Login</button>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <button type="submit"
                                                                class="gmail-btn btn btn-info btn-block"><img
                                                                    src="images/gmail.svg"> Gmail</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                                <p></p>Already have an account?
                                                @if (Route::has('register'))
                                                <a href="{{ route('register') }}"
                                                    class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">Register</a>
                                                @endif
                                                </p>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Login Modal end -->



    <section id="sign-up">

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <div class="row">
                                <div class="panel panel-primary">
                                    <div class="panel-body">
                                        <form method="POST" action="#" role="form">
                                            <div class="form-group">
                                                <h2>SIGN UP</h2>
                                            </div>
                                            <div class="form-group">
                                                <input id="signupName" type="text" maxlength="50" class="form-control"
                                                    placeholder="Your name">
                                            </div>
                                            <div class="form-group">
                                                <input id="signupEmail" type="email" maxlength="50" class="form-control"
                                                    placeholder="Email">
                                            </div>

                                            <div class="form-group">
                                                <input id="signupPassword" type="password" maxlength="25"
                                                    class="form-control" placeholder="at least 6 characters"
                                                    length="40">
                                            </div>

                                            <div class="form-group">
                                                <button id="signupSubmit" type="submit"
                                                    class="btn btn-info btn-block">Create your account</button>
                                            </div>
                                            <p class="form-group">By creating an account, you agree to our <a
                                                    href="#">Terms of Use</a> and our <a href="#">Privacy Policy</a>.
                                            </p>
                                            <hr>
                                            <p></p>Already have an account? <a href="#">Sign in</a></p>



                                            <!-- Button trigger modal -->
                                            <button type="button" class="btn btn-primary btn_sign_modal_nav"
                                                data-toggle="modal" data-target="#exampleModal">
                                                Sign in
                                            </button></li>
                                            <a href="{{ route('login') }}"
                                                class="text-sm text-gray-700 dark:text-gray-500 underline">Login</a>


                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </section>


    <footer class="section footer-classic context-dark bg-image bg_footer_gra">
        <div class="container">
            <div class="row">
                {{-- <div class="col-md-3">
                    <h5 class="hed_foter">How forex works</h5>
                                <p class="para_foter">He Printing And Typese tting Indus try. Lorem Ipsum has Been The Type</p>
                </div> --}}

                <div class="col-md-7">
                    <h5 class="hed_foter"> Features</h5>
                    <p class="para_foter">Reporting of Rebates High Rebate Rates<br> Highly Efficient custome</p>
                </div>

                <div class="col-md-5">
                    <h5 class="hed_foter">Join Our Newsletter</h5>
                    <div class="input-group">
                        <input type="search" class="form-control rounded" placeholder="Search" aria-label="Search"
                            aria-describedby="search-addon" />
                        <button type="button" class="btn btn-outline-primary">Submit</button>
                    </div>
                </div>


                <div class="container">
                    <div class="row pd">
                        <div class="seprater"></div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">

                        <div class="col-md-6">
                            <a {{ (request()->routeIs('terms')) ? 'active' : '' }}" href="{{ route('terms') }}">
                                <p class="text-center">Terms of Service</p>

                        </div>



                        <div class="col-md-6">
                            <a {{ (request()->routeIs('privacy')) ? 'active' : '' }}" href="{{ route('privacy') }}">
                                <p class="text-center">Privacy Policy</p>


                        </div>
                    </div>
                </div>

            </div>
        </div>

    </footer>
    <style>
    /* .embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%;  */
    </style>
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script>
        @if(count($errors) > 0 || session('error'))
        $('#loginexampleModal').modal('show');
        @endif



    </script>
 


</body>

</html>
