<link href="{{ url('')}}/assets/img/favicon.png" rel="icon">
<link href="{{ url('')}}/assets/img/apple-touch-icon.png" rel="apple-touch-icon">

@extends('layouts.common')

@section('content')
<section id="hero_inner_page" class="hero">

    <div class="inner">
        <div class="copy">

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="heading_fea_new">HOW IT WORKS<h2>
                    </div>



                </div>
            </div>

        </div>
    </div>
</section>
<section class="how_it_works" id="howItWorks">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-9 col-md-9 col-lg-7 col-centered">
                <h2>How it works</h2>
                <p>The whole thing starts which you do something (registering, depositing, purchasing, trading, playing
                    etc.) through our referral link. Or you connect your existing account in some cases. Then, it's all
                    on us and you'll receive money.</p>
            </div>
        </div>
    </div>
</section>

<section id="indexHiw">

    <div class="container">

        <hr class="underHead">

        <div id="hiwSteps">
            <ul>
                <li class="hsReg">
                    <h4>Register an account</h4>
                    <p>
                        <a href="{{ route('register') }}"><strong>Register a fxCashbacks account for free.</strong></a>
                        No credit card, no account information needs to be provided.
                    </p>
                </li>
                <li class="hsConnect">
                    <h4>Connect to a broker</h4>
                    <p>Select a broker suitable for you. You can open new accounts at <a href="/brokers">more than 30
                            brokers</a> or even add existing accounts at some of them.</p>
                </li>
                <li class="hsTrade">
                    <h4>Trade, trade, trade</h4>
                    <p>Once you start trading your cashback will automatically arrive on your account. Plus, if you
                        trade more, you get more. And don’t forget, <strong>you get rebate on EVERY trade, no matter you
                            win or lose</strong>.</p>
                </li>
                <li class="hsMoney">
                    <h4>Get your money</h4>
                    <p>After collecting cashback you can withdraw your money to NETELLER, Skrill, PayPal, EcoPayz,
                        FasaPay and Wire Transfer. Your money should arrive in 3 business days.</p>
                </li>
            </ul>
            <div class="paymentIcons">
                <i data-bg="url(/common/images/payments/payments.png), linear-gradient(#fff, #fff)"
                    class="lazy sprite-payments payment-neteller" title="Neteller" data-was-processed="true"
                    style="background-image: url(&quot;/common/images/payments/payments.png&quot;), linear-gradient(rgb(255, 255, 255), rgb(255, 255, 255));"></i>
                <i data-bg="url(/common/images/payments/payments.png), linear-gradient(#fff, #fff)"
                    class="lazy sprite-payments payment-paypal" title="PayPal" data-was-processed="true"
                    style="background-image: url(&quot;/common/images/payments/payments.png&quot;), linear-gradient(rgb(255, 255, 255), rgb(255, 255, 255));"></i>
                <i data-bg="url(/common/images/payments/payments.png), linear-gradient(#fff, #fff)"
                    class="lazy sprite-payments payment-skrill" title="Skrill" data-was-processed="true"
                    style="background-image: url(&quot;/common/images/payments/payments.png&quot;), linear-gradient(rgb(255, 255, 255), rgb(255, 255, 255));"></i>
                <i data-bg="url(/common/images/payments/payments.png), linear-gradient(#fff, #fff)"
                    class="lazy sprite-payments payment-ecopayz" title="EcoPayz" data-was-processed="true"
                    style="background-image: url(&quot;/common/images/payments/payments.png&quot;), linear-gradient(rgb(255, 255, 255), rgb(255, 255, 255));"></i>
                <i data-bg="url(/common/images/payments/payments.png), linear-gradient(#fff, #fff)"
                    class="lazy sprite-payments payment-fasapay" title="FasaPay" data-was-processed="true"
                    style="background-image: url(&quot;/common/images/payments/payments.png&quot;), linear-gradient(rgb(255, 255, 255), rgb(255, 255, 255));"></i>
                <i data-bg="url(/common/images/payments/payments.png), linear-gradient(#fff, #fff)"
                    class="lazy sprite-payments payment-wiretransfer" title="Wiretransfer" data-was-processed="true"
                    style="background-image: url(&quot;/common/images/payments/payments.png&quot;), linear-gradient(rgb(255, 255, 255), rgb(255, 255, 255));"></i>
            </div>
        </div>
        <div id="hiwFigureContainer">
            <div class="hiwFigure">
                <div class="hf hfTop">
                    <h5 class="fcbc">Forex<br><strong>Fxcashbacks</strong></h5>
                    <p>We verify your account and you’ll be put under our affiliate network.</p>
                </div>
                <div class="hf hfRight">
                    <h5>Broker</h5>
                    <p>gives us money from all spreads and commissions you pay them</p>
                </div>
                <div class="hf hfBottom">
                    <p>We share the majority of our revenue with you, paying you a cash rebate for each trade</p>
                    <h5 class="fcbc">Forex<br><strong>Fxcashbacks</strong></h5>
                </div>
                <div class="hf hfLeft">
                    <h5>You</h5>
                    <p>open an account through us or connect your existing account</p>
                </div>
            </div>

            <div class="hiwText">
                If you open an account through fxCashbacks, the brokers pay part of their spreads and commissions to us
                on every trade you make. We share most of this revenue with you, paying you forex rebate on each trade.
                <strong>Your spreads and trading conditions remain the same as if you had opened an account directly
                    with the broker.</strong> However, as our client you earn extra cash on every trade.
            </div>

        </div>
        <div class="clear"></div>
        <div class="endButton">
            <a href="//fxCashbacks.com/register?r=forex" class="indexButton green">Get started now</a>
        </div>

    </div>
</section>
<section id="howItWorks">
    <section class="cb_way">
        <div class="container">
            <h2>The FxCashbacks way</h2>

            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ways">
                    <div class="img_container">
                        {{-- <img data-src="{{url('')}}/images/padlock.svg" alt="padlock" width="25" height="35"
                        class="lazy loaded" src="{{url('')}}/images/padlock.svg" data-was-processed="true"> --}}
                    </div>
                    <strong>Privacy and protection first</strong>
                    <p>It's important for us to offer a completely risk-free solution. We don’t ask for credit cards or
                        any sensible account information.</p>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ways">
                    <div class="img_container">
                        {{-- <img data-src="{{url('')}}/images/clients.svg" alt="group" width="45" height="39"
                        class="lazy loaded" src="{{url('')}}/images/clients.svg" data-was-processed="true"> --}}
                    </div>
                    <strong>Trusted by 10,000+ clients</strong>
                    <p>We proudly serve more than 10,000 forex and binary option traders, poker players and e-wallet
                        users from all over the world.</p>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ways">
                    <div class="img_container">
                        {{-- <img data-src="{{url('')}}/images/star.svg" alt="star" width="38" height="38" class="lazy
                        loaded" src="{{url('')}}/images/star.svg" data-was-processed="true"> --}}
                    </div>
                    <strong>4.8 / 5 rating by our users</strong>
                    <p>We ask everybody to rate us and tell their opinion. Based on more than 1,000 ratings, our
                        customers says that we’re awesome.</p>
                </div>
            </div>
        </div>

    </section>
</section>
