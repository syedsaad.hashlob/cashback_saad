<link href="{{ url('')}}/assets/img/favicon.png" rel="icon">
<link href="{{ url('')}}/assets/img/apple-touch-icon.png" rel="apple-touch-icon">

@extends('layouts.common')

@section('content')

<section id="staticContact" style=" padding-top: 100px;" class="container_fixed staticPage">


<div class="container">
    <div class="row">
    <div class="contactLeft col-md-6">
        <h4>Get in touch with us anytime, from anywhere</h4>
        <ul>
            <li>
                <em>Email<small>for general inquiries</small></em>
                <span><a href="mailto:info@fxcashbacks.com">info@fxcashbacks.com</a></span>
            </li>
            <li>
                <em>Support</em>
                <span><a href="mailto:support@fxcashbacks.com">support@fxcashbacks.com</a></span>
            </li>
            <li>
                <em>accounts</em>
                <span><a href="mailto:accounts@fxcashbacks.com">accounts@fxcashbacks.com</a></span>
            </li>
            {{-- <li>
                <em>Partnership and sales email<small>for brokers and affiliates</small></em>
                <span><a href="mailto:partner@fxcashbacks.com">partner@fxcashbacks.com</a></span>
            </li>
                                                                                                                                                                                                                                    <li>
                <em>Skype<small></small></em>
                <span><a href="skype:fxcashbacks?userinfo"><i class="fa fa-skype fa-fw fa-normal marginRight4"></i>FxCashback</a></span>
            </li>
            <li>
                <em>Facebook<small></small></em>
                <span><a href="https://www.facebook.com/fxcashbacks"><i class="fa fa-facebook fa-fw fa-normal marginRight4"></i>FxCashback</a></span>
            </li>
            <li>
                <em>Twitter<small></small></em>
                <span><a href="https://twitter.com/fxcashbacks"><i class="fa fa-twitter fa-fw fa-normal marginRight4"></i>FxCashback</a></span>
            </li>
            <li>
                <em>Google+<small></small></em>
                <span><a href="https://plus.google.com/+FxCashback/"><i class="fa fa-google-plus fa-fw fa-normal marginRight4"></i>FxCashback</a></span>
            </li> --}}

        </ul>
    </div>

    {{-- <div class="contactRight col-md-5">
        <h4>Company Information</h4>
        <p>FxCashback was founded in 2013. During this past 2 years we became one of the most reliable forex rebate provider on the market. We offer the highest rebate to our loved customers in more than 50 countries all over the world.</p>

        <ul>
            <li>
                <em>Company name<small></small></em>
                <span>FxCashback Ltd.</span>
            </li>
            <li>
                <em>Company address<small></small></em>
                <span style="width: 150px;">8 Copthall, Roseau Valley, 00152, Commonwealth of Dominica</span>
                <div class="clear"></div>
            </li>
            <li>
                <em>Registration number<small></small></em>
                <span>17414</span>
            </li>
            <li>
                <em>Founded in<small></small></em>
                <span>2013</span>
            </li>

        </ul>

        <h4>Staff</h4>
        <p>We are working for you to get more and more money back. We cannot tell you enough how happy we are if you join us. Huge thanks to all of our customers. We love you!</p>



    </div> --}}

    <div class="clear"></div>

    </div>
</div>
</section>
