<link href="{{ url('')}}/assets/img/favicon.png" rel="icon">
<link href="{{ url('')}}/assets/img/apple-touch-icon.png" rel="apple-touch-icon">

@extends('layouts.common')

@section('content')
            <style>

a, a:hover, a:focus, a:active {
    text-decoration: none;
    outline: none;
}
ul {
    margin: 0;
    padding: 0;
    list-style: none;
            }.bg-gray {
    background-color: #f9f9f9;
}

            .site-heading h2 {
  display: block;
  font-weight: 700;
  margin-bottom: 10px;
  text-transform: uppercase;
            }

.site-heading h2 span {
  color:#005570;
            }

.site-heading h4 {
  display: inline-block;
  padding-bottom: 20px;
              position: relative;
  text-transform: capitalize;
  z-index: 1;
}

.site-heading h4::before {
              background: #eff4f5 none repeat scroll 0 0;
  bottom: 0;
  content: "";
  height: 2px;
  left: 50%;
  margin-left: -25px;
              position: absolute;
  width: 50px;
}

.site-heading {
  margin-bottom: 60px;
  overflow: hidden;
  margin-top: -5px;
}

            .carousel-shadow .owl-stage-outer {
  margin: -15px -15px 0;
  padding: 15px;
}

            .we-offer-area .our-offer-carousel .owl-dots .owl-dot span {
  background: #ffffff none repeat scroll 0 0;
  border: 2px solid;
  height: 15px;
  margin: 0 5px;
  width: 15px;
}

            .we-offer-area .our-offer-carousel .owl-dots .owl-dot.active span {
  background: #eff4f5 none repeat scroll 0 0;
  border-color: #eff4f5;
}

.we-offer-area .item {
  background: #ffffff none repeat scroll 0 0;
  border-left: 2px solid #eff4f5;
  -moz-box-shadow: 0 0 10px #cccccc;
  -webkit-box-shadow: 0 0 10px #cccccc;
  -o-box-shadow: 0 0 10px #cccccc;
              box-shadow: 0 0 10px #cccccc;
  overflow: hidden;
  padding: 30px;
  position: relative;
  z-index: 1;
}

            .we-offer-area.text-center .item {
  background:#a7a7a7  none repeat scroll 0 0;
  border: medium none;
  padding: 67px 40px 64px;
  height: 330px;


}

            .we-offer-area.text-center .item i {
  background: #013546 none repeat scroll 0 0;
  -webkit-border-radius: 50%;
  -moz-border-radius: 50%;
              border-radius: 50%;
  color: #ffffff;
  font-size: 40px;
  height: 80px;
  line-height: 80px;
  position: relative;
  text-align: center;
              width: 80px;
  z-index: 1;
  transition: all 0.35s ease-in-out;
  -webkit-transition: all 0.35s ease-in-out;
              -moz-transition: all 0.35s ease-in-out;
  -ms-transition: all 0.35s ease-in-out;
  -o-transition: all 0.35s ease-in-out;
  margin-bottom: 25px;
}

.we-offer-area.text-center .item i::after {
  border: 2px solid #eff4f5;
  -webkit-border-radius: 50%;
  -moz-border-radius: 50%;
  border-radius: 50%;
  content: "";
  height: 90px;
              left: -5px;
  position: absolute;
  top: -5px;
  width: 90px;
  z-index: -1;
  transition: all 0.35s ease-in-out;
  -webkit-transition: all 0.35s ease-in-out;
              -moz-transition: all 0.35s ease-in-out;
  -ms-transition: all 0.35s ease-in-out;
  -o-transition: all 0.35s ease-in-out;
}

.we-offer-area.item-border-less .item {
              border: medium none;
}

.we-offer-area .our-offer-items.less-carousel .equal-height {
  margin-bottom: 30px;
}

.we-offer-area.item-border-less .item .number {
  font-family: "Poppins",sans-serif;
  font-size: 50px;
  font-weight: 900;
  opacity: 0.1;
              position: absolute;
  right: 30px;
  top: 30px;
}

            .our-offer-carousel.center-active .owl-item:nth-child(2n) .item,
.we-offer-area.center-active .single-item:nth-child(2n) .item {
  background: #eff4f5 none repeat scroll 0 0;
}

            .our-offer-carousel.center-active .owl-item:nth-child(2n) .item i,
.our-offer-carousel.center-active .owl-item:nth-child(2n) .item h4,
.our-offer-carousel.center-active .owl-item:nth-child(2n) .item p,
.we-offer-area.center-active .single-item:nth-child(2n) .item i,
.we-offer-area.center-active .single-item:nth-child(2n) .item h4,
            .we-offer-area.center-active .single-item:nth-child(2n) .item p {
  color: #ffffff;
}

            .we-offer-area .item i {
  color: #eff4f5;
  display: inline-block;
  font-size: 60px;
  margin-bottom: 20px;
            }

.we-offer-area .item h4 {
  font-weight: 600;
  text-transform: capitalize;
}

            .we-offer-area .item p {
  margin: 0;
}

.we-offer-area .item i,
.we-offer-area .item h4,
            .we-offer-area .item p {
  transition: all 0.35s ease-in-out;
  -webkit-transition: all 0.35s ease-in-out;
  -moz-transition: all 0.35s ease-in-out;
  -ms-transition: all 0.35s ease-in-out;
  -o-transition: all 0.35s ease-in-out;
            }

.we-offer-area .item::after {
  background: black none repeat scroll 0 0;
  content: "";
              height: 100%;
  left: -100%;
  position: absolute;
  top: 0;
  transition: all 0.35s ease-in-out;
  -webkit-transition: all 0.35s ease-in-out;
              -moz-transition: all 0.35s ease-in-out;
  -ms-transition: all 0.35s ease-in-out;
  -o-transition: all 0.35s ease-in-out;
  width: 100%;
  z-index: -1;
}

            .we-offer-area .item:hover::after {
  left: 0;
}

.we-offer-area .item:hover i,
.we-offer-area .item:hover h4,
.we-offer-area .item:hover p {
  color: #ffffff !important;
}

.we-offer-area.text-center .item:hover i::after {
              border-color: #004055 !important;
}

.we-offer-area.text-center .item:hover i {
              background-color: #004055 !important;
  color: #eff4f5 !important;
}

.we-offer-area.text-left .item i {
  background: #eff4f5 none repeat scroll 0 0;
  -webkit-border-radius: 50%;
  -moz-border-radius: 50%;
  border-radius: 50%;
  color: #ffffff;
  display: inline-block;
  font-size: 60px;
  height: 100px;
              line-height: 100px;
  margin-bottom: 30px;
  position: relative;
  width: 100px;
  z-index: 1;
  text-align: center;
}

            .we-offer-area.text-left .item i::after {
  border: 2px solid #eff4f5;
  -webkit-border-radius: 50%;
  -moz-border-radius: 50%;
  border-radius: 50%;
  content: "";
  height: 120px;
  left: -10px;
  position: absolute;
              top: -10px;
  width: 120px;
}
section.we-offer-area.text-center.bg-gray {
    padding: 65px;
}
.we-offer-area h4 {
    color: white;
    padding: 10px;


    font-weight: 900;
}
#feature_banner{
  background-image: url("{{url('')}}/images/bg_fea.jpg") !important;
}
</style>
<section id="feature_banner">
                    <div class="container">
                    <div class="row">
                <div class="col-md-12">
                <h3 class="index_fold_content">WE PAY YOU BACK FOR YOUR TRADES.
                                TRADE. <strong>GET PAID.</strong> REPEAT.</h3>
                        </div>

             </div>
            </div>
            </section>


        <section id="Features_Cashback_works">
                    <div class="container">
                        <div class="row">
                    <div class="col-md-12">
                        <h2>How Forex Cashback works?</h2>
            <p>When you open your Forex trading account (or connect an existing one) through us, your broker pays us a rebate for every trade.
                We then pay you back the majority of this rebate which you can withdraw at any time.</p>
                        <p>Keep in mind that your trading conditions (including spreads) remain exactly the same as if you had opened the account directly with the broker, so in effect, you're reducing trading costs and improving profitability.</p>
                        <p>Sounds simple? It is!</p>

                                <p>Start getting paid back for your trades today. Sign up for free.</p>

                    </div>
                    </div>
                                    <div class="row">
                            <div class="col-md-4 offset-md-5">
                                <h6 class="btn-danger"><a href="{{ route('register') }}">SIGN UP FOR FREE</a></h6>
                            </div>
                            </div>
                </div>
        </section>


<section id="FX_Features">
            <div class="container">
                <div class="row">

                    <div class="col-md-12">
                <h2>FXcashbacks Features</h2>

                </div>
            <div class="col-md-6 box_info">
                {{-- inner_row --}}
                <div class="row">
        <div class="col-sm-4">
            <img src="images/icon1.png">
            </div>
                        <div class="col-sm-8">
                <h1>Real-Time Reports</h1>
                <p>View your earnings in real-time.</p>
                </div>
    </div>
        {{-- inner_rowend --}}
            </div>
            {{-- boxend --}}


            <div class="col-md-6 box_info">
                {{-- inner_row --}}
    <div class="row">
        <div class="col-sm-4">
                        <img src="images/speed.png">
            </div>
            <div class="col-sm-8">
                <h1>Highest Rebate Rates</h1>
                <p>We pay the best rebate rates.</p>
                </div>
                </div>
        {{-- inner_rowend --}}
            </div>
            {{-- boxend --}}




                        <div class="col-md-6 box_info">
                {{-- inner_row --}}
    <div class="row">
        <div class="col-sm-4">
                        <img src="images/icon3.png">
            </div>
            <div class="col-sm-8">
                <h1>Brought To You By Myfxbook.com</h1>
                <p>Developed and maintained by Myfxbook, the leading social forex community since 2009.</p>
                            </div>
    </div>
        {{-- inner_rowend --}}
            </div>
            {{-- boxend --}}


            <div class="col-md-6 box_info">
                {{-- inner_row --}}
    <div class="row">
                    <div class="col-sm-4">
            <img src="images/icon2.png">
            </div>
            <div class="col-sm-8">
                <h1>Awesome Support</h1>
                            <p>Get answers for any query you may have on time.</p>
                </div>
    </div>
        {{-- inner_rowend --}}
            </div>
                        {{-- boxend --}}












    </div>
            </div>
            </section>


{{-- <section id="support_forex_sec">
    <h2>Supported Forex Cashback Rebate brokers</h2>
                <div class="container">
    <div class="row">

        <div class="col-md-8">
                 <div class="row inner_log">

                <div class="col-md-4 box_bg">
                    <img class="broker_icon" src="images/fortfs.png">

                            </div>
            <div class="col-md-4 box_bg">
                            <img class="broker_icon" src="images/fortfs.png">
            </div>
                    <div class="col-md-4 box_bg">
            <img class="broker_icon" src="images/fortfs.png">
        </div>

                    </div>


        </div>
        <div class="col-md-4 bg_inner_rebate_detail">
          <h3>How Much Forex Rebates Can I Earn?</h3>
          <p>Earnings will very much depend on the rebate rate of your broker, the instruments you trade as well as the volume of your trades.
            Use the calculator below to roughly estimate your earnings</p>
                    </div>
           


            <div class="view_all_broker"><a href="">View All brokers And Rebates</a></div>


    </div>
    </div>
</section> --}}
