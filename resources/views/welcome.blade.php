<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>FX CASHBACKS</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
        integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('')}}/homepage/css/style.css">
    <link rel="stylesheet" href="{{url('')}}/css/modern_business.css">
    <link href="{{ url('')}}/assets/img/favicon.png" rel="icon">
    <link href="{{ url('')}}/assets/img/apple-touch-icon.png" rel="apple-touch-icon">
   <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-249312996-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'UA-249312996-1');
    </script>

</head>
@php
$all_brokers = getBrokers();
@endphp

<body style="padding-top:0px !important;">
    <header>
        <nav class="navbar custom-navbar navbar-expand-lg navbar-dark fixed-top" data-spy="affix" data-offset-top="10">
            <div class="container-fluid">
                <a class="navbar-brand" href="#">
                    <img src="{{url('')}}/homepage/imgs/logo.png" alt="">
                </a>
                <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active {{ (request()->routeIs('Homepage')) ? 'active' : '' }}">
                            <a class="nav-link {{ (request()->routeIs('Homepage')) ? 'active' : '' }}"
                                href="{{ route('Homepage') }}">HOME</a>
                        </li>

                        <li class="nav-item {{ (request()->routeIs('how-its-work')) ? 'active' : '' }}">
                            <a class="nav-link {{ (request()->routeIs('how-its-work')) ? 'active' : '' }}"
                                href="{{ route('how-its-work') }}">HOW IT WORKS</a>
                        </li>


                        <li class="nav-item {{ (request()->routeIs('features')) ? 'active' : '' }}">
                            <a class="nav-link {{ (request()->routeIs('features')) ? 'active' : '' }}"
                                href="{{ route('features') }}">FEATURES</a>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                BROKERS
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
                                @foreach ($all_brokers as $all_broker)
                                <a class="dropdown-item"
                                    href="{{route('web_broker',$all_broker->id)}}">{{$all_broker->name}}</a>

                                @endforeach
                            </div>
                        </li>

                        <li class="nav-item {{ (request()->routeIs('support')) ? 'active' : '' }}">
                            <a class="nav-link {{ (request()->routeIs('support')) ? 'active' : '' }}"
                                href="{{ route('support') }}">SUPPORT</a>
                        </li>


                        <li class="nav-item {{ (request()->routeIs('contact')) ? 'active' : '' }}">
                            <a class="nav-link {{ (request()->routeIs('contact')) ? 'active' : '' }}"
                                href="{{ route('contact') }}">CONTACT</a>
                        </li>
                        <!-- Button trigger modal -->
                    </ul>
                </div>
            </div>
        </nav>
    </header>

    <!-- Hero Banner Start -->
    <div class="overlay">
        <div id="hero-banner">

            <h1>EARN WITH EVERY TRADE <br>
                YOU MAKE!</h1>
            @auth
            <div class="btn-box text-center">
                <a href="{{route('home')}}" class="btn-1">DASHBOARD</a>
                <a href="#" class="btn-2"
                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="far fa-play-circle"></i>SEE HOW IT WORKS</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>

            @else
            <div class="btn-box text-center">
                <a href="{{route('login')}}" class="btn-1">LOGIN</a>
                <a href="{{route('register')}}" class="btn-1">REGISTER</a>
                <br><br><br><br>
                <a href="" style="margin: 0 auto" class="btn-2" data-toggle="modal" data-target="#staticBackdrop"><i
                        class="far fa-play-circle"></i>SEE HOW IT WORKS</a>
            </div>

            @endif

        </div>
    </div>
    <!-- Hero Banner End -->



    <!-- How it works Start -->
    <div id="how-it-works">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="how-it-works-content text-center">
                        <h2>How FxCashBacks Works</h2>
                        <p>When you open your Forex trading account (or connect an existing one) through us, your broker
                            pays us a rebate for every trade. We then pay you back the majority of this rebate which you
                            can withdraw at any time. Keep in mind that your trading conditions (including spreads)
                            remain exactly the same as if you had opened the account directly with the broker, so in
                            effect, you’re reducing trading costs and improving profitability.</p>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center box-container">
                <div class="col-lg-4">
                    <div class="box text-center align-center">
                        <h4>Real-Time Reports</h4>
                        <p>View your earnings in real-time.</p>

                        <img src="{{url('')}}/homepage/imgs/clock.png" alt="">
                        <a href="{{route('register')}}" class="box-btn">Get Started</a>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="box text-center">
                        <h4>Highest Rebate Rates</h4>
                        <p>We pay the best rebate rates <br> </p>

                        <img src="{{url('')}}/homepage/imgs/timer.png" alt="">
                        <a href="{{route('register')}}" class="box-btn">Get Started</a>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="box text-center">
                        <h4>Brought To You By Myfxbook.com</h4>
                        <p>Developed and maintained by Myfxbook, the
                            leading social forex community since 2009.</p>
                        <img src="{{url('')}}/homepage/imgs/idea.png" alt="">
                        <a href="{{route('register')}}" class="box-btn">Get Started</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- How it works End -->


    <!-- Broker Start -->

    <div id="broker">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>BROKERS</h2>
                </div>
            </div>
            <div class="row logo-container justify-content-center">
                @foreach($all_brokers as $key => $brokers)
                <div class="col-lg-2 text-center img-box">
                    <a href="{{route('web_broker',$brokers->id)}}" style="color: black;">
                        <img src="{{url('')}}/uploads/{{$brokers->image}}" alt="">
                        <h5>{{ $brokers->name}}</h5>
                        <p>{{ $brokers->description}}</p>
                    </a>

                </div>
                @endforeach
            </div>
        </div>
    </div>

    <!-- Broker End -->


    <!-- HOW TO START Start -->

    <div id="how-to-start">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2>HOW IT WORKS AND HOW TO START</h2>
            </div>
        </div>
        <div class="row justify-content-center ball">
            <div class="bubble-1">
                <h4>Register an account</h4>
                <p class="bulble_p"><a href="{{route('register')}}">Register a FxCashBacks account for free</a>.
                    No credit card, no account information needs to be provided. </p>
            </div>
            <div class="bubble-2">
                <h4>Connect to a broker</h4>
                <p class="bulble_p">
                    Select a broker suitable for you. You can open new accounts at more than 30 brokers or even add
                    existing accounts at some of them.
                </p>
            </div>
            <div class="col-lg-4 text-center ball-2">
                <img src="{{url('')}}/homepage/imgs/how-to-start-ball.png" class="img-fluid" style="height:72%;">
            </div>
            <div class="bubble-3">
                <h4>Trade, trade, trade</h4>
                <p class="bulble_p">
                    Once you start trading your FXCashbacks will automatically arrive on your account. Plus, if you
                    trade more, you get more. And don’t forget,<b> you get rebate on EVERY trade, no matter you win or
                        lose.</b>
                    <!-- We share the majority of our revenue with <br>you, paying you a cash rebate for each <br>trade -->
                </p>
            </div>
            <div class="bubble-4">
                <h4>Get your money</h4>
                <p class="bulble_p">
                    After collecting FXCashbacks you can withdraw your money to NETELLER, Skrill, PayPal, EcoPayz,
                    FasaPay and Wire Transfer. Your money should arrive in 3 business days.
                    <!-- gives us money from all spreads and <br>commissions you pay them -->
                </p>
            </div>
        </div>
    </div>
    <footer class="section footer-classic context-dark bg-image bg_footer_gra">
        <div class="container">
            <div class="row">

                <div class="col-md-7">
                    <h5 class="hed_foter"><a class="{{ (request()->routeIs('terms')) ? 'active' : '' }}" href="{{ route('terms') }}"> Terms of Service </a></h5>
                    <h5 class="hed_foter"><a class="{{ (request()->routeIs('privacy')) ? 'active' : '' }}" href="{{ route('privacy') }}"> Privacy Policy </a></h5>
                    <h5 class="hed_foter"><a class="{{ (request()->routeIs('support')) ? 'active' : '' }}" href="{{ route('support') }}"> FAQ </a></h5>
                    <h5 class="hed_foter"> <a class=" {{ (request()->routeIs('contact')) ? 'active' : '' }}"
                        href="{{ route('contact') }}"> Contact Us </a></h5>
                    {{-- <p class="para_foter">Reporting of Rebates High Rebate Rates<br> Highly Efficient custome</p> --}}
                </div>

                <div class="col-md-5">
                    <h5 class="hed_foter">Join Our Newsletter</h5>
                    <div class="input-group">
                        <input type="search" class="form-control rounded" placeholder="Search" aria-label="Search"
                            aria-describedby="search-addon" />
                        <button type="button" class="btn btn-outline-primary">Submit</button>
                    </div>
                </div>


                {{-- <div class="container">
                    <div class="row pd">
                        <div class="seprater"></div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">

                        <div class="col-md-6">
                            <a {{ (request()->routeIs('terms')) ? 'active' : '' }}" href="{{ route('terms') }}">
                                <p class="text-center">Terms of Service</p>

                        </div>



                        <div class="col-md-6">
                            <a {{ (request()->routeIs('privacy')) ? 'active' : '' }}" href="{{ route('privacy') }}">
                                <p class="text-center">Privacy Policy</p>


                        </div>
                    </div>
                </div> --}}

            </div>
        </div>

    </footer>

    <!-- Footer End -->



    <!-- Modal -->
    <div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">How It Works!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <video controls>
                        <source src="{{url('')}}/homepage/vid/video.mp4" type="video/mp4">
                        Your browser does not support the video tag.
                    </video>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js"
        integrity="sha384-+sLIOodYLS7CIrQpBjl+C7nPvqq+FbNUBDunl/OZv93DB7Ln/533i8e/mZXLi/P+" crossorigin="anonymous">
    </script>
    <script src="https://kit.fontawesome.com/190961af7c.js" crossorigin="anonymous"></script>
</body>

</html>
