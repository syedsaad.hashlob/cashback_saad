<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>FXCASHBACKS</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <!-- Favicon -->
    <link href="{{ url('')}}/assets/img/favicon.png" rel="icon">
    <link href="{{ url('')}}/assets/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/modern_business.css" rel="stylesheet">
    <link href="css/media_query.css" rel="stylesheet">

    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>



</head>
   <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-249312996-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'UA-249312996-1');
    </script>


<body>
@php
    $all_brokers = getBrokers();
@endphp
  <!-- Navigation -->
  <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            <img class='logo_f' src="{{url('')}}/images/logo.png" >
        </a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
            data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active {{ (request()->routeIs('Homepage')) ? 'active' : '' }}">
                    <a class="nav-link {{ (request()->routeIs('Homepage')) ? 'active' : '' }}" href="{{ route('Homepage') }}">HOME</a>
                </li>

                <li class="nav-item {{ (request()->routeIs('how-its-work')) ? 'active' : '' }}">
                    <a class="nav-link {{ (request()->routeIs('how-its-work')) ? 'active' : '' }}" href="{{ route('how-its-work') }}">HOW IT WORKS</a>
                </li>


                <li class="nav-item {{ (request()->routeIs('features')) ? 'active' : '' }}">
                    <a class="nav-link {{ (request()->routeIs('features')) ? 'active' : '' }}" href="{{ route('features') }}">FEATURES</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        BROKERS
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
                        @foreach ($all_brokers as $all_broker)
                            <a class="dropdown-item" href="{{route('web_broker',$all_broker->id)}}">{{$all_broker->name}}</a>
                        
                        @endforeach
                    </div>
                </li>

                <li class="nav-item {{ (request()->routeIs('support')) ? 'active' : '' }}">
                    <a class="nav-link {{ (request()->routeIs('support')) ? 'active' : '' }}" href="{{ route('support') }}">SUPPORT</a>
                </li>


                <li class="nav-item {{ (request()->routeIs('contact')) ? 'active' : '' }}">
                    <a class="nav-link {{ (request()->routeIs('contact')) ? 'active' : '' }}" href="{{ route('contact') }}">CONTACT</a>
                </li>



 <!-- Button trigger modal -->


            </ul>
            {{-- <ul>
                @if (Route::has('login'))
                <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
                    @auth
                    <a href="{{ route('home') }}"><button type="button" class="btn btn-primary btn_sign_modal_nav" >
                            DASHBOARD
                        </button></a>
                    @else
                    <li>
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-primary btn_sign_modal_nav" data-toggle="modal"
                            data-target="#loginexampleModal">
                           login
                        </button>
                    </li>
                    @endauth
                </div>
                @endif

            </ul> --}}
        </div>
    </div>
</nav>
    <section class="oldPageContainer">
        <h2 class="heading">Your personal data is completely safe with us</h2>

        <section class="privacy-policy-content">
            <h3>Privacy Policy</h3>

            <p>At FxCashbacks, we care about the privacy of your data and are
            committed to protecting it. This Privacy Policy explains what information we collect
            about you and why, what we do with that information, and how we handle that
            information. Throughout this policy, when we write “FxCashbacks” “we” or “us,” we’re referring to FxCashbacks Ltd.,
            incorporated in Dominica, and our subsidiaries and other affiliates.</p>

            <p>What is the scope of this Privacy Policy?</p>

            <p>This Privacy Policy (“Policy”) is incorporated into FxCashbacks <a href="/terms-of-use">Terms of
            Service</a> and <a href="/risk-disclosure">Risk Disclosure</a> applies to
            the information obtained by us through your use of FxCashbacks Site, Software, and Services
            (“Information”) as described in this Policy. Capitalised terms used in this Policy and not otherwise
            defined shall have the meanings provided for those terms in the Terms of Service.</p>

            <h3>Information collection and use</h3>

            <p>What Information does FxCashbacks collect about me?
            When you interact with our Site, Software, and/or Services, we collect Information that,
            alone or in combination with other data, could be used to identify you (“Personal Data”).
            Some of the Information we collect is stored in a manner that cannot be linked back to you
            (“Non-Personal Data”).</p>

            <h4>Information you provide us when you create an account</h4>

            <p>When you sign up for or use our Services, you voluntarily give us certain Personal Data, including:</p>
            <ul>
                <li>Your username, email address, and contact and language preferences. If you log
                into FxCashbacks with a social networking credential, such as with your Facebook or
                Google+ account, we will ask permission to access basic information from that
                account, such as your name and email address. You can stop sharing that
                information with us at any time by removing FxCashbacks access to that account.</li>
                <li>Your payment information, if you are a paying customer. This is required to
                complete a commercial transaction on the Site. We use this information to enable
                and fulfill your transaction. If you choose to use Worldpay to finalize and pay for
                your order, you will provide your credit card number directly to Worldpay. The
                privacy policy of Worldpay will apply to the information you provide on the
                Worldpay website.</li>
            </ul>

            <h4>Other Information we collect</h4>

            <p>We collect this Information as you use the Site, Software, and/or Services:</p>
            <ul>
                <li>UserContent. Thisconsistsofalltext, documents, orothercontentorinformation
                uploaded, entered, or otherwise transmitted by you in connection with your use of
                the Services and/or Software.</li>
            </ul>

            <h4>Automatically collected Information</h4>

            <p>Certain data about the devices you use to connect with FxCashbacks and your use of the Site,
            Software, and/or Services are automatically logged in our systems, including:</p>

            <ul>
                <li>Location information. This is the geographic area where you use your computer and
                mobile devices (as indicated by an Internet Protocol [IP] address or similar identifier)
                when interacting with our Site, Software, and/or Services.</li>
                <li>Log data. As with most websites and technology services delivered over the
                internet, our servers automatically collect data when you access or use our Site,
                Software, and/or Services and record it in log files. This log data may include the IP
                address, browser type and settings, the date and time of use, information about
                browser configuration, language preferences, and cookie data.</li>
                <li>Usage information. This is information about the FxCashbacks Site, Software, and/or Services
                you use and how you use them. We may also obtain data from our third-party
                partners and service providers to analyze how users use our Site, Software, and/or
                Services. For example, we will know how many users access a specific page on the
                Site and which links they clicked on. We use this aggregated information to better
                understand and optimize the Site.</li>
                <li>Device information. These are data from your computer or mobile device, such as
                the type of hardware and software you are using (for example, your operating
                system and browser type), as well as unique device identifiers for devices that are
                using FxCashbacks Software.</li>
                <li>Cookies. Data obtained from cookies are described in the “Does FxCashbacks use cookies?”
                section and in our cookie policy.</li>
            </ul>

            <h4>How does FxCashbacks use my Information?</h4>

            <ul>
                <li>to help us administer our Site, Software, and/or Services, authenticate users for
                security purposes, provide personalized user features and access, process
                transactions, conduct research, develop new features, and improve the features,
                algorithms, and usability of our Site, Software, and/or Services.</li>
                <li>to communicate with you about your use of our Site, Software, and/or Services,
                product announcements, and software updates, as well as respond to your requests
                for assistance, including providing account verification support if you’re having
                difficulty accessing your account.</li>
                <li>to send you direct marketing emails and special offers about FxCashbacks, from which you
                can unsubscribe at any time. If you are located in the European Economic Area
                (EEA), we will only send you marketing information if you consent to us doing so at
                the time you create your account or any point thereafter.</li>
                <li>to display User Content associated with your account and make sure it is available to
                you when you use our Services</li>
                <li>to calculate aggregate statistics on the number of unique devices using our Site,
                Software, and/or Services, and to detect and prevent fraud and misuse of those.</li>
            </ul>

            <h4>Does FxCashbacks review User Content?</h4>

            <p>As a rule, FxCashbacks employees do not monitor or view your User Content stored in or transferred
            through our Site, Software, and/or Services, but it may be viewed if we believe the Terms of
            Service have been violated and confirmation is required, if we need to do so to respond to
            your requests for user support, if we otherwise determine that we have an obligation to
            review it as described in the Terms of Service, or to improve our algorithms as described in the User
            Content section of our Terms of Service. In addition, if you request our human proofreading
            services, our proofreaders may also read the User Content you submit for this specific
            service, as necessary to perform our contract with you and for our legitimate business
            interests. Finally, your Information may be viewed where necessary to protect the rights,
            property, or personal safety of FxCashbacks and its users, or to comply with our legal obligations,
            such as responding to warrants, court orders, or other legal processes.</p>

            <h3>Information access and disclosure</h3>

            <h4>Does FxCashbacks share my Information?</h4>

            <p>We only disclose Personal Data to third parties when:</p>

            <ul>
                <li>We use service providers who assist us in meeting business operations needs,
                including hosting, delivering, and improving our Services. We also use service
                providers for specific services and functions, including email communication,
                customer support services, and analytics. These service providers may only access,
                process, or store Personal Data pursuant to our instructions and to perform their
                duties to us.</li>
                <li>We have your explicit consent to share your Personal Data.</li>
                <li>We believe it is necessary to investigate potential violations of the Terms of Service,
                to enforce those Terms of Service, or where we believe it is necessary to investigate,
                prevent, or take action regarding illegal activities, suspected fraud, or potential
                threats against persons, property, or the systems on which we operate our Site,
                Software, and/or Services.</li>
                <li>We determine that the access, preservation, or disclosure of your Personal Data is
                required by law to protect the rights, property, or personal safety of FxCashbacks and users
                of our Site, Software, and/or Services, or to respond to lawful requests by public
                authorities, including national security or law enforcement requests.</li>
                <li>We need to do so in connection with a merger, acquisition, bankruptcy,
                reorganization, sale of some or all of our assets or stock, public offering of securities,
                or steps in consideration of such activities (e.g., due diligence). In these cases some
                or all of your Personal Data may be shared with or transferred to another entity,
                subject to this Privacy Policy.</li>
            </ul>

            <p>We may disclose Non-Personal Data publicly and to third parties – for example, in public
            reports about financial trading, to partners under agreement with us, or as part of progress
            reports we may provide to users.<br>
            Through the use of cookies, we help deliver advertisements for relevant FxCashbacks products and
            services to you.<br>
            FxCashbacks does not share your Personal Data with third parties for the purpose of enabling them
            to deliver their advertisements to you.</p>

            <h4>Does FxCashbacks sell or rent my Personal Data?</h4>
            <p>No, FxCashbacks does not sell or rent your Personal Data.</p>

            <h4>Does FxCashbacks use cookies?</h4>
            <p>Cookies are small text files stored on your device and used by web browsers to deliver
            personalized content and remember logins and account settings. FxCashbacks uses cookies and
            similar technologies, including tracking pixels and web beacons, to collect usage and
            analytic data that helps us provide our Site, Software, and/or Services to you, as well as to
            help deliver ads for relevant FxCashbacks products and services to you when you visit certain pages
            on the Site and then visit certain third-party sites. For more information on cookies and how
            FxCashbacks uses them, please see our cookie Policy. Our products currently do not respond to Do
            Not Track requests.</p>

            <h4>How do third-party apps and plugins work?</h4>
            <p>Some third-party applications and services that work with us may ask for permission to
            access your Information. Those applications will provide you with notice and request your
            consent in order to obtain such access or information. Please consider your selection of
            such applications and services, and your permissions, carefully.
            Some third parties’ embedded content or plugins on our Site and/or Software, such as
            Facebook “Like” buttons, may allow their operators to learn that you have visited the Site,
            and they may combine this knowledge with other data they have collected about your visits
            to other websites or online services that can identify you.
            Data collected by third parties through these apps and plugins is subject to each parties’
            own policies. We encourage you to read those policies and understand how other
            companies use your data.</p>

            <h4>Will FxCashbacks send me emails?</h4>
            <p>From time to time, we may want to contact you with information about product
            announcements, software updates, and special offers. We also may want to contact you
            with information about products and services from our business partners. You may opt out
            of such communications at any time by clicking the “unsubscribe” link found within FxCashbacks
            emails and changing your contact preferences. All FxCashbacks account holders will continue to
            receive transactional messages related to our Services, even if you unsubscribe from
            promotional emails.</p>

            <h4>Does FxCashbacks collect information from children?</h4>
            <p>FxCashbacks does not knowingly collect personal information from children under the age of 18. If
            we determine we have collected personal information from a child younger than 18 years of
            age, we will take reasonable measures to remove that information from our systems. If you
            are under the age of 18, please do not submit any personal information through the Site,
            Service, and/or Software. We encourage parents and legal guardians to monitor their
            children’s Internet usage and to help enforce this Policy by instructing their children never
            to provide personal information through the Site, Service, and/or Software without their
            permission.</p>

            <h3>Data storage, transfer, retention, and deletion</h3>

            <h4>Where is my Information stored?</h4>

            <p>Information submitted to FxCashbacks will be transferred to, processed, and stored in the United
            Kingdom. When you use the Software on your computing device, User Content you save
            will be stored locally on that device and synced with our servers. If you post or transfer any
            Information to or through our Site, Software, and/or Services, you are agreeing to such
            Information, including Personal Data and User Content, being hosted and accessed in the
            United Kingdom.</p>

            <h4>How secure is my Information?</h4>
            <p>FxCashbacks is committed to protecting the security of your Information and takes reasonable
            precautions to protect it. However, Internet data transmissions, whether wired or wireless,
            cannot be guaranteed to be 100% secure, and as a result, we cannot ensure the security of
            Information you transmit to us, including Personal Data and User Content; accordingly, you
            acknowledge that you do so at your own risk.
            We use industry-standard encryption to protect your data in transit. This is commonly
            referred to as transport layer security (“TLS”) or secure socket layer (“SSL”) technology.</p>

            <p>Once we receive your data, we protect it on our servers using a combination of technical,
            physical, and logical security safeguards. The security of the data stored locally in any of our
            Software installed on your computing device requires that you make use of the security
            features of your device. We recommend that you take the appropriate steps to secure all
            computing devices that you use in connection with our Site, Software, and Services.</p>

            <p>If FxCashbacks learns of a security system breach, we may attempt to notify you and provide
            information on protective steps, if available, through the email address that you have
            provided to us or by posting a notice on the Site. Depending on where you live, you may
            have a legal right to receive such notices in writing.</p>

            <h4>How can I delete my Personal Data from FxCashbacks?</h4>
            <p>You can remove your Personal Data from FxCashbacks at any time by contacting us, and requesting
            FxCashbacks to remove your data from our systems. To contact us, visit: <a href="/contact-us">https://FxCashbacks.co/contact-us</a></p>

            <h4>How long is Personal Data retained?</h4>
            <p>You can remove your Personal Data from FxCashbacks at any time by deleting your account as
            described above. However, we may keep some of your Personal Data for as long as
            reasonably necessary for our legitimate business interests, including fraud detection and
            prevention and to comply with our legal obligations including tax, legal reporting, and
            auditing obligations.</p>

            <h4>What happens if FxCashbacks closes my account?</h4>
            <p>If FxCashbacks closes your account due to your violation of the Terms of Service, then you may
            contact FxCashbacks to request deletion of your data. FxCashbacks will evaluate such requests on a case by
            case basis, pursuant to our legal obligations.</p>

            <h4>For EEA users</h4>
            <p>FxCashbacks uses, processes, and stores Personal Data, including those listed in the “what
            Information does FxCashbacks collect about me?” section, as necessary to perform our contract with
            you, and based on our legitimate interests in order to provide the Services. We rely on your
            consent to process Personal Data to send promotional emails and to place cookies on your
            devices. In some cases, FxCashbacks may process Personal Data pursuant to legal obligation or to
            protect your vital interests or those of another person.</p>

            <h4>What rights do I have, and how can I exercise them?</h4>
            <p>Individuals located in the European Economic Area (EEA) have certain rights in respect to
            their personal information, including the right to access, correct, or delete Personal Data we
            process through your use of the Site, Software, and/or Services. If you’re a user based in the
            EEA, you can:</p>

            <ul>
                <li>Request a Personal Data report by submitting a support ticket through
                info@FxCashbacks.co This report will include the Personal Data we have about you,
                provided to you in a structured, commonly used, and portable format. Please note
                that FxCashbacks may request additional information from you to verify your identity before
                we disclose any information.</li>
                <li>Have your Personal Data corrected or deleted. Some Personal Data can be updated by you:</li><ul>
                <li>Object to us processing your Personal Data. You can ask us to stop using your
                Personal Data, including when we use your Personal Data to send you marketing
                emails. We only send marketing communications to users located in the EEA with
                your prior consent, and you may withdraw your consent at any time by clicking the
                “unsubscribe” link found within FxCashbacks emails and changing your contact preferences.
                Please note you will continue to receive transactional messages related to our
                Services, even if you unsubscribe from marketing emails.</li>
                <li>Complain to a regulator. If you’re based in the EEA and think that we haven’t
                complied with data protection laws, you have a right to lodge a complaint with your
                local supervisory authority.</li></ul>
            </ul>

            <h4>Who is FxCashbacks data processor?</h4>
            <p>Infusionsoft has been appointed as FxCashbacks data processor in the EEA for data protection
            matters, pursuant to Article 27 of the General Data Protection Regulation of the European
            Union.</p>

            <h4>Will this Privacy Policy ever change?</h4>
            <p>As FxCashbacks evolves, we may need to update this Policy to keep pace with changes in our Site,
            Software, and Services, our business, and laws applicable to us and you. We will, however,
            always maintain our commitment to respect your privacy. We will notify you of any material
            changes that impact your rights under this Policy by email (to your most recently provided
            email address) or post any other revisions to this Policy, along with their effective date, in an
            easy-to- find area of the Site, so we recommend that you periodically check back here to
            stay informed of any changes. Please note that your continued use of FxCashbacks after any change
            means that you agree with, and consent to be bound by, the new Policy. If you disagree
            with any changes in this Policy and do not wish your information to be subject to it, you will
            need to contact FxCashbacks and your data will be deleted.</p>

            <h3>Contact us</h3>

            <p>You may contact us with any questions relating to this Privacy Policy by submitting a
            help desk or by postal mail at: <a href="/contact">https://FxCashbacks.com/contact</a></p>
        </section>
    </section>


    <footer class="section footer-classic context-dark bg-image bg_footer_gra">
        <div class="container">
            <div class="row">

                <div class="col-md-7">
                    <h5 class="hed_foter"><a class="{{ (request()->routeIs('terms')) ? 'active' : '' }}" href="{{ route('terms') }}"> Terms of Service </a></h5>
                    <h5 class="hed_foter"><a class="{{ (request()->routeIs('privacy')) ? 'active' : '' }}" href="{{ route('privacy') }}"> Privacy Policy </a></h5>
                    <h5 class="hed_foter"><a class="{{ (request()->routeIs('support')) ? 'active' : '' }}" href="{{ route('support') }}"> FAQ </a></h5>
                    <h5 class="hed_foter"> <a class=" {{ (request()->routeIs('contact')) ? 'active' : '' }}"
                        href="{{ route('contact') }}"> Contact Us </a></h5>
                    {{-- <p class="para_foter">Reporting of Rebates High Rebate Rates<br> Highly Efficient custome</p> --}}
                </div>

                <div class="col-md-5">
                    <h5 class="hed_foter">Join Our Newsletter</h5>
                    <div class="input-group">
                        <input type="search" class="form-control rounded" placeholder="Search" aria-label="Search"
                            aria-describedby="search-addon" />
                        <button type="button" class="btn btn-outline-primary">Submit</button>
                    </div>
                </div>


                {{-- <div class="container">
                    <div class="row pd">
                        <div class="seprater"></div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">

                        <div class="col-md-6">
                            <a {{ (request()->routeIs('terms')) ? 'active' : '' }}" href="{{ route('terms') }}">
                                <p class="text-center">Terms of Service</p>

                        </div>



                        <div class="col-md-6">
                            <a {{ (request()->routeIs('privacy')) ? 'active' : '' }}" href="{{ route('privacy') }}">
                                <p class="text-center">Privacy Policy</p>


                        </div>
                    </div>
                </div> --}}

            </div>
        </div>

    </footer>
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>
<style>

.oldPageContainer {
    margin: 0 auto;
    padding: 90px 0;



    width: 700px;}
    .oldPageContainer  .heading {
    color: #343434;
    font-size: 32px;
    font-family: Open Sans Condensed,Open Sans,Helvetica,Arial,sans-serif;
    font-weight: 700;
    text-shadow: 0 1px 0 hsl(0deg 0% 100% / 20%);
    text-transform: uppercase;
    text-align: center;
}
.oldPageContainer section.terms h3 {
    margin-top: 50px;


}

section.terms p {
    margin-top: 20px;
    text-align: justify;


}
@media screen and (max-width: 480px){
    .oldPageContainer h2.heading {
    font-size: 26px;
    margin: 0 auto;
    width: 90%;
}
.oldPageContainer {
    margin: 0 auto;
    box-sizing: border-box;
    padding: 0px;
   width: 100%;
   padding: 10px;
   padding-top: 45px;


}
}


</style>
</html>





