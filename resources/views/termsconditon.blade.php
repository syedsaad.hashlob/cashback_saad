<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
<!-- Favicon -->
<link href="{{ url('')}}/assets/img/favicon.png" rel="icon">
<link href="{{ url('')}}/assets/img/apple-touch-icon.png" rel="apple-touch-icon">
    <title>FXCASHBACKS</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">


    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/modern_business.css" rel="stylesheet">
    <link href="css/media_query.css" rel="stylesheet">

    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>


   <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-249312996-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'UA-249312996-1');
    </script>

</head>


<body>
@php
    $all_brokers = getBrokers();
@endphp
 <!-- Navigation -->
 <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            <img class='logo_f' src="{{url('')}}/images/logo.png" >
        </a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
            data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="\ navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active {{ (request()->routeIs('Homepage')) ? 'active' : '' }}">
                    <a class="nav-link {{ (request()->routeIs('Homepage')) ? 'active' : '' }}" href="{{ route('Homepage') }}">HOME</a>
                </li>

                <li class="nav-item {{ (request()->routeIs('how-its-work')) ? 'active' : '' }}">
                    <a class="nav-link {{ (request()->routeIs('how-its-work')) ? 'active' : '' }}" href="{{ route('how-its-work') }}">HOW IT WORKS</a>
                </li>


                <li class="nav-item {{ (request()->routeIs('features')) ? 'active' : '' }}">
                    <a class="nav-link {{ (request()->routeIs('features')) ? 'active' : '' }}" href="{{ route('features') }}">FEATURES</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        BROKERS
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
                        @foreach ($all_brokers as $all_broker)
                            <a class="dropdown-item" href="{{route('web_broker',$all_broker->id)}}">{{$all_broker->name}}</a>
                        
                        @endforeach
                    </div>
                </li>


                <li class="nav-item {{ (request()->routeIs('support')) ? 'active' : '' }}">
                    <a class="nav-link {{ (request()->routeIs('support')) ? 'active' : '' }}" href="{{ route('support') }}">SUPPORT</a>
                </li>


                <li class="nav-item {{ (request()->routeIs('contact')) ? 'active' : '' }}">
                    <a class="nav-link {{ (request()->routeIs('contact')) ? 'active' : '' }}" href="{{ route('contact') }}">CONTACT</a>
                </li>



 <!-- Button trigger modal -->


            </ul>
            {{-- <ul>
                @if (Route::has('login'))
                <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
                    @auth
                    <a href="{{ route('home') }}"><button type="button" class="btn btn-primary btn_sign_modal_nav" >
                            DASHBOARD
                        </button></a>
                    @else
                    <li>
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-primary btn_sign_modal_nav" data-toggle="modal"
                            data-target="#loginexampleModal">
                           login
                        </button>
                    </li>
                    @endauth
                </div>
                @endif

            </ul> --}}
        </div>
    </div>
</nav>
    <section id="howitworks" class="oldPageContainer">

        <h2 class="heading">FxCashbacks .: TERMS OF USE</h2>

        <section class="oneHIW terms">

            <h3>AGREEMENT BETWEEN USER AND FxCashbacks</h3>

            <p>The FxCashbacks Web Site is comprised of various Web pages operated by FxCashbacks.</p>

            <p>The FxCashbacks Web Site is offered to you conditioned on your acceptance without modification of the terms, conditions, and notices contained herein. Your use of the FxCashbacks Web Site constitutes your agreement to all such terms, conditions, and notices.</p>

            <p>A Customer may register only one profile on FxCashbacksâ€™s website. In case of multiple profiles registered by one Customer, FxCashbacks shall be entitled to block all but one such Profiles.</p>

            <p><strong>The Customer has read the Risk Disclosure Statement available on this website.</strong></p>

            <h3>MODIFICATION OF THESE TERMS OF USE</h3>

            <p>FxCashbacks reserves the right to change the terms, conditions, and notices under which the FxCashbacks Web Site is offered, including but not limited to the charges associated with the use of the FxCashbacks Web Site.</p>

            <h3>PROMOTIONS AND BONUSES</h3>

            <p>Promotions and bonuses offered by a third party through FxCashbacks are the responsibility of the third party.  FxCashbacks will not be held liable for a third party failing to deliver, or delivering inadequately, on bonus or promotional offers.</p>

            <h3>HIGH RISK INVESTMENT</h3>

            <p>Trading foreign exchange on margin carries a high level of risk, and may not be suitable for all investors. The high degree of leverage can work against you as well as for you. Before deciding to trade foreign exchange you should carefully consider your investment objectives, level of experience, and risk appetite. The possibility exists that you could sustain a loss of some or all of your initial investment and therefore you should not invest money that you cannot afford to lose. You should be aware of all the risks associated with foreign exchange trading, and seek advice from an independent financial advisor if you have any doubts.</p>

            <h3>MARKET OPINIONS</h3>

            <p>Any opinions, signals, news, research, analyses, prices, or other information contained on this website is provided as general market commentary, and does not constitute investment advice. The owners, operators, and affiliates of FxCashbacks will not accept liability for any loss or damage, including without limitation to, any loss of profit, which may arise directly or indirectly from use of or reliance on such information.</p>

            <h3>INTERNET TRADING RISKS</h3>

            <p>There are risks associated with utilizing any Internet-based trading service including, but not limited to, the failure of hardware, software, and Internet connection. Since FxCashbacks owners do not control signal power, its reception or routing via Internet, configuration of your equipment or reliability of its connection, we cannot be responsible for communication failures, distortions or delays when trading via the Internet.</p>

            <h3>ACCURACY OF INFORMATION</h3>

            <p>The content on this website is subject to change at any time without notice, and is provided for the sole purpose of assisting traders to make independent decisions. FxCashbacks owners, operators, and affiliates have taken reasonable measures to ensure the accuracy of the information on the website, however, do not guarantee its accuracy, and will not accept liability for any loss or damage which may arise directly or indirectly from the content or your inability to access the website, for any delay in or failure of the transmission or the receipt of any instruction or notifications sent through this website.</p>

            <h3>DISTRIBUTION</h3>

            <p>This site is not intended for distribution, or use by, any person in any country where such distribution or use would be contrary to local law or regulation. None of the services or investments referred to in this website are available to persons residing in any country where the provision of such services or investments would be contrary to local law or regulation. It is the responsibility of visitors to this website to ascertain the terms of and comply with any local law or regulation to which they are subject. Plus, we do not offer any of our services to people in Commonwealth of Dominica. It is the customer's responsibility not to register from Commonwealth of Dominica.</p>

            <h3>LINKS TO THIRD PARTY SITES</h3>

            <p>The FxCashbacks Web Site may contain links to other Web Sites ("Linked Sites"). The Linked Sites are not under the control of FxCashbacks and FxCashbacks is not responsible for the contents of any Linked Site, including without limitation any link contained in a Linked Site, or any changes or updates to a Linked Site. FxCashbacks is not responsible for webcasting or any other form of transmission received from any Linked Site. FxCashbacks is providing these links to you only as a convenience, and the inclusion of any link does not imply endorsement by FxCashbacks of the site or any association with its operators.</p>

            <h3>NO UNLAWFUL OR PROHIBITED USE</h3>

            <p>As a condition of your use of the FxCashbacks Web Site, you warrant to FxCashbacks that you will not use the FxCashbacks Web Site for any purpose that is unlawful or prohibited by these terms, conditions, and notices. You may not use the FxCashbacks Web Site in any manner which could damage, disable, overburden, or impair the FxCashbacks Web Site or interfere with any other party's use and enjoyment of the FxCashbacks Web Site. You may not obtain or attempt to obtain any materials or information through any means not intentionally made available or provided for through the FxCashbacks Web Sites.</p>

            <h3>FX CASH BACK PAYMENTS</h3>

            <p>FxCashbacks diligently pays all clients according to either the currently listed rates or agreed upon rate. Rebates and bonus points for products and services that allow refunds are paid after expiration of the refund period. Any custom quotes/offers must be approved in writing. Certain situations can occur that would prevent payments from being made i.e. if a broker refuses to pay, or if an individual has provided incorrect payment information. FxCashbacks owners, operators and affiliates shall have no liability or obligation under any circumstance to issue client payments for any reason, and all payments are issued at our sole discretion. If at any time FxCashbacks becomes unable to collect funds from any broker(s) or contracted firms/entities then FxCashbacks reserves the right not to pay clients. FxCashbacks shall not be indebted to any client or other party for any reason regardless of whether FxCashbacks has received compensation based on actions or agreements executed by the client. Client agrees to hold FxCashbacks owners, operators, and affiliates harmless from all claims, demands, proceedings, suits, and actions, and all liabilities, losses, and expenses associated with fxcash back payments or lack thereof, including payments made based on client instructions and profile settings.</p>

            <p>If cashback was paid but FxCashbacks does not receive commissions from your forex broker, FxCashbacks has the right to ask for a refund from you. Thus, the client has to be sure, that all his or her trading is legal and will not be involved in any type of fraud. If a clients trading were canceled and FxCashbacks do not collect any cashback from the broker, the withdrew money has to be refunded by the user in 5 business days.</p>

            <p>Cashbacks shown on users' profile is only informational and could be modified without any notice to the client. FxCashbacks has the right to change cashbacks and rebates on users' profiles without notice. Plus, FxCashbacks has the right to remove cashback from users's profile if FxCashbacks do not get the actual money from the trades. (This could happen for example if a client trades arbitrage or makes any other illegal trades)</p>

            <h3>ACCOUNT INACTIVITY</h3>

            <p>If a user is inactive (not logged in) for longer than a year (365 days) FxCashbacks holds the right to set the balance of his/her account to 0 (zero) after 365 days. FxCashbacks is obliged to send a notice to the client's email address (which was used at the registration) a week earlier. Also, FxCashbacks is obliged to send a notice about account balance removal 1 day before the action.</p>

            <h3>FEES INCURRED RELATED TO PAYMENTS</h3>

            <p>Certain reasonable fees may be incurred by clients as a result of payments. Wire fees, paypal fees, and other similar costs and the labor associated with these services may be charged to the clients account and deducted from the payment amount.</p>

            <h3>USE OF COMMUNICATION SERVICES</h3>

            <p>The FxCashbacks Web Site may contain bulletin board services, chat areas, news groups, forums, communities, personal web pages, calendars, and/or other message or communication facilities designed to enable you to communicate with the public at large or with a group (collectively, "Communication Services"), you agree to use the Communication Services only to post, send and receive messages and material that are proper and related to the particular Communication Service. By way of example, and not as a limitation, you agree that when using a Communication Service, you will not:</p>
            <ul>
                <li>Defame, abuse, harass, stalk, threaten or otherwise violate the legal rights (such as rights of privacy and publicity) of others.</li>
                <li>Publish, post, upload, distribute or disseminate any inappropriate, profane, defamatory, infringing, obscene, indecent or unlawful topic, name, material or information.</li>
                <li>Upload files that contain software or other material protected by intellectual property laws (or by rights of privacy of publicity) unless you own or control the rights thereto or have received all necessary consents.</li>
                <li>Upload files that contain viruses, corrupted files, or any other similar software or programs that may damage the operation of another's computer.</li>
                <li>Advertise or offer to sell or buy any goods or services for any business purpose, unless such Communication Service specifically allows such messages.</li>
                <li>Conduct or forward surveys, contests, pyramid schemes or chain letters.</li>
                <li>Download any file posted by another user of a Communication Service that you know, or reasonably should know, cannot be legally distributed in such manner.</li>
                <li>Falsify or delete any author attributions, legal or other proper notices or proprietary designations or labels of the origin or source of software or other material contained in a file that is uploaded.</li>
                <li>Restrict or inhibit any other user from using and enjoying the Communication Services.</li>
                <li>Violate any code of conduct or other guidelines which may be applicable for any particular Communication Service.</li>
                <li>Harvest or otherwise collect information about others, including e-mail addresses, without their consent.</li>
                <li>Violate any applicable laws or regulations.
            </li></ul>

            <p>FxCashbacks has no obligation to monitor the Communication Services. However, FxCashbacks reserves the right to review materials posted to a Communication Service and to remove any materials in its sole discretion. FxCashbacks reserves the right to terminate your access to any or all of the Communication Services at any time without notice for any reason whatsoever.</p>

            <p>FxCashbacks reserves the right at all times to disclose any information as necessary to satisfy any applicable law, regulation, legal process or governmental request, or to edit, refuse to post or to remove any information or materials, in whole or in part, in FxCashbacks's sole discretion.</p>

            <p>Always use caution when giving out any personally identifying information about yourself or your children in any Communication Service. FxCashbacks does not control or endorse the content, messages or information found in any Communication Service and, therefore, FxCashbacks specifically disclaims any liability with regard to the Communication Services and any actions resulting from your participation in any Communication Service. Managers and hosts are not authorized FxCashbacks spokespersons, and their views do not necessarily reflect those of FxCashbacks.</p>

            <p>Materials uploaded to a Communication Service may be subject to posted limitations on usage, reproduction and/or dissemination. You are responsible for adhering to such limitations if you download the materials.</p>


            <h3>MATERIALS PROVIDED TO FxCashbacks OR POSTED AT ANY FxCashbacks WEB SITE</h3>

            <p>FxCashbacks does not claim ownership of the materials you provide to FxCashbacks (including feedback and suggestions) or post, upload, input or submit to any FxCashbacks Web Site or its associated services (collectively "Submissions"). However, by posting, uploading, inputting, providing or submitting your Submission you are granting FxCashbacks, its affiliated companies and necessary sublicensees permission to use your Submission in connection with the operation of their Internet businesses including, without limitation, the rights to: copy, distribute, transmit, publicly display, publicly perform, reproduce, edit, translate and reformat your Submission; and to publish your name in connection with your Submission.</p>

            <p>No compensation will be paid with respect to the use of your Submission, as provided herein. FxCashbacks is under no obligation to post or use any Submission you may provide and may remove any Submission at any time in FxCashbacks's sole discretion.</p>

            <p>By posting, uploading, inputting, providing or submitting your Submission you warrant and represent that you own or otherwise control all of the rights to your Submission as described in this section including, without limitation, all the rights necessary for you to provide, post, upload, input or submit the Submissions.</p>


            <h3>LIABILITY DISCLAIMER</h3>

            <p>THE INFORMATION, SOFTWARE, PRODUCTS, AND SERVICES INCLUDED IN OR AVAILABLE THROUGH THE FxCashbacks WEB SITE MAY INCLUDE INACCURACIES OR TYPOGRAPHICAL ERRORS. CHANGES ARE PERIODICALLY ADDED TO THE INFORMATION HEREIN. FxCashbacks AND/OR ITS SUPPLIERS MAY MAKE IMPROVEMENTS AND/OR CHANGES IN THE FxCashbacks WEB SITE AT ANY TIME. ADVICE RECEIVED VIA THE FxCashbacks WEB SITE SHOULD NOT BE RELIED UPON FOR PERSONAL, MEDICAL, LEGAL OR FINANCIAL DECISIONS AND YOU SHOULD CONSULT AN APPROPRIATE PROFESSIONAL FOR SPECIFIC ADVICE TAILORED TO YOUR SITUATION.</p>

            <p>FxCashbacks AND/OR ITS SUPPLIERS MAKE NO REPRESENTATIONS ABOUT THE SUITABILITY, RELIABILITY, AVAILABILITY, TIMELINESS, AND ACCURACY OF THE INFORMATION, SOFTWARE, PRODUCTS, SERVICES AND RELATED GRAPHICS CONTAINED ON THE FxCashbacks WEB SITE FOR ANY PURPOSE. TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, ALL SUCH INFORMATION, SOFTWARE, PRODUCTS, SERVICES AND RELATED GRAPHICS ARE PROVIDED "AS IS" WITHOUT WARRANTY OR CONDITION OF ANY KIND. FxCashbacks AND/OR ITS SUPPLIERS HEREBY DISCLAIM ALL WARRANTIES AND CONDITIONS WITH REGARD TO THIS INFORMATION, SOFTWARE, PRODUCTS, SERVICES AND RELATED GRAPHICS, INCLUDING ALL IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT.</p>

            <p>TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT SHALL FxCashbacks AND/OR ITS SUPPLIERS BE LIABLE FOR ANY DIRECT, INDIRECT, PUNITIVE, INCIDENTAL, SPECIAL, CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF USE, DATA OR PROFITS, ARISING OUT OF OR IN ANY WAY CONNECTED WITH THE USE OR PERFORMANCE OF THE FxCashbacks WEB SITE, WITH THE DELAY OR INABILITY TO USE THE FxCashbacks WEB SITE OR RELATED SERVICES, THE PROVISION OF OR FAILURE TO PROVIDE SERVICES, OR FOR ANY INFORMATION, SOFTWARE, PRODUCTS, SERVICES AND RELATED GRAPHICS OBTAINED THROUGH THE FxCashbacks WEB SITE, OR OTHERWISE ARISING OUT OF THE USE OF THE FxCashbacks WEB SITE, WHETHER BASED ON CONTRACT, TORT, NEGLIGENCE, STRICT LIABILITY OR OTHERWISE, EVEN IF FxCashbacks OR ANY OF ITS SUPPLIERS HAS BEEN ADVISED OF THE POSSIBILITY OF DAMAGES. BECAUSE SOME STATES/JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, THE ABOVE LIMITATION MAY NOT APPLY TO YOU. IF YOU ARE DISSATISFIED WITH ANY PORTION OF THE FxCashbacks WEB SITE, OR WITH ANY OF THESE TERMS OF USE, YOUR SOLE AND EXCLUSIVE REMEDY IS TO DISCONTINUE USING THE FxCashbacks WEB SITE.</p>


            <h3>SERVICE CONTACT : info@FxCashbacks.co</h3>


            <h3>TERMINATION/ACCESS RESTRICTION</h3>

            <p>FxCashbacks reserves the right, in its sole discretion, to terminate your access to the FxCashbacks Web Site and the related services or any portion thereof at any time, without notice. GENERAL To the maximum extent permitted by law, this agreement is governed by the laws of the Budapest, Hungary and you hereby consent to the exclusive jurisdiction and venue of courts in Budapest, Hungary in all disputes arising out of or relating to the use of the FxCashbacks Web Site. Use of the FxCashbacks Web Site is unauthorized in any jurisdiction that does not give effect to all provisions of these terms and conditions, including without limitation this paragraph. You agree that no joint venture, partnership, employment, or agency relationship exists between you and FxCashbacks as a result of this agreement or use of the FxCashbacks Web Site. FxCashbacks's performance of this agreement is subject to existing laws and legal process, and nothing contained in this agreement is in derogation of FxCashbacks's right to comply with governmental, court and law enforcement requests or requirements relating to your use of the FxCashbacks Web Site or information provided to or gathered by FxCashbacks with respect to such use. If any part of this agreement is determined to be invalid or unenforceable pursuant to applicable law including, but not limited to, the warranty disclaimers and liability limitations set forth above, then the invalid or unenforceable provision will be deemed superseded by a valid, enforceable provision that most closely matches the intent of the original provision and the remainder of the agreement shall continue in effect. Unless otherwise specified herein, this agreement constitutes the entire agreement between the user and FxCashbacks with respect to the FxCashbacks Web Site and it supersedes all prior or contemporaneous communications and proposals, whether electronic, oral or written, between the user and FxCashbacks with respect to the FxCashbacks Web Site. A printed version of this agreement and of any notice given in electronic form shall be admissible in judicial or administrative proceedings based upon or relating to this agreement to the same extent an d subject to the same conditions as other business documents and records originally generated and maintained in printed form. It is the express wish to the parties that this agreement and all related documents be drawn up in English.</p>


            <h3>COPYRIGHT AND TRADEMARK NOTICES</h3>

            <p>All contents of the FxCashbacks Web Site are: Copyright by FxCashbacks and/or its suppliers. All rights reserved.</p>


            <h3>TRADEMARKS</h3>

            <p>The names of actual companies and products mentioned herein may be the trademarks of their respective owners.</p>

            <p>The example companies, organizations, products, people and events depicted herein are fictitious. No association with any real company, organization, product, person, or event is intended or should be inferred.</p>

            <p>Any rights not expressly granted herein are reserved.</p>

            <h3>NOTICES AND PROCEDURE FOR MAKING CLAIMS OF COPYRIGHT INFRINGEMENT</h3>

            <p>Pursuant to Title 17, United States Code, Section 512(c)(2), notifications of claimed copyright infringement under United States copyright law should be sent to Service Provider's Designated Agent. ALL INQUIRIES NOT RELEVANT TO THE FOLLOWING PROCEDURE WILL RECEIVE NO RESPONSE. See Notice and Procedure for Making Claims of Copyright Infringement.</p>


        </section>

    </section>














    <footer class="section footer-classic context-dark bg-image bg_footer_gra">
        <div class="container">
            <div class="row">

                <div class="col-md-7">
                    <h5 class="hed_foter"><a class="{{ (request()->routeIs('terms')) ? 'active' : '' }}" href="{{ route('terms') }}"> Terms of Service </a></h5>
                    <h5 class="hed_foter"><a class="{{ (request()->routeIs('privacy')) ? 'active' : '' }}" href="{{ route('privacy') }}"> Privacy Policy </a></h5>
                    <h5 class="hed_foter"><a class="{{ (request()->routeIs('support')) ? 'active' : '' }}" href="{{ route('support') }}"> FAQ </a></h5>
                    <h5 class="hed_foter"> <a class=" {{ (request()->routeIs('contact')) ? 'active' : '' }}"
                        href="{{ route('contact') }}"> Contact Us </a></h5>
                    {{-- <p class="para_foter">Reporting of Rebates High Rebate Rates<br> Highly Efficient custome</p> --}}
                </div>

                <div class="col-md-5">
                    <h5 class="hed_foter">Join Our Newsletter</h5>
                    <div class="input-group">
                        <input type="search" class="form-control rounded" placeholder="Search" aria-label="Search"
                            aria-describedby="search-addon" />
                        <button type="button" class="btn btn-outline-primary">Submit</button>
                    </div>
                </div>


                {{-- <div class="container">
                    <div class="row pd">
                        <div class="seprater"></div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">

                        <div class="col-md-6">
                            <a {{ (request()->routeIs('terms')) ? 'active' : '' }}" href="{{ route('terms') }}">
                                <p class="text-center">Terms of Service</p>

                        </div>



                        <div class="col-md-6">
                            <a {{ (request()->routeIs('privacy')) ? 'active' : '' }}" href="{{ route('privacy') }}">
                                <p class="text-center">Privacy Policy</p>


                        </div>
                    </div>
                </div> --}}

            </div>
        </div>

    </footer>
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>
<style>

.oldPageContainer {
    margin: 0 auto;
    padding: 90px 0;



    width: 700px;}
    .oldPageContainer  .heading {
    color: #343434;
    font-size: 32px;
    font-family: Open Sans Condensed,Open Sans,Helvetica,Arial,sans-serif;
    font-weight: 700;
    text-shadow: 0 1px 0 hsl(0deg 0% 100% / 20%);
    text-transform: uppercase;
    text-align: center;
}
.oldPageContainer section.terms h3 {
    margin-top: 50px;


}

section.terms p {
    margin-top: 20px;
    text-align: justify;


}
@media screen and (max-width: 480px){
    .oldPageContainer h2.heading {
    font-size: 26px;
    margin: 0 auto;
    width: 90%;
}
.oldPageContainer {
    margin: 0 auto;
    box-sizing: border-box;
    padding: 0px;
   width: 100%;
   padding: 10px;
   padding-top: 45px;


}
}

</style>
</html>





