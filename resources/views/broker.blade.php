<link href="{{ url('')}}/assets/img/favicon.png" rel="icon">
<link href="{{ url('')}}/assets/img/apple-touch-icon.png" rel="apple-touch-icon">

@extends('layouts.common')
<!-- Login Modal -->
<section id="login_modal">

    @if (Auth::check())
        
    
    <div class="modal fade" id="brokerexampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    Enter Account ID
                                </div>
                                <div class="panel-body">
                                    <form action="{{route('update_broker_account')}}" method="post">
                                        @csrf
                                        <div class="form-group">
                                            {{-- <h2>Broker</h2> --}}
                                        </div>
                                            <div class="form-group">
                                                <input id="account_id" name="user_account_id" placeholder="Account ID" type="text" class="form-control">
                                                <input type="hidden" name="broker_id" value="{{$broker->id}}">
                                            </div>

                                            <div class="form-group">
                                                <button id="signupSubmit" type="submit"
                                                    class="btn btn-info btn-block">Submit</button>
                                            </div>
                                    </form>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                </div>
            </div>
        </div>
    </div>
    @endif
    {{-- Payment Method Modal --}}
    @if (Auth::check())
        
    <div class="modal fade" id="paymentexampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    Transaction Amount
                                </div>
                                @php
                                    $user_account_id = \App\UserBrokerAccount::where([['user_id',Auth::user()->id],['broker_id',$broker->id]])->first();
                                    if(!empty($user_account_id)){
                                        $trade = getBrockerByIdAndAccountId($user_account_id->user_account_id,$broker->id);
                                    }
                                @endphp
                                <div class="panel-body">
                                    <form action="{{route('transaction.store')}}" method="post">
                                        @csrf
                                        <div class="form-group">
                                            {{-- <h2>Broker</h2> --}}
                                        </div>
                                            <span class="total_amount">Your Amount: ${{isset($trade) ?  $trade['sum_amount'] : 0}}</span>
                                            <hr>
                                            <div class="form-group">
                                                <input id="type_amount" name="amount" placeholder="Enter Amount" type="number" class="form-control">
                                                <input type="hidden" name="broker_id" value="{{$broker->id}}">
                                                <input type="hidden" name="payment_method_id" class="payment_method_id"  value="">
                                            </div>

                                            <div class="form-group">
                                                <button id="signupSubmit" type="submit" class="btn btn-info btn-block">Submit</button>
                                            </div>
                                    </form>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                </div>
            </div>
        </div>
    </div>
    @endif

</section>
<!-- Login Modal end -->
<section id="hero_inner_page" class="hero" bgcolor="green">

    <div class="inner">
        <div class="copy">

            <div class="container">
                <a href="{{$broker->web_link}}" target="_blank">
                    <div class="row">
                        <div class="col-md-2"> <img class='brand_lg' style="width: 220px;" src="{{url('')}}/uploads/{{$broker->image}}" ></div>
                        <div class="col-md-5 text_col">
                            <div class="row">
                                <div class="col-12">
                            <h2>{{$broker->name}} CASHBACK</h2>
                                </div>
                                <div class="col-12">
                                    <p>up to $3.06 / lot on EUR/USD. More info.</p>
                                        </div>
                            </div>
                        </div>
                        <div class="col-md-5 btn_col">
                            {{-- <button type="button" class="btn btn-primary">CASH BACK NEW ACCOUNT</button> --}}
                            <!-- Button trigger modal -->
                            {{-- <button type="button" class="btn btn-primary btn_sign_modal_nav" data-toggle="modal"
                            data-target="#brokerexampleModal">
                                CREATE NEW ACCOUNT
                            </button> --}}
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

<div class="container container_fixed">
    <br>
    <div class="row">
        @if (session('success'))
            <p class="col-md-12 alert alert-success">{{session('success')}}</p>
        @endif
        @if (session('error'))
            <p class="col-md-12 alert alert-danger">{{session('error')}}</p>
        @endif
    </div>
    <section id="broker_info_left" >

        <section id="bi_signup" class="oneinfo">

            <div class="forex-cashback-rates">
                <h4><span>%</span><em>Cashback rates</em> on {{{$broker->name}}} account types</h4>

                <p class="select-lvl">Select your possible level based on your monthly possible cashback volume. <a href="http://support.fxcashbacks.com/knowledge-base/the-cbc-cashback-system/"
                        class="hover-opac">Learn more about levels.</a></p>

                <div class="tables-container">


                    <table class="cbc-levels">
                        <tbody>
                            <tr>

                                <td class="cbc-lvl active">
                                    <a>
                                        <i class="fa fa-circle color-bronze" aria-hidden="true"></i>
                                        <strong class="lvl-name">
                                            Bronze
                                        </strong>
                                        <span class="lots-mo">
                                            to $5 cb/mo
                                        </span>
                                    </a>
                                </td>
                                <td class="cbc-lvl">
                                    <a>
                                        <i class="fa fa-circle color-silver" aria-hidden="true"></i>
                                        <strong class="lvl-name">
                                            Silver
                                        </strong>
                                        <span class="lots-mo">
                                            $5 - $30 cb/mo
                                        </span>
                                    </a>
                                </td>
                                <td class="cbc-lvl">
                                    <a>
                                        <i class="fa fa-circle color-gold" aria-hidden="true"></i>
                                        <strong class="lvl-name">
                                            Gold
                                        </strong>
                                        <span class="lots-mo">
                                            $30 - $100 cb/mo
                                        </span>
                                    </a>
                                </td>
                                <td class="cbc-lvl">
                                    <a>
                                        <i class="fa fa-circle color-platinum" aria-hidden="true"></i>
                                        <strong class="lvl-name">
                                            Platinum
                                        </strong>
                                        <span class="lots-mo">
                                            $100 - $370 cb/mo
                                        </span>
                                    </a>
                                </td>
                                <td class="cbc-lvl">
                                    <a>
                                        <i class="fa fa-circle color-diamond" aria-hidden="true"></i>
                                        <strong class="lvl-name">
                                            Diamond
                                        </strong>
                                        <span class="lots-mo">
                                            $370 - $1,600 cb/mo
                                        </span>
                                    </a>
                                </td>
                                <td class="cbc-lvl">
                                    <a>
                                        <i class="fa fa-circle color-master" aria-hidden="true"></i>
                                        <strong class="lvl-name">
                                            Master
                                        </strong>
                                        <span class="lots-mo">
                                            from $1,600 cb/mo
                                        </span>
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <div class="cb-rates-container">
                        <table class="table-cb-rates">
                            <thead>
                                <tr class="tr-heading">
                                    <th>&nbsp;</th>

                                    <th>Mini</th>
                                    <th>Classic</th>
                                    <th>ECN</th>
                                </tr>
                            </thead>

                            <tbody>


                                <tr>
                                    <td>
                                        <p><strong>Cashback rate</strong></p>
                                        <p class="small">on EUR/USD. <a class="open-html-popup hover-opac"
                                                data-mfp-src="#account-infos-popup">More info.</a></p>
                                    </td>


                                    <td>$1.80 / lot</td>




                                    <td>$1.50 / lot</td>





                                    <td>$0.42 / lot</td>

                                </tr>

                                <tr>
                                    <td>Cashback frequency</td>
                                    <td colspan="3">
                                        Daily
                                        updates
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <p>Spreads on EUR/USD</p>
                                        <p class="small"><a href="https://www.exness.com/forex/specifications#tab_mini"
                                                class="hover-opac" target="_blank">Full spread table</a></p>
                                    </td>


                                    <td>1.1 pip</td>

                                    <td>0.9 pip</td>


                                    <td>0.4 pip</td>

                                </tr>

                                <tr>
                                    <td>Commission</td>
                                    <td>No Commission</td>
                                    <td>No Commission</td>
                                    <td>$5 / lot</td>
                                </tr>

                                <tr class="tr-heading">
                                    <td colspan="4">
                                        <p><strong>Other instruments</strong></p>
                                        <p class="smaller">(click on the rate to see the exact instruments *)</p>
                                    </td>
                                </tr>




                                <tr>
                                    <td>Forex</td>


                                    <td>
                                        <a class="open-html-popup hover-opac" data-mfp-src="#account-infos-popup">
                                            $1.65 - $4.86
                                        </a>
                                    </td>


                                    <td>
                                        <a class="open-html-popup hover-opac" data-mfp-src="#account-infos-popup">
                                            $1.50 - $4.35
                                        </a>
                                    </td>


                                    <td>
                                        <a class="open-html-popup hover-opac" data-mfp-src="#account-infos-popup">
                                            $0.42
                                        </a>
                                    </td>


                                </tr>


                                <tr>
                                    <td>Metals</td>


                                    <td>
                                        <a class="open-html-popup hover-opac" data-mfp-src="#account-infos-popup">
                                            $7.90 - $9.90
                                        </a>
                                    </td>


                                    <td>
                                        <a class="open-html-popup hover-opac" data-mfp-src="#account-infos-popup">
                                            $7.45 - $8.25
                                        </a>
                                    </td>


                                    <td>
                                        <a class="open-html-popup hover-opac" data-mfp-src="#account-infos-popup">
                                            $0
                                        </a>
                                    </td>


                                </tr>
                                <tr>
                                    <td>CFD</td>


                                    <td>
                                        <a class="open-html-popup hover-opac" data-mfp-src="#account-infos-popup">
                                            $0 - $1.30
                                        </a>
                                    </td>


                                    <td>
                                        <a class="open-html-popup hover-opac" data-mfp-src="#account-infos-popup">
                                            $0 - $1
                                        </a>
                                    </td>


                                    <td>
                                        <a class="open-html-popup hover-opac" data-mfp-src="#account-infos-popup">
                                            $0
                                        </a>
                                    </td>


                                </tr>

                                <tr>
                                    <td>Crypto</td>


                                    <td>
                                        <a class="open-html-popup hover-opac" data-mfp-src="#account-infos-popup">
                                            $0 - $12.38
                                        </a>
                                    </td>


                                    <td>
                                        <a class="open-html-popup hover-opac" data-mfp-src="#account-infos-popup">
                                            $0 - $3.83
                                        </a>
                                    </td>


                                    <td>
                                        <a class="open-html-popup hover-opac" data-mfp-src="#account-infos-popup">
                                            $0
                                        </a>
                                    </td>


                                </tr>




                                <tr class="tr-heading">
                                    <td colspan="4"><strong>Other details</strong></td>
                                </tr>

                                <tr>
                                    <td>Trading platforms</td>
                                    <td>MT4, WebTrader, MT5</td>
                                    <td>MT4, WebTrader, MT5</td>
                                    <td>MT4, WebTrader, MT5</td>
                                </tr>

                                <tr>
                                    <td>Mobile platforms</td>
                                    <td>IOS,Android MT4</td>
                                    <td>IOS,Android MT4</td>
                                    <td>IOS,Android MT4</td>
                                </tr>

                                <tr>
                                    <td>Spread type</td>
                                    <td>Variable Spread</td>
                                    <td>Variable Spread</td>
                                    <td>Variable Spread</td>
                                </tr>

                                <tr>
                                    <td>Execution type</td>
                                    <td>MM</td>
                                    <td>MM</td>
                                    <td>ECN</td>
                                </tr>

                                <tr>
                                    <td>Order execution</td>
                                    <td>Market / Instant execution</td>
                                    <td>Market / Instant execution</td>
                                    <td>Market execution</td>
                                </tr>

                                <tr>
                                    <td>Minimum deposit</td>
                                    <td>$0</td>
                                    <td>$2,000</td>
                                    <td>$300</td>
                                </tr>

                                <tr>
                                    <td>Maximum leverage</td>
                                    <td>1:2,000</td>
                                    <td>1:2,000</td>
                                    <td>1:200</td>
                                </tr>

                                <tr>
                                    <td>Minimum trading size</td>
                                    <td>0.01</td>
                                    <td>0.1</td>
                                    <td>0.01</td>
                                </tr>

                                <tr>
                                    <td>Forex Pricing decimals</td>
                                    <td>5</td>
                                    <td>5</td>
                                    <td>5</td>
                                </tr>

                                <tr>
                                    <td>Swap or Rollover</td>
                                    <td><a href="https://www.exness.com/forex/specifications#tab_mini"
                                            class="hover-opac" taget="_blank">Click for info</a></td>
                                    <td><a href="https://www.exness.com/forex/specifications#tab_classic"
                                            class="hover-opac" taget="_blank">Click for info</a></td>
                                    <td><a href="https://www.exness.com/forex/specifications#tab_ecn" class="hover-opac"
                                            taget="_blank">Click for info</a></td>
                                </tr>

                                <tr>
                                    <td>Trading Platform's Timezone</td>
                                    <td>UTC +0</td>
                                    <td>UTC +0</td>
                                    <td>UTC +0</td>
                                </tr>

                                <tr>
                                    <td>Trailing stops</td>
                                    <td>
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    </td>
                                </tr>

                                <tr>
                                    <td>1-click trading</td>
                                    <td>
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    </td>
                                </tr>

                                <tr>
                                    <td>OCO Orders</td>
                                    <td>
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Trading API</td>
                                    <td>
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Non-expiring demo</td>
                                    <td>
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Observe DST Change</td>
                                    <td>
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Scalping allowed</td>
                                    <td>
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Hedging allowed</td>
                                    <td>
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Islamic accounts</td>
                                    <td>
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    </td>
                                </tr>

                                <tr>
                                    <td>MAMM/PAMM Platform option</td>
                                    <td>
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    </td>
                                </tr>

                                <tr>
                                    <td>MAMM/PAMM Leaderboard</td>
                                    <td>
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <script>
                            $('.open-html-popup').magnificPopup({
                                type: 'inline',
                                midClick: true, // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
                            });

                        </script>
                    </div>
                </div>
                <p class="trading-text">
                    Trading in large volumes? <a
                        href="http://support.fxcashbacks.com/knowledge-base/private-cashback-deal/"
                        class="hover-opac">Learn more about our private deals.</a>
                    <br>
                    <br>
                    * Spreads, commissions and cashback rates could be slightly different because of variable spreads at
                    certain account types.
                </p>
            </div>


            <div class="forex-notes">
                <h4><i class="fa fa-asterisk" aria-hidden="true"></i>Notes</h4>
                <div class="readMore" style="max-height: none;">
                    <p>Cashback will not be paid if your {{$broker->name}} account balance consists of only bonuses.
                    </p>
                    <p>users from Russia are not accepted to register for cashback</p>
                    <p>Chinese and Indonesian clients can register through this link, only:
                        {{$broker->web_link}} </p>
                    <p></p>
                </div>
            </div>


            <div id="new-accounts" class="forex_show_signup">

                <h4><i class="fa fa-share-square-o" aria-hidden="true"></i>{{$broker->name}} cashback for <em>new accounts</em>
                </h4>

                <div class="icon"><i class="fa fa-trash-o"></i></div>
                <div class="content">
                    <h5>1. Clear cookies</h5>
                    <p>
                        To correctly set up your new {{$broker->name}} account you must clear the cookies in your browser or open
                        an incognito mode window in your browser.
                    </p>
                    <p>
                        If you need help with clearing your cookies choose a guide for your browser.
                    </p>

                    <div class="browsers">
                        <a class="browser hover-opac"
                            href="http://support.fxcashbacks.com/knowledge-base/clear-cookies/#win-chrome"><i
                                class="fa fa-chrome margin-right-5"></i>Chrome</a>
                        <a class="browser hover-opac"
                            href="http://support.fxcashbacks.com/knowledge-base/clear-cookies/#win-firefox"><i
                                class="fa fa-firefox margin-right-5"></i>Firefox</a>
                        <a class="browser hover-opac"
                            href="http://support.fxcashbacks.com/knowledge-base/clear-cookies/#mac-safari"><i
                                class="fa fa-safari margin-right-5"></i>Safari</a>
                    </div>

                    <p id="underBrowser">This process will NOT affect any of the cashback deals you have at any other
                        brokers.</p>
                </div>

                <div class="icon"><i class="fa fa-link" aria-hidden="true"></i></div>
                <div class="content">
                    <h5>2. Sign up at {{$broker->name}} via our referral link</h5>
                    <p>Click the button below to start.</p>
                    <p>You'll be taken to the {{$broker->name}} website to download the software and create a new {{$broker->name}} account.
                    </p>
                    <div class="text-align-center">
                        <a class="cta add-account-popup" target="_blank"
                            href="{{$broker->web_link}}">
                            <i class="fa fa-external-link margin-right-5"></i>
                            Open a live {{$broker->name}} account with cashback
                        </a>
                        <div id="private_window" style="display: none;">Open in a private window!</div>

                    </div>
                </div>

                <div class="icon"><i class="fa fa-plus-square-o" aria-hidden="true"></i></div>
                <div class="content">
                    <h5>3. After opening an account, add it on FxCashback too</h5>
                    <p>To be able to track your account we need to know your unique ID at {{$broker->name}}. After you opened your
                        account successfully through our referral link above you need to give us your {{$broker->name}} details.
                    </p>
                    <div class="text-align-center">
                        <input type="hidden" id="brokerId" value="{{$broker->id}}">
                        @if (Auth::check())
                            <button data-toggle="modal" data-target="#brokerexampleModal" id="forex" class="addAccountBrokerPage">Add a(n) {{$broker->name}} cashback account</button>
                            
                        @else
                            <a href="{{route('login')}}">
                                <button id="forex" class="addAccountBrokerPage">Add a(n) {{$broker->name}} cashback account</button>
                            </a>
                        @endif
                    </div>
                </div>

                <div class="icon"><i class="fa fa-usd" aria-hidden="true"></i></div>
                <div class="content">
                    <h5>4. Trade on your account and receive cashback</h5>
                    <p>After we verify your account you'll be eligible to receive cashback on your account.Verifying
                        could take 3-5 days depending on your broker. <b>Of course, your other bonuses and deals remain
                            the same.</b></p>
                </div>

            </div>

            <div id="existing-accounts" class="content_row existingAccountPossible">

                <h4><i class="fa fa-anchor" aria-hidden="true"></i>Cashback for {{$broker->name}} <em>existing accounts</em></h4>


                <div class="label-true">
                    <i class="fa fa-check" aria-hidden="true"></i>
                    <strong>You CAN link existing accounts to this broker</strong>
                </div>

                <p id="existing">
                    If you already have an account at {{$broker->name}} it <strong>may be possible to put your account under
                        FxCashback</strong>. If you would like to do so, please <a
                        href="mailto:support@fxcashbacks.com" class="hover-opac">contact us</a> and we'll tell you the
                    rest. Just click on the button below and write an email about it and we'll see what we can do. <a
                        href="http://support.fxcashbacks.com/knowledge-base/existing-account-cashback/" target="_blank"
                        class="color-blue underlined hover-opac">Learn more about linking existing accounts.</a>
                </p>

                <div class="margin-bottom-80">
                    <a href="https://www.exness.com/about_us/contacts" class="btn-contact hover-opac"><strong>Contact
                            your broker</strong></a>
                    <a href="/email-composer-for-existing-accounts" class="btn-email hover-opac"><strong>Use our email
                            sender</strong></a>
                </div>


            </div>


            {{-- <div class="forex_show_more content_row">
                <h4><i class="fa fa-file-text-o" aria-hidden="true"></i> More about this broker</h4>

                <div class="moreAboutThisBroker">
                    <div class="readMore" style="max-height: none;">
                        <p>{{$broker->description}}</p>
                    </div>
                </div>

                <p class="ratings">Ratings</p>
                <ul class="ratings_content">
                    <li>
                        Competitive Costs
                        <span class="stars">
                            <i class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                class="fa fa-fw fa-star-o font-size-16 color-star"></i>
                        </span>
                    </li>
                    <li>
                        Popularity
                        <span class="stars">
                            <i class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                class="fa fa-fw fa-star-half-o font-size-16 color-star"></i>
                        </span>
                    </li>
                    <li>
                        Regulator ratings
                        <span class="stars">
                            <i class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                class="fa fa-fw fa-star-half-o font-size-16 color-star"></i><i
                                class="fa fa-fw fa-star-o font-size-16 color-star"></i>
                        </span>
                    </li>
                    <li>
                        User reviews
                        <span class="stars">
                            <i class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                class="fa fa-fw fa-star-o font-size-16 color-star"></i><i
                                class="fa fa-fw fa-star-o font-size-16 color-star"></i>
                        </span>
                        <a href="http://www.myfxbook.com/reviews/brokers/exness/8933,1" class="hover-opac">(237)</a>
                    </li>
                </ul>
            </div> --}}

            {{-- <div class="similar_cashback_deals clearfix">
                <h4><i class="fa fa-arrow-right" aria-hidden="true"></i> Similar cashback deals</h4>
                <a href="/brokers/spectre-ai">
                    <div class="deal-box">
                        <img class="broker-logo lazy loaded"
                            data-src="{{url('')}}/images/brokers/forex-small/spectre-ai.png" alt="Spectre.ai"
                            src="{{url('')}}/images/brokers/forex-small/spectre-ai.png" data-was-processed="true"><br>
                        <h5>Spectre.ai</h5>
                        <div class="stars"> <i class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                class="fa fa-fw fa-star font-size-16 color-star"></i> </div>
                        <div class="small">
                            cashback up to
                            <div class="rebate_info">$0.00 / lot</div>
                        </div>
                    </div>
                </a>
                <a href="/brokers/fortfs">
                    <div class="deal-box">
                        <img class="broker-logo lazy loaded" data-src="{{url('')}}/images/brokers/forex-small/fortfs.png"
                            alt="Fort Financial Services" src="{{url('')}}/images/brokers/forex-small/fortfs.png"
                            data-was-processed="true"><br>
                        <h5>Fort Financial Services</h5>
                        <div class="stars"> <i class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                class="fa fa-fw fa-star font-size-16 color-star"></i> </div>
                        <div class="small">
                            cashback up to
                            <div class="rebate_info">$9.35 / lot</div>
                        </div>
                    </div>
                </a>
                <a href="/brokers/fp-markets">
                    <div class="deal-box">
                        <img class="broker-logo lazy loaded"
                            data-src="{{url('')}}/images/brokers/forex-small/fp-markets.png" alt="FP Markets"
                            src="{{url('')}}/images/brokers/forex-small/fp-markets.png" data-was-processed="true"><br>
                        <h5>FP Markets</h5>
                        <div class="stars"> <i class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                class="fa fa-fw fa-star font-size-16 color-star"></i> </div>
                        <div class="small">
                            cashback up to
                            <div class="rebate_info">$4.25 / lot</div>
                        </div>
                    </div>
                </a>
                <a href="/brokers/swiss-markets">
                    <div class="deal-box">
                        <img class="broker-logo lazy loaded"
                            data-src="{{url('')}}/images/brokers/forex-small/swiss-markets.png" alt="Swiss Markets"
                            src="{{url('')}}/images/brokers/forex-small/swiss-markets.png" data-was-processed="true"><br>
                        <h5>Swiss Markets</h5>
                        <div class="stars"> <i class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                class="fa fa-fw fa-star-o font-size-16 color-star"></i> </div>
                        <div class="small">
                            cashback up to
                            <div class="rebate_info">$5.95 / lot</div>
                        </div>
                    </div>
                </a>
                <a href="/brokers/fxpro-cysec">
                    <div class="deal-box">
                        <img class="broker-logo lazy loaded"
                            data-src="{{url('')}}/images/brokers/forex-small/fxpro-cysec.png" alt="FxPro"
                            src="{{url('')}}/images/brokers/forex-small/fxpro-cysec.png" data-was-processed="true"><br>
                        <h5>FxPro</h5>
                        <div class="stars"> <i class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                class="fa fa-fw fa-star font-size-16 color-star"></i><i
                                class="fa fa-fw fa-star font-size-16 color-star"></i> </div>
                        <div class="small">
                            cashback up to
                            <div class="rebate_info">$4.25 / lot</div>
                        </div>
                    </div>
                </a>
            </div> --}}
        </section>
    </section>

    <section id="broker_info_right">
        <div class="margin-top-70">
            <section class="deal_type_box">
                <h2><img src="/images/Assetsicon_cpa_black.svg" alt="icon-cpa" width="21" height="17">Trading volume
                    deal</h2>
                <p>
                    This means that you get cashback after <strong>every</strong> commission or fee you generate at your
                    broker. You get cashback when you trade.
                </p>
                <a href="http://support.fxcashbacks.com/knowledge-base/deal-types/" class="btn btn-m hover-opac">Learn
                    more about deal types</a>
            </section>
        </div>

        <section class="crypto_cashback_info">
            <h1>Crypto Cashback available</h2>
            <p>It’s possible to receive cashback on crypto currency trades such as Bitcoin or Ethereum.</p>
            <img data-src="/images/Assets/Group 540.png" width="54" height="54" alt="btc-logo" class="lazy loaded"
                src="/images/Assets/Group 540.png" data-was-processed="true">
        </section>


        <div class="margin-top-20">
            <section class="cbc_rating_box">

                <h4>FxCashback Rating</h4>

                <p><i class="fa fa-fw fa-star font-size-16 color-star"></i><i
                        class="fa fa-fw fa-star font-size-16 color-star"></i><i
                        class="fa fa-fw fa-star font-size-16 color-star"></i><i
                        class="fa fa-fw fa-star font-size-16 color-star"></i><i
                        class="fa fa-fw fa-star-o font-size-16 color-star"></i></p>

                <p class="margin-top-10">
                    This service is one of the most trustworthy on the market with great deals and good infrastructure.
                    Nearly everything is perfect, but still misses something to be one of the best.
                </p>

                <p class="margin-top-15"><a href="http://support.fxcashbacks.com/knowledge-base/forex-broker-rating/"
                        class="hover-opac">Learn more about CBC ratings</a></p>
            </section>
        </div>

        <section id="bi_info" class="oneinfo">

            <h4><i class="fa fa-university" aria-hidden="true"></i>Broker information</h4>

            <label>
                <input type="text" class="tableSearcher" placeholder="Type something here to search in info">
            </label>
            <table class="searchableTable">

                <tbody>
                    <tr class="head">
                        <td colspan="2">Broker on FxCashback</td>
                    </tr>

                    <tr>
                        <td>{{$broker->name}} accounts on FxCashback</td>
                        <td>541</td>
                    </tr>

                    <tr>
                        <td>FxCashback Point / $1 commission made</td>
                        <td>50</td>
                    </tr>


                    <tr>
                        <td>Cashback deal type</td>
                        <td>Trading Volume <i class="fa fa-info-circle margin-left-5 color-dgray margin-right-0"
                                data-qtip="Basically in this type, you’ll receive cashback based on your trading volume. Based on this, you’ll generate commission to your provider (broker, poker room, etc.). Because you’re under the FxCashback referral network, we’ll receive a portion of this commission and share the majority of it with you. That’s how you earn money consistently, when you generate commission to your provider."
                                data-hasqtip="15296"></i></td>
                    </tr>

                    <tr>
                        <td colspan="2" class="devider"></td>
                    </tr>


                    <tr class="head">
                        <td colspan="2">Broker information</td>
                    </tr>

                    <tr>
                        <td>Name of broker</td>
                        <td>{{$broker->name}}</td>
                    </tr>

                    <tr>
                        <td>Banned countries</td>
                        <td>Residents and citizens: US, VC, VA, IL, MY ; Only res.: NZ, CA, AU, UK, AD, AT, BA, BG, HR,
                            CY, CZ, DK, EE, FI, FR, GR, HU, IS, IT, IR, LV, LI, LT, LU, MT, MC, NO, NL, PL, PT, RO, SM,
                            SI, SK, ES, SE, CH, GI, IR, IQ, SY, YE, SO, PS, ET, SS, JP, KP, SG</td>
                    </tr>


                    <tr>
                        <td>Accepts US clients</td>
                        <td>
                            <i class="fa fa-times" aria-hidden="true"></i>No
                        </td>
                    </tr>

                    <tr>
                        <td>Accepts Japanese clients</td>
                        <td>
                            <i class="fa fa-times" aria-hidden="true"></i>No
                        </td>
                    </tr>

                    <tr>
                        <td>Broker's company name</td>
                        <td>{{$broker->name}} LTD</td>
                    </tr>

                    <tr>
                        <td>Online establishment</td>
                        <td>2008</td>
                    </tr>

                    <tr>
                        <td>Broker's company address</td>
                        <td>F20, 1st floor, Eden Plaza, Eden Island, Seychelles</td>
                    </tr>


                    <tr>
                        <td>Employees</td>
                        <td>538</td>
                    </tr>

                    <tr>
                        <td>Broker's website</td>
                        <td><a target="_blank" href="/redirect-to-url/https://www.exness.com/a/rcg673kp">Website</a>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2" class="devider"></td>
                    </tr>


                    <tr class="head">
                        <td colspan="2">Trading options</td>
                    </tr>


                    <tr>
                        <td>Execution Options</td>
                        <td>MM, ECN</td>
                    </tr>

                    <tr>
                        <td>Instruments</td>
                        <td>Forex, Futures, Crypto</td>
                    </tr>

                    <tr>
                        <td colspan="2" class="devider"></td>
                    </tr>


                    <tr class="head">
                        <td colspan="2">Funding information</td>
                    </tr>

                    <tr>
                        <td>Available account currencies</td>
                        <td>
                            USD,
                            EUR,
                            GBP,
                            CHF,
                            AUD,
                            Gold,
                            Silver,
                            Platinum,
                            Palladium,
                            Combined metals,
                            Metal-Currency
                        </td>
                    </tr>

                    <tr>
                        <td>Funding methods</td>
                        <td>
                            Skrill,
                            Neteller,
                            Wire Transfer,
                            Credit/Debit Card,
                            WebMoney,
                            CashU,
                            ukash,
                            Perfect Money
                        </td>
                    </tr>


                    <tr>
                        <td>Interest bearing account</td>
                        <td>
                            <i class="fa fa-times" aria-hidden="true"></i>No
                        </td>
                    </tr>

                    <tr>
                        <td>Government enforced segregated accounts</td>
                        <td>
                            <i class="fa fa-check" aria-hidden="true"></i>Yes
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2" class="devider"></td>
                    </tr>

                    <tr class="head">
                        <td colspan="2">Customer support</td>
                    </tr>

                    <tr>
                        <td>Contact</td>
                        <td><a href="https://www.exness.com/about_us/contacts">Contact</a></td>
                    </tr>

                    <tr>
                        <td>24 hours support</td>
                        <td>
                            <i class="fa fa-check" aria-hidden="true"></i>Yes
                        </td>
                    </tr>

                    <tr>
                        <td>Support languages</td>
                        <td>
                            English,
                            Russian,
                            Spanish,
                            Arabic,
                            Chinese,
                            Hindi,
                            Indonesian,
                            Korean,
                            Malay,
                            Portuguese,
                            Thai,
                            Vietnamese,
                            Bengali,
                            Urdu,
                            Farsi,
                            Tamil
                        </td>
                    </tr>

                    <tr>
                        <td>Support Options</td>
                        <td>
                            Email,
                            Phone,
                            Live Chat,
                            Whatsapp,
                            Facebook,
                            Viber
                        </td>
                    </tr>

                </tbody>
            </table>
        </section>

        <section id="bi_payment" class="oneinfo">
            <h4><i class="fa fa-credit-card" aria-hidden="true"></i>Payment methods</h4>
            @foreach ($payment_methods as $payment_method)
                <img data-toggle="modal" data-target="#paymentexampleModal" data-src="{{url('')}}/uploads/{{$payment_method->image}}" alt="{{$payment_method->name}}" width="50" height="30" data-id="{{$payment_method->id}}" data-qtip="Skrill" class="payment_method_area lazy loaded" src="{{url('')}}/uploads/{{$payment_method->image}}" data-was-processed="true" data-hasqtip="15380">                
            @endforeach

            {{-- <img data-src="/images/Assets/Group 539.png" alt="Neteller" width="50" height="30"
                data-qtip="Neteller" class="lazy loaded" src="/images/Assets/Group 539.png"
                data-was-processed="true" data-hasqtip="15381">
            <img data-src="/images/Assets/Group 540.png" alt="Wire Transfer" width="50" height="30"
                data-qtip="Wire Transfer" class="lazy loaded" src="/images/Assets/Group 540.png"
                data-was-processed="true" data-hasqtip="15382">
            <img data-src="/images/Assets/Group 538.png" alt="Credit/Debit Card" width="50" height="30" --}}
                {{-- data-qtip="Credit/Debit Card" class="lazy loaded" src="/images/Assets/Group 538.png" --}}
                {{-- data-was-processed="true" data-hasqtip="15383"> --}}
            {{-- <img data-src="/images/Assetspayments/webmoney.png" alt="WebMoney" width="50" height="30"
                data-qtip="WebMoney" class="lazy loaded" src="/images/Assetspayments/webmoney.png"
                data-was-processed="true" data-hasqtip="15384">
            <img data-src="/images/Assetspayments/cashu.png" alt="CashU" width="50" height="30" data-qtip="CashU"
                class="lazy loaded" src="/images/Assetspayments/cashu.png" data-was-processed="true"
                data-hasqtip="15385">
            <img data-src="/images/Assetspayments/ukash.png" alt="ukash" width="50" height="30" data-qtip="ukash"
                class="lazy loaded" src="/images/Assetspayments/ukash.png" data-was-processed="true"
                data-hasqtip="15386">
            <img data-src="/images/Assetspayments/perfectmoney.png" alt="Perfect Money" width="50" height="30"
                data-qtip="Perfect Money" class="lazy loaded" src="/images/Assetspayments/perfectmoney.png"
                data-was-processed="true" data-hasqtip="15387"> --}}
        </section>


    </section>
</div>
<!-- Bootstrap core JavaScript -->
<script src="{{url('')}}/vendor/jquery/jquery.min.js"></script>
<script src="{{url('')}}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<script>
    $(document).ready(function(){
        $('#type_amount').change(function(){
            var type_amount = $(this).val();
            console.log(type_amount);
        });
        $('.payment_method_area').click(function(){
            var id = $(this).data('id');
            $('.payment_method_id').val(id);
            console.log(id);            
        });
    });
</script>
