<link href="{{ url('')}}/assets/img/favicon.png" rel="icon">
<link href="{{ url('')}}/assets/img/apple-touch-icon.png" rel="apple-touch-icon">

@extends('layouts.common')

@section('content')
        <style>
      /* ========== Who We Are ========== */

      .info-section {
            float: left;
            width: 100%;
            padding: 5rem 0;
            position: relative;
            z-index: 1;
            background: white;
            padding-top: 120px;
  padding-bottom: 120px;


                }

        .info-section h2 {
            font-weight: 700;
            font-size: 2.5rem;
                }

        .info-section .head-sec {
            float: left;
                    width: 100%;
        }

        .info-section h3 {
            font-size: 1.4rem;
                }

        .info-section p {
            font-size: 1rem;
            line-height: 1.3rem;
        }

        .info-section .box h2 {
            font-size: 24px;
            margin-bottom: 20px;
            margin-top: 0;
        }

        .info-section .box i {
            font-size: 20px;
        }

        .info-section .box {
            display: flex;
        }

        .info-section .text-box {
                    flex: 1 1 0;
            text-align: left;
        }

        .info-section .icon-box {
                    line-height: 1.2;
            width: 70px;
        }

        .info-section .service-block-overlay {
            transition: .5s;
            -webkit-transition: .5s;
            -moz-transition: .5s;
                }

        .info-section .service-block-overlay:hover {
            background: #fff none repeat scroll 0 0;
            border-radius: 5px;
            box-shadow: 0 0 90px rgba(0, 0, 0, 0.1);
            float: left;
            margin-top: -10px;
            position: relative;
            width: 100%;
            transition: .5s;
                    -webkit-transition: .5s;
            -moz-transition: .5s;
        }

        .info-section .content-half {
            color: #fff;
        }

        .info-section .content-half ul {
            padding: 0;
            list-style: none;
        }

        .info-section .content-half ul li {
            margin: 15px 0;
            float: left;
                    width: 100%;
        }

        .info-section .content-half ul li i {
                    float: left;
            font-size: 30px;
            padding-top: 10px;
            padding-bottom: 10px;
        }

        .info-section .content-half ul li .list-content {
                    float: left;
            margin-left: 20px;
        }

                .info-section .content-half ul li strong {
            font-size: 19px;
            font-weight: 700;
        }

                .info-section .content-half .btn {
            margin-top: 20px;
        }
        /*--- three Block Panel ---*/

        .info-section .three-panel-block {
            float: left;
            width: 100%;
                }

        .info-section .three-panel-block i {
            font-size: 1.5rem;
            margin-bottom: 15px;
        }

                .info-section .three-panel-block i.box-round {
            border-color: inherit;
            border-width: 1px;
            border-style: solid;
            padding: 16px;
            border-radius: 50%;
                }
        /*--- Two Block Panel ---*/

        .info-section .two-panel-block {
            float: left;
            width: 100%;
                }

        .info-section .two-panel-block p {
            font-size: 16px;
        }
        /*--- Two Block Panel ---*/

        .info-section .four-panel-block {
            float: left;
            width: 100%;
        }
        #support_Section{
            padding-top: 120px;
            padding-bottom: 50px;}


        /* [data-toggle="collapse"]:after {
display: inline-block;
    display: inline-block;
    font: normal normal normal 14px/1 FontAwesome;
    font-size: inherit;
    text-rendering: auto;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  content: "\f054";
  transform: rotate(90deg) ;
  transition: all linear 0.25s;
  float: right;
  }
[data-toggle="collapse"].collapsed:after {
  transform: rotate(0deg) ;
} */
#support_Section .card-header {
    text-align: center;
    font-size: 25px;
    color: #002e3e;
    background:transparent;
    border-left: 8px solid #012632;



}
#support_Section a {
    color: #00516b;
    font-size: 18px;
    text-align: center;
}
#support_Section .card-body{
    font-size: 13px;
    line-height: 21px;
}
        </style>
                 <!-- Page Content-->
<!-- Info block 1 -->



<section id="support_Section">
    <div class="container">
    <div class="row">
    <div class="col-md-8 offset-md-2">



    <div id="accordion" role="tablist">
        <div class="card">
          <div class="card-header" role="tab" id="headingOne">
            <h5 class="mb-0">
              <a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                1. How Forex Cashback works?
              </a>
            </h5>
      </div>
      <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
            <div class="card-body">
                It is all quite very simple. You open an account through us with your selected broker, apply it in FXcashbacksFX and every trade you make will earn you a rebate, whether it makes profit or not.
            </div>
      </div>
      </div>
      {{-- //cardend --}}
      <div class="card">
          <div class="card-header" role="tab" id="headingTwo">
            <h5 class="mb-0">
              <a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                2. Why should I use FXcashbacksFX?
              </a>
            </h5>
      </div>
      <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
            <div class="card-body">
                You can save hundreds of dollars per month with no extra effort.
            </div>
      </div>
      </div>
      {{-- //cardend --}}
      <div class="card">
          <div class="card-header" role="tab" id="headingThree">
            <h5 class="mb-0">
              <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                3. How much profit can I make?
              </a>
            </h5>
      </div>
      <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
            <div class="card-body">
                Profits are unlimited and depend on your trading volume and the broker's rebate rate. The more you trade, the more you earn.
            </div>
      </div>
      </div>
{{-- //cardend --}}

<div class="card">
    <div class="card-header" role="tab" id="headingfour">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
            4. If my trade loses, do I still get a profit?
        </a>
      </h5>
</div>
<div id="collapsefour" class="collapse" role="tabpanel" aria-labelledby="headingfour" data-parent="#accordion">
      <div class="card-body">
        Yes. You earn a profit for each trade whether it is profitable or not.
      </div>
</div>
</div>
{{-- //cardend --}}


<div class="card">
    <div class="card-header" role="tab" id="headingfive">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#collapsefive" aria-expanded="false" aria-controls="collapsefive">
            5. Why can't I open an account on my own to earn rebates?
        </a>
      </h5>
</div>
<div id="collapsefive" class="collapse" role="tabpanel" aria-labelledby="headingfive" data-parent="#accordion">
      <div class="card-body">
        When you sign up through us, the broker pays a rebate most of which transferred to you. When you open an account on your own, the broker will not pay any rebates.
      </div>
</div>
</div>
{{-- //cardend --}}


<div class="card">
    <div class="card-header" role="tab" id="headingsix">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#collapsesix" aria-expanded="false" aria-controls="collapsesix">
            6. Will my trading conditions change? (Spreads, commissions, swap, etc)
        </a>
      </h5>
</div>
<div id="collapsesix" class="collapse" role="tabpanel" aria-labelledby="headingsix" data-parent="#accordion">
      <div class="card-body">
        No. Your conditions will remain exactly the same.
      </div>
</div>
</div>
{{-- //cardend --}}


<div class="card">
    <div class="card-header" role="tab" id="headingsix">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#collapse7" aria-expanded="false" aria-controls="collapse7">
            7. Can you access my account?
        </a>
      </h5>
</div>
<div id="collapse7" class="collapse" role="tabpanel" aria-labelledby="heading7" data-parent="#accordion">
      <div class="card-body">
        No, we cannot.
      </div>
</div>
</div>
{{-- //cardend --}}




<div class="card">
    <div class="card-header" role="tab" id="headingeight">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#collapse8" aria-expanded="false" aria-controls="collapse8">
            8. Can I use my existing trading account?
        </a>
      </h5>
</div>
<div id="collapse8" class="collapse" role="tabpanel" aria-labelledby="heading8" data-parent="#accordion">
      <div class="card-body">
        This depends on your broker. Some brokers allow to change IB's (Introducing Broker) for existing accounts and some don't (in which can you'll have to simply open a new account).
      </div>
</div>
</div>
{{-- //cardend --}}



<div class="card">
    <div class="card-header" role="tab" id="heading9">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#collapse9" aria-expanded="false" aria-controls="collapse9">
            9. My broker is not on the list of your supported brokers?
        </a>
      </h5>
</div>
<div id="collapse9" class="collapse" role="tabpanel" aria-labelledby="heading9" data-parent="#accordion">
      <div class="card-body">
        We are happy to add new brokers to the list. Simply contact us with more information.
      </div>
</div>
</div>
{{-- //cardend --}}


<div class="card">
    <div class="card-header" role="tab" id="heading10">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#collapse10" aria-expanded="false" aria-controls="collapse10">
            10. Can I earn with a demo account?
        </a>
      </h5>
</div>
<div id="collapse10" class="collapse" role="tabpanel" aria-labelledby="heading10" data-parent="#accordion">
      <div class="card-body">
        No, since your broker will not pay us any rebates for a demo account.
      </div>
</div>
</div>
{{-- //cardend --}}

<div class="card">
    <div class="card-header" role="tab" id="heading11">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#collapse11" aria-expanded="false" aria-controls="collapse11">
            11. Can I have multiple accounts with the same broker?
        </a>
      </h5>
</div>
<div id="collapse11" class="collapse" role="tabpanel" aria-labelledby="heading11" data-parent="#accordion">
      <div class="card-body">
        Yes, you can, however please note all accounts must be opened through us as the IB so we can pay you rebates.
      </div>
</div>
</div>
{{-- //cardend --}}


<div class="card">
    <div class="card-header" role="tab" id="heading12">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#collapse12" aria-expanded="false" aria-controls="collapse12">
            12. Can I have multiple profiles with you?
        </a>
      </h5>
</div>
<div id="collapse12" class="collapse" role="tabpanel" aria-labelledby="heading12" data-parent="#accordion">
      <div class="card-body">
        Yes, but there is no reason to. We strongly suggest to allocate all of your accounts under one profile for easier tracking.
      </div>
</div>
</div>
{{-- //cardend --}}



      </div>
    </div>
    </div>
    </div>
</section>
{{-- <section class="info-section">
	<div class="container">
		<div class="head-box text-center mb-5">
			<h2>Help Topics</h2>
		</div>
		<div class="three-panel-block mt-5">
			<div class="row">
                				<div class="col-lg-4 col-md-6 col-sm-6">
					<div class="service-block-overlay text-center mb-5 p-lg-3">
        						<i class="fa fa-laptop box-circle-solid mt-3 mb-3" aria-hidden="true"></i>
						<h3>
                        	Account details                        </h3>
                						<p class="px-4">
                                            Information regarding VIP levels, CBC points, cashback rate and how to increase it even up to 100%.                            </p>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-6">
        					<div class="service-block-overlay text-center mb-5 p-lg-3">
                						<i class="fa fa-calendar box-circle-solid mt-3 mb-3" aria-hidden="true"></i>
						<h3>
                        	Getting Started                        </h3>
						<p class="px-4">
                            Everything starting from registering to our site, ending in connecting an account to receive cashback.                            </p>
					</div>
                        				</div>
				<div class="col-lg-4 col-md-6 col-sm-6">
					<div class="service-block-overlay text-center mb-5 p-lg-3">
						<i class="fa fa-bug box-circle-solid mt-3 mb-3" aria-hidden="true"></i>
						<h3>
                        	Miscellaneous                        </h3>
                						<p class="px-4">
                                            Answers to the common user questions , plus some random information that some might find useful                            </p>
        					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-6">
					<div class="service-block-overlay text-center p-lg-3">
						<i class="fa fa-cloud-upload box-circle-solid mt-3 mb-3" aria-hidden="true"></i>
                						<h3>
                                            Poker                        </h3>
						<p class="px-4">
                            Every related information to know about our newest service, the poker cashback.                            </p>
        					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-6">
					<div class="service-block-overlay text-center p-lg-3">
                						<i class="fa fa-diamond box-circle-solid mt-3 mb-3" aria-hidden="true"></i>
						<h3>
                        	Referral Program                        </h3>
						<p class="px-4">
                            All information needed to start building your own referral system and earn money with ease after your referrals                            </p>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-6">
					<div class="service-block-overlay text-center p-lg-3">
        						<i class="fa fa-comments box-circle-solid mt-3 mb-3" aria-hidden="true"></i>
						<h3>
                        	The Basics                        </h3>
						<p class="px-4">
                            Who are we? What we do? What we offer? Why do we have the best offer on the market?  You know, the basics.                            </p>
                					</div>
        				</div>
			</div>
		</div>
	</div>
                </section> --}}



