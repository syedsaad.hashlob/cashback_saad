@extends('admin.layouts.app')

@section('content')
<main id="main" class="main">

    <div class="pagetitle">
        <h1>View Summary</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
                <li class="breadcrumb-item">Summary</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body">'
                        <h5 class="card-title">View Summary Report</h5>
                        <!-- Table with stripped rows -->
                        <table class="table datatable">
                            <thead>
                                <tr>
                                    <th scope="col">S.No</th>
                                    <th scope="col">User Account ID</th>
                                    <th scope="col">Broker</th>
                                    <th scope="col">Current Cashback Rate</th>
                                    <th scope="col">Amount</th>
                                    <th scope="col">Earn Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($summaries as $key => $summary)
                                <tr>
                                    <td>{{++$key}}</td>
                                    <td>{{$summary->MT4_MT5_ID}}</td>
                                    <td>{{$summary->userBrokerAccount->broker->name}}</td>
                                    <td>{{$summary->user->cashback_rate}}</td>
                                    <td>{{$summary->Actual_Comm}}</td>
                                    @php
                                        $user_total_comm  =   ($summary->Actual_Comm * $summary->user->cashback_rate) / 100;
                                    @endphp
                                    <td>{{$user_total_comm}}</td>
                                    {{-- <td>{{$summary->Actual_Comm - $user_total_comm}}</td> --}}
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <!-- End Table with stripped rows -->

                    </div>
                </div>

            </div>
        </div>

    </section>

</main><!-- End #main -->

@endsection
