@extends('admin.layouts.app')

@section('content')
<main id="main" class="main">

    <div class="pagetitle">
        <h1>View User Summary</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
                <li class="breadcrumb-item">User Summary</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section">
        <div class="row">
            <div class="col-lg-12">
                <div class="col-md-4">
                    <h5 class="card-title">Export Transaction</h5>
                    <a href="{{route('user-site-summary-report')}}"><h5 class="card-title"><i style="font-size: 40px" class="bi bi-file-earmark-excel-fill"></i></h5></a>
                </div>
                <div class="card">
                    <div class="card-body">'
                        <h5 class="card-title">View User Summary Report</h5>
                        <!-- Table with stripped rows -->
                        <table class="table datatable">
                            <thead>
                                <tr>
                                    <th scope="col">S.No</th>
                                    <th scope="col">Current Points</th>
                                    <th scope="col">Current Cashback Rate</th>
                                    <th scope="col">Amount</th>
                                    <th scope="col">Earn Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $key => $user)
                                <tr>
                                    <td>{{++$key}}</td>
                                    <td>{{$user->current_points}}</td>
                                    <td>{{$user->cashback_rate}}</td>
                                    @php
                                        $transaction_trading_amount = \App\Trader_trades::where([['user_id',$user->id]])->pluck('Actual_Comm')->toArray();   
                                        $transaction_trading_amount = array_sum($transaction_trading_amount);
                                    @endphp
                                    <td>{{$transaction_trading_amount}}</td>
                                    @php
                                        $earning_trading_amount = \App\Trader_trades::where([['user_id',$user->id]])->pluck('Total_Comm')->toArray();   
                                        $earning_trading_amount = array_sum($earning_trading_amount);
                                    @endphp
                                    <td>{{$earning_trading_amount}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <!-- End Table with stripped rows -->

                    </div>
                </div>

            </div>
        </div>

    </section>

</main><!-- End #main -->

@endsection
