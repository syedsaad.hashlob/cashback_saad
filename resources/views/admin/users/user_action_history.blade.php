@extends('admin.layouts.app')

@section('content')
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.4/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css">

<main id="main" class="main">

    <div class="pagetitle">
      <h1>View Action History</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{url('admin.home')}}">Home</a></li>
          <li class="breadcrumb-item">Tables</li>
          <li class="breadcrumb-item active">Data</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

    <section class="section">
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <div class="card-body">
              <div class="row">
                <div class="col-md-4">
                    <h5 class="card-title">Export Transaction</h5>
                    <a href="{{route('user_action.history_export',$action_historys->id)}}"><h5 class="card-title"><i style="font-size: 40px" class="bi bi-file-earmark-excel-fill"></i></h5></a>
                </div>
                <div class="col-md-4">
                  <h5 class="card-title">Export Withdraw Requests</h5>
                  <a href="{{route('user_action.withdraw_export',$action_historys->id)}}">
                    <h5 class="card-title"><i style="font-size: 40px" class="bi bi-file-earmark-excel-fill"></i></h5>
                  </a>
                </div>
                <br><br>
                <div class="col-md-5">
                  <select class="form-select" required name="broker_id" aria-label="Default select example">
                    <option selected disabled>Select File Type</option>
                    @foreach ($brokers as $broker)
                      <option value="{{$broker->id}}">{{$broker->name}}</option>
                    @endforeach
                </select>
                </div>
              </div>
              @php
                  $tr=0;
                  $wd=0;
              @endphp
              <!-- Table with stripped rows -->
              <table class="table datatable">
                <thead>
                  <tr>
                    <th scope="col">S.No</th>
                    <th scope="col">User Account ID / Payment Method</th>
                    <th scope="col">Date</th>
                    <th scope="col">Total Commission / Transaction Amount</th>
                    <th scope="col">Type</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($action_historys->tradings as $action_history)
                    <tr>
                      <td>{{$action_history->id}}</td>
                      <td>{{$action_history->MT4_MT5_ID}}</td>
                      <td>{{$action_history->date}}</td>
                      <td>${{$action_history->Total_Comm ?? 0}}</td>
                      <td>Trading</td>
                    </tr>
                    @php
                        $tr+=$action_history->Total_Comm ?? 0;
                    @endphp
                  @endforeach
                  @foreach($action_historys->withdraw as $withdraw_history)
                  <tr>
                    <td>{{$withdraw_history->id}}</td>
                    <td>{{$withdraw_history->paymentMethod->name}}</td>
                    <td>{{$withdraw_history->created_at}}</td>
                    <td>${{$withdraw_history->total_amount ?? 0}}</td>
                    <td>Withdraw</td>
                  </tr>
                  @php
                      $wd+=$withdraw_history->total_amount ?? 0;
                  @endphp
                @endforeach
                <tr>
                    <td colspan="3" align="right">Total</td>
                    <td align="left">Trading: {{$tr}}</td>
                    <td align="left">Withdraw: {{$wd}}</td>
                </tr>
                </tbody>
              </table>
              <!-- End Table with stripped rows -->

            </div>
          </div>

        </div>
      </div>
    </section>

  </main><!-- End #main -->

@section('page-script')

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
<script>
  $(document).ready(function(){
    $('#example').DataTable();
  });

@endsection
</script>
@endsection
