@extends('admin.layouts.app')

@section('content')
<link href="{{ url('')}}/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<main id="main" class="main">

    <div class="pagetitle">
      <h1>View Activity</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{url('admin.home')}}">Home</a></li>
          <li class="breadcrumb-item">Tables</li>
          <li class="breadcrumb-item active">Data</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

    <section class="section">
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <div class="card-body">'
              <h5 class="card-title">View Activity</h5>
              <!-- Table with stripped rows -->
              <table class="table datatable">
                <thead>
                  <tr>
                    <th scope="col">S.No</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Action</th>
                    <th scope="col">Action At</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($activity_logs as $key => $activity_log)
                    <tr>
                      <td>{{++$key}}</td>
                      <td>{{$activity_log->user->name}}</td>
                      <td>{{$activity_log->user->email}}</td>
                      <td>{{$activity_log->action}}</td>
                      <td>{{$activity_log->created_at}}</td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              <!-- End Table with stripped rows -->

            </div>
          </div>

        </div>
      </div>
  </div>
    </section>

  </main><!-- End #main -->
  <!-- Bootstrap core JavaScript -->
  <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
 
@endsection
