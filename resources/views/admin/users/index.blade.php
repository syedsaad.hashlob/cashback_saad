@extends('admin.layouts.app')

@section('content')
<link href="{{ url('')}}/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<main id="main" class="main">

    <div class="pagetitle">
        <h1>View Users</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('admin.home')}}">Home</a></li>
                <li class="breadcrumb-item">Tables</li>
                <li class="breadcrumb-item active">Data</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body">'
                        <h5 class="card-title">View Users</h5>
                        @if (session('success'))
                        <p class="alert alert-success">{{session('success')}}</p>
                        @endif
                        <!-- Table with stripped rows -->
                        <table class="table datatable">
                            <thead>
                                <tr>
                                    <th scope="col">S.No</th>
                                    <th scope="col">User Account ID</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Current Points</th>
                                    <th scope="col">View Referals</th>
                                    <th scope="col">View Summary</th>
                                    <th scope="col">View Action History</th>
                                    <th scope="col">Delete All Transactions</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                <tr>
                                    <td>{{$user->id}}</td>
                                    <td>{{$user->user_account_id}}</td>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>{{$user->current_points ?? 0}}</td>
                                    <td>
                                        <a href="{{route('user.referals',$user->id)}}" class="btn btn-primary">View</a>
                                    </td>
                                    <td>
                                        <a href="{{route('user.summary',$user->id)}}" class="btn btn-primary">View</a>
                                    </td>
                                    <td>
                                        <a href="{{route('user_action.history',$user->id)}}"
                                            class="btn btn-primary">View</a>
                                    </td>
                                    <td>
                                        <button type="button" data-toggle="modal" data-target="#transactionexampleModal"
                                            data-id="{{$user->id}}" class="btn btn-danger transactiondelete">Tran
                                            Delete</button>
                                    </td>
                                    <td>
                                        <a class="btn btn-primary" href="{{route('user.status',$user->id)}}">
                                            {{$user->status == 1 ? 'Active' : 'InActive'}}
                                        </a>
                                        {{-- <a href="" class="delete" > --}}
                                        <button type="submit" data-toggle="modal" data-target="#deleteexampleModal"
                                            data-id="{{$user->id}}" class="btn btn-danger delete">Delete</button>
                                        {{-- </a> --}}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <!-- End Table with stripped rows -->

                    </div>
                </div>

            </div>
        </div>
        <div class="modal fade" id="deleteexampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4>Confirm</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <div class="row">
                                <p>Are you sure you wants to delete this</p>
                                <div class="form-group col-md-4">
                                    <button type="button" class="btn btn-info btn-block">Cancel</button>
                                </div>
                                <div class="form-group col-md-4">
                                    <a href="" id="confirm_delete">
                                        <button type="button" class="btn btn-info btn-danger">Delete</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="modal fade" id="transactionexampleModal" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4>Confirm</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <div class="row">
                                <p>Are you sure you wants to delete all transactions of this user</p>
                                <div class="form-group col-md-4">
                                    <button type="button" class="btn btn-info btn-block">Cancel</button>
                                </div>
                                <div class="form-group col-md-4">
                                    <a href="" id="confirm_transaction">
                                        <button type="button" class="btn btn-info btn-danger">Delete</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        </div>
    </section>

</main><!-- End #main -->
<!-- Bootstrap core JavaScript -->
<script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<script>
    $(document).ready(function () {
        $('.delete').click(function () {
            var id = $(this).data('id');
            console.log(id);
            var url = '{{route("user.delete",[":id"])}}';
            url = url.replace(':id', id);
            $('#confirm_delete').attr('href', url);
        });
        $('.transactiondelete').click(function () {
            var id = $(this).data('id');
            console.log(id);
            var url = '{{route("user.transactiondelete",[":id"])}}';
            url = url.replace(':id', id);
            $('#confirm_transaction').attr('href', url);
        });
    });

</script>
@endsection
