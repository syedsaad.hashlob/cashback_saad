@extends('admin.layouts.app')

@section('content')
<main id="main" class="main">

    <div class="pagetitle">
      <h1>View Referals</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
          <li class="breadcrumb-item">Tables</li>
          <li class="breadcrumb-item active">Data</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

    <section class="section">
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <div class="card-body">
              <h5 class="card-title">View Referals</h5>

  
              <!-- Table with stripped rows -->
              <table class="table datatable">
                <thead>
                  <tr>
                    <th scope="col">S.No</th>
                    <th scope="col">User Account ID</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Current Points</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($referrals->referrals as $user)
                    <tr>
                      <td>{{$user->referralUser->id}}</td>
                      <td>{{$user->referralUser->user_account_id}}</td>
                      <td>{{$user->referralUser->name}}</td>
                      <td>{{$user->referralUser->email}}</td>
                      <td>{{$user->referralUser->current_points ?? 0}}</td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              <!-- End Table with stripped rows -->

            </div>
          </div>

        </div>
      </div>
    </section>

  </main><!-- End #main -->

@endsection
