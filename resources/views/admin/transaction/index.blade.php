@extends('admin.layouts.app')

@section('content')
<main id="main" class="main">

    <div class="pagetitle">
      <h1>WithDraw Request</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{url('admin.home')}}">Home</a></li>
          <li class="breadcrumb-item">Tables</li>
          <li class="breadcrumb-item active">Data</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

    <section class="section">
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <div class="card-body">
              <h5 class="card-title">WithDraw Request</h5>

              <!-- Table with stripped rows -->
              <table class="table datatable">
                <thead>
                  <tr>
                    <th scope="col">S.No</th>
                    <th scope="col">User Account ID</th>
                    <th scope="col">Name</th>
                    <th scope="col">Payment Method</th>
                    <th scope="col">Amount</th>
                    <th scope="col">Transaction Date</th> 
                    <th scope="col">Status</th> 
                    <th scope="col">Action</th> 
                  </tr>
                </thead>
                <tbody>
                  @foreach($transactions as $transaction)
                    <tr>
                      <td>{{$transaction->id}}</td>
                      <td>{{$transaction->user->user_account_id}}</td>
                      <td>{{$transaction->user->name}}</td>
                      <td>{{$transaction->paymentMethod->name ?? 0}}</td>
                      <td>{{$transaction->amount ?? 0}}</td>
                      <td>{{$transaction->created_at}}</td>
                      <td class="withdraw-status">{{$transaction->status}}</td>
                      <td>
                        <select class="form-select status" data-id="{{$transaction->id}}" required name="status" >
                          <option value="" disabled selected>Action</option>
                          <a href="{{route('withdraw_request_status',[$transaction->id,'Pending'])}}"><option value="Pending" >Pending</option></a>
                          <a href="{{route('withdraw_request_status',[$transaction->id,'Successfull'])}}"><option value="Successfull" >Successfull</option></a>
                          <a href="{{route('withdraw_request_status',[$transaction->id,'Rejected'])}}"><option value="Rejected" >Rejected</option></a>
                        </select>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              <!-- End Table with stripped rows -->

            </div>
          </div>

        </div>
      </div>
    </section>

  </main><!-- End #main -->
  @section('page-script')
      
  
  <script>
    $(document).ready(function(){
      $('.status').change(function () {
        var $this = $(this);
        var id = $this.data('id');
        var status = $this.val();
        // var status = this.checked;

        
        axios
            .post('{{route("withdraw_request_status")}}', {
                _token: '{{csrf_token()}}',
                _method: 'post',
                id: id,
                status: status,
            })
            .then(function (responsive) {
                console.log(responsive);
                $('.withdraw-status').html(status);
                Swal.fire(
                    'Updated!',
                    'status has been updated.',
                    'success'
                )
            })
            .catch(function (error) {
                console.log(error);
            });
    });

    });
  </script>
@endsection
@endsection
