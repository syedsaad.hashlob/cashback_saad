@extends('admin.layouts.app')

@section('content')
    <main id="main" class="main">

        <div class="pagetitle">
        <h1>Dashboard</h1>
        <nav>
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
            </ol>
        </nav>
        </div><!-- End Page Title -->

        <section class="section dashboard">
        <div class="row">

            <!-- Left side columns -->
            <div class="col-lg-12">
            <div class="row">

                <!-- Sales Card -->
                <div class="col-xxl-4 col-md-6">
                <div class="card info-card sales-card">

                    <div class="card-body">
                    <h5 class="card-title">Total Brokers </h5>

                    <div class="d-flex align-items-center">
                        <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                        <i class="bi bi-cart"></i>
                        </div>
                        <div class="ps-3">
                        <h6>{{$total_brokers ?? 0}}</h6>
                        {{-- <span class="text-success small pt-1 fw-bold">12%</span> <span class="text-muted small pt-2 ps-1">increase</span> --}}

                        </div>
                    </div>
                    </div>

                </div>
                </div><!-- End Sales Card -->

                <!-- Revenue Card -->
                <div class="col-xxl-4 col-md-6">
                <div class="card info-card revenue-card">

                    <div class="card-body">
                    <h5 class="card-title">Total Users</h5>
                        
                    <div class="d-flex align-items-center">
                        <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                        <i class="bi bi-currency-dollar"></i>
                        </div>
                        <div class="ps-3">
                        <h6>{{$total_users ?? 0}}</h6>
                        {{-- <span class="text-success small pt-1 fw-bold">8%</span> <span class="text-muted small pt-2 ps-1">increase</span> --}}

                        </div>
                    </div>
                    </div>

                </div>
                </div>
                <!-- End Revenue Card -->

                <!-- Customers Card -->
                <div class="col-xxl-4 col-xl-12">

                    <div class="card info-card customers-card">
                        <div class="card-body">
                        <h5 class="card-title">Total Trading Amount </h5>

                        <div class="d-flex align-items-center">
                            <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                            <i class="bi bi-people"></i>
                            </div>
                            <div class="ps-3">
                            <h6>${{$total_trading_ammount ?? 0}}</h6>
                            {{-- <span class="text-danger small pt-1 fw-bold">12%</span> <span class="text-muted small pt-2 ps-1">decrease</span> --}}

                            </div>
                        </div>

                        </div>
                    </div>

                </div>
                <!-- End Customers Card -->

                <!-- Recent Sales -->
                <div class="col-12">
                    <div class="card recent-sales">

                        <div class="card-body">
                        <h5 class="card-title">Recent User Trading </h5>

                        <table class="table table-borderless datatable">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Customer</th>
                                <th scope="col">Broker</th>
                                <th scope="col">Trading Amount</th>
                                {{-- <th scope="col">Status</th> --}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($recent_users as $key => $user)
                            @php
                            $broker = $user->userBookerAccount->last();
                            $broker_amount = getBrockerByIdAndAccountId($broker->user_account_id,$broker->broker_id);
                                // dd($broker->broker);
                                
                            @endphp
                            <tr>
                                <th scope="row"><a href="#">#{{++$key}}</a></th>
                                <td>{{$user->name}}</td>
                                <td><a href="#" class="text-primary">{{$broker->broker->name}}</a></td>
                                <td>${{$broker_amount['sum_amount']}}</td>
                            </tr>
                            @endforeach
                            
                            </tbody>
                        </table>

                        </div>

                    </div>
                </div>
                <!-- End Recent Sales -->

                <!-- Top Selling -->
                
            </div>
            </div><!-- End Left side columns -->

            <!-- Right side columns -->
   
        </div>
        </section>

    </main><!-- End #main -->
@endsection
