@extends('admin.layouts.app')

@section('content')
<main id="main" class="main">

    <div class="pagetitle">
      <h1>View Brokers</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{url('admin.home')}}">Home</a></li>
          <li class="breadcrumb-item">Tables</li>
          <li class="breadcrumb-item active">Data</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

    <section class="section">
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <div class="card-body">
              <h5 class="card-title">View Brokers</h5>

            <a href="{{route('broker.create')}}"> Add Broker</a>

              <!-- Table with stripped rows -->
              <table class="table datatable">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">images</th>
                    <th scope="col">Name</th>
                    <th scope="col">Description</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($brokers as $broker)
                  <tr>
                    <td> {{ $broker->id}}</td>
                  <td><img src="{{ url('').'/uploads/'. $broker->image}}" width="50" height="50" class="rounded-circle" /></td>
                    <td>{{ $broker->name}}</td>
                    <td>{{ $broker->description}}</td>
                    <td>
                        <a href="{{route('broker.edit',$broker->id)}}" class="btn btn-primary">Edit</a>

                        <form action="{{ route('broker.destroy',$broker->id) }}" method="POST">

                            {{-- <a class="btn btn-info" href="{{ route('broker.show',$broker->id) }}">Show</a> --}}

                            {{-- <a class="btn btn-primary" href="{{ route('broker.edit',$broker->id) }}">Edit</a> --}}

                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </td>

                  </tr>
                  @endforeach
                </tbody>
              </table>
              <!-- End Table with stripped rows -->

            </div>
          </div>

        </div>
      </div>
    </section>

  </main><!-- End #main -->

@endsection
