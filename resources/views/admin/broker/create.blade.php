@extends('admin.layouts.app')

@section('content')
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Add Broker </h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('admin.home')}}">Home</a></li>
                <li class="breadcrumb-item">Broker</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->
    <section class="section">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Add Broker</h5>

                        <!-- Multi Columns Form -->
                        <form class="row g-3" method="post" enctype="multipart/form-data"
                            action="{{$isEdit ?  route('broker.update',$broker->id) : route('broker.store') }}">
                            @csrf
                            @if ($isEdit)
                            @method('put')
                            @endif
                            <div class="col-md-12">
                                <label for="inputName1" class="form-label">Broker Title</label>
                                <input value="{{$isEdit ? $broker->name : old('name')}}" type="text"
                                    class="form-control" name="name" id="inputName1">
                                <span class="text-danger">{{$errors->first('name') ?? null}}</span>
                            </div>
                            <div class="col-md-12">
                                <label for="inputEmail2" class="form-label">Broker Description</label>
                                <input value="{{$isEdit ? $broker->description : old('description')}}" type="text"
                                    class="form-control" name="description" id="inputEmail2">
                                <span class="text-danger">{{$errors->first('description') ?? null}}</span>
                            </div>
                            <div class="col-md-12">
                                <label for="inputEmail2" class="form-label">Broker Website Link</label>
                                <input value="{{$isEdit ? $broker->web_link : old('web_link')}}" type="text"
                                    class="form-control" name="web_link" id="inputEmail2">
                                <span class="text-danger">{{$errors->first('web_link') ?? null}}</span>
                            </div>
                            <div class="col-md-12">
                                <label for="inputEmail3" class="form-label">Broker Image</label>
                                <input type="file" class="form-control" name="image" id="inputEmail3">
                                @if ($isEdit)
                                <img src="{{url('').'/uploads/'.$broker->image}}" height="100px">
                                @endif
                                <span class="text-danger">{{$errors->first('image') ?? null}}</span>
                            </div>
                            <div class="text-end">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form><!-- End Multi Columns Form -->


                    </div>
                </div>
            </div>
    </section>

</main><!-- End #main -->
@endsection
