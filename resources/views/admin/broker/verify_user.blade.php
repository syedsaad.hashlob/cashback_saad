@extends('admin.layouts.app')

@section('content')
<main id="main" class="main">

    <div class="pagetitle">
      <h1>View UnApproved Booker Accounts</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{url('admin.home')}}">Home</a></li>
          <li class="breadcrumb-item">Tables</li>
          <li class="breadcrumb-item active">Data</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

    <section class="section">
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <div class="card-body">
              <h5 class="card-title">View UnApproved Booker Accounts</h5>

              <!-- Table with stripped rows -->
              <table class="table datatable">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Broker</th>
                    <th scope="col">Account</th>
                    <th scope="col">Status</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($unverify_users as $key => $verify)
                  <tr>
                    <td> {{ ++$key }}</td>
                    <td>{{ $verify->user->name}}</td>
                    <td>{{ $verify->user->email}}</td>
                    <td>{{ $verify->broker->name}}</td>
                    <td>{{ $verify->user_account_id}}</td>
                    <td>
                      @if ($verify->approved == 1)
                      <a href="{{route('verify.user.booker.update',$verify->id)}}" class="btn btn-success">Approved</a>    
                      
                      @else
                          <a href="{{route('verify.user.booker.update',$verify->id)}}" class="btn btn-primary">Dis-Approved</a>
                      @endif
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
              <!-- End Table with stripped rows -->

            </div>
          </div>

        </div>
      </div>
    </section>

  </main><!-- End #main -->

@endsection
