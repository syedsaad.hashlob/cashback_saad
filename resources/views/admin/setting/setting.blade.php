@extends('admin.layouts.app')

@section('content')
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Settings </h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('admin.home')}}">Home</a></li>
                <li class="breadcrumb-item">Settings</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->
    <section class="section">
        <div class="row">
            <div class="col-lg-12">
                @if (session('success'))
                    <p class="alert alert-success">{{session('success')}}</p>
                @endif
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Settings</h5>

                        <!-- Multi Columns Form -->
                        <form class="row g-3" method="post" enctype="multipart/form-data" action="{{ route('settings.update') }}">
                            @csrf
                            <div class="row">
                                <h5>Bronze</h5>
                                <div class="col-md-4">
                                    <label for="inputName1" class="form-label">Start Points</label>
                                    <input type="text" class="form-control" name="points[bronze][]" value="{{getScoringPoints('bronze')['from'] ?? null}}" id="inputName1">
                                </div>
                                <div class="col-md-4">
                                    <label for="inputName1" class="form-label">To Points</label>
                                    <input type="text" class="form-control" name="points[bronze][]" value="{{getScoringPoints('bronze')['to'] ?? null}}" id="inputName1">
                                </div>
                                <div class="col-md-4">
                                    <label for="inputName1" class="form-label">Percentage</label>
                                    <input type="text" class="form-control" name="percentage[percentage_bronze]" value="{{getScoringPercentage('percentage_bronze')}}" id="inputName1">
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <h5>Silver</h5>
                                <div class="col-md-4">
                                    <label for="inputName1" class="form-label">Start Points</label>
                                    <input type="text" class="form-control" name="points[silver][]" value="{{getScoringPoints('silver')['from'] ?? null}}" id="inputName1">
                                </div>
                                <div class="col-md-4">
                                    <label for="inputName1" class="form-label">To Points</label>
                                    <input type="text" class="form-control" name="points[silver][]" value="{{getScoringPoints('silver')['to'] ?? null}}" id="inputName1">
                                </div>
                                <div class="col-md-4">
                                    <label for="inputName1" class="form-label">Percentage</label>
                                    <input type="text" class="form-control" name="percentage[percentage_silver]" value="{{getScoringPercentage('percentage_silver')}}" id="inputName1">
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <h5>Gold</h5>
                                <div class="col-md-4">
                                    <label for="inputName1" class="form-label">Start Points</label>
                                    <input type="text" class="form-control" name="points[gold][]" value="{{getScoringPoints('gold')['from'] ?? null}}" id="inputName1">
                                </div>
                                <div class="col-md-4">
                                    <label for="inputName1" class="form-label">To Points</label>
                                    <input type="text" class="form-control" name="points[gold][]" value="{{getScoringPoints('gold')['to'] ?? null}}" id="inputName1">
                                </div>
                                <div class="col-md-4">
                                    <label for="inputName1" class="form-label">Percentage</label>
                                    <input type="text" class="form-control" name="percentage[percentage_gold]" value="{{getScoringPercentage('percentage_gold')}}" id="inputName1">
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <h5>Platinum</h5>
                                <div class="col-md-4">
                                    <label for="inputName1" class="form-label">Start Points</label>
                                    <input type="text" class="form-control" name="points[platinum][]" value="{{getScoringPoints('platinum')['from'] ?? null}}" id="inputName1">
                                </div>
                                <div class="col-md-4">
                                    <label for="inputName1" class="form-label">To Points</label>
                                    <input type="text" class="form-control" name="points[platinum][]" value="{{getScoringPoints('platinum')['to'] ?? null}}" id="inputName1">
                                </div>
                                <div class="col-md-4">
                                    <label for="inputName1" class="form-label">Percentage</label>
                                    <input type="text" class="form-control" name="percentage[percentage_platinum]" value="{{getScoringPercentage('percentage_platinum')}}" id="inputName1">
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <h5>Diamond</h5>
                                <div class="col-md-4">
                                    <label for="inputName1" class="form-label">Start Points</label>
                                    <input type="text" class="form-control" name="points[diamond][]" value="{{getScoringPoints('diamond')['from'] ?? null}}" id="inputName1">
                                </div>
                                <div class="col-md-4">
                                    <label for="inputName1" class="form-label">To Points</label>
                                    <input type="text" class="form-control" name="points[diamond][]" value="{{getScoringPoints('diamond')['to'] ?? null}}" id="inputName1">
                                </div>
                                <div class="col-md-4">
                                    <label for="inputName1" class="form-label">Percentage</label>
                                    <input type="text" class="form-control" name="percentage[percentage_diamond]" value="{{getScoringPercentage('percentage_diamond')}}" id="inputName1">
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <h5>Emerald </h5>
                                <div class="col-md-4">
                                    <label for="inputName1" class="form-label">Start Points</label>
                                    <input type="text" class="form-control" name="points[emerald][]" value="{{getScoringPoints('emerald')['from'] ?? null}}" id="inputName1">
                                </div>
                                <div class="col-md-4">
                                    <label for="inputName1" class="form-label">To Points</label>
                                    <input type="text" class="form-control" name="points[emerald][]" value="{{getScoringPoints('emerald')['from'] ?? null}}" id="inputName1">
                                </div>
                                <div class="col-md-4">
                                    <label for="inputName1" class="form-label">Percentage</label>
                                    <input type="text" class="form-control" name="percentage[percentage_emerald]" value="{{getScoringPercentage('percentage_emerald')}}" id="inputName1">
                                </div>
                            </div>
                            <hr><br>
                            <div class="row">
                                <h3 style="font-weight: bolder">Site Setting </h3>
                                <div class="col-md-4">
                                    <label for="inputName1" class="form-label">Transaction Percentage Fee </label>
                                    <input type="text" class="form-control" name="transaction_fee_percentage" value="{{getSetting('transaction_fee_percentage')}}" id="inputName1">
                                </div>
                                <div class="col-md-4">
                                    <label for="inputName1" class="form-label">Min Withdraw Amount</label>
                                    <input type="text" class="form-control" name="min_withdraw_amount" value="{{getSetting('min_withdraw_amount')}}" id="inputName1">
                                </div>
                            </div>
                            <div class="text-end">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form><!-- End Multi Columns Form -->
                        <a href="{{route('monthly-level-down')}}">
                            <button type="button" class="btn btn-primary">Level Down</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main><!-- End #main -->
@endsection
