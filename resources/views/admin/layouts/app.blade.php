<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>FxCashbacks</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    {{-- dashboard links Cs --}}

<!-- Favicons -->
<link href="{{ url('')}}/assets/img/favicon.png" rel="icon">
<link href="{{ url('')}}/assets/img/apple-touch-icon.png" rel="apple-touch-icon">

<!-- Google Fonts -->
<link href="https://fonts.gstatic.com" rel="preconnect">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

<!-- Vendor CSS Files -->
<link href="{{ url('')}}/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="{{ url('')}}/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
<link href="{{ url('')}}/assets/vendor/remixicon/remixicon.css" rel="stylesheet">
<link href="{{ url('')}}/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
<link href="{{ url('')}}/assets/vendor/quill/quill.snow.css" rel="stylesheet">
<link href="{{ url('')}}/assets/vendor/quill/quill.bubble.css" rel="stylesheet">
<link href="{{ url('')}}/assets/vendor/simple-datatables/style.css" rel="stylesheet">
<link rel="stylesheet" href="{{url('')}}/assets/sweetalert2/sweetalert2.min.css">

<!-- Template Main CSS File -->
<link href="{{ url('')}}/assets/css/style.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.4/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css">

<!-- =======================================================
* Template Name: NiceAdmin - v2.1.0
* Template URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
* Author: BootstrapMade.com
* License: https://bootstrapmade.com/license/
======================================================== -->


    {{-- dashboard links Cs --}}


</head>
<body>


    @include('admin.layouts.header')
    @include('admin.layouts.sidebar')
    @yield('content')

 <!-- ======= Footer ======= -->
<footer id="footer" class="footer">
    <div class="copyright">
      &copy; Copyright <strong><span>Cashback</span></strong>. All Rights Reserved
    </div>
    <div class="credits">
      <!-- All the links in the footer should remain intact. -->
      <!-- You can delete the links only if you purchased the pro version. -->
      <!-- Licensing information: https://bootstrapmade.com/license/ -->
      <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/ -->
      Designed by <a href="https://hashlob.com/">Hashlob</a>
    </div>
</footer><!-- End Footer -->








    {{-- <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ route('admin.home') }}">
                    Admin
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->


                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest('admin')
                            <li><a class="nav-link" href="{{ route('admin.login') }}">{{ __('Admin Login') }}</a></li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('admin.logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>  --}}

    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

    <!-- Vendor JS Files -->
    <script src="{{url('')}}/vendor/jquery/jquery.min.js"></script>

    <script src="{{ url('')}}/assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <script src="{{ url('')}}/assets/vendor/php-email-form/validate.js"></script>
    <script src="{{ url('')}}/assets/vendor/quill/quill.min.js"></script>
    <script src="{{ url('')}}/assets/vendor/tinymce/tinymce.min.js"></script>
    <script src="{{ url('')}}/assets/vendor/simple-datatables/simple-datatables.js"></script>
    <script src="{{ url('')}}/assets/vendor/chart.js/chart.min.js"></script>
    <script src="{{ url('')}}/assets/vendor/apexcharts/apexcharts.min.js"></script>
    <script src="{{ url('')}}/assets/vendor/echarts/echarts.min.js"></script>
    <script src="{{ url('')}}/assets/js/main.js"></script>
    <!-- END CORE PLUGINS -->
    <script src="{{url('')}}/assets/axios/axios.min.js"></script>
    {{-- SweetAlert2 --}}
    <script src="{{url('')}}/assets/sweetalert2/sweetalert2.min.js" charset="UTF-8"></script>
  
    @yield('page-script')
</body>
</html>
