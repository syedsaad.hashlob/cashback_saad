  <!-- ======= Sidebar ======= -->
  <aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">

      <li class="nav-item">
        <a class="nav-link " href="{{route('admin.home')}}">
          <i class="bi bi-grid"></i>
          <span>Dashboard</span>
        </a>
      </li><!-- End Dashboard Nav -->
      @if (Auth::guard('admin')->user()->user_type == 1)
      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#components-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-menu-button-wide"></i><span>Admin Accounts</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="components-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <li>
            <a href="{{route('accounts.create')}}">
              <i class="bi bi-circle"></i><span>Add Account</span>
            </a>
          </li>
          <li>
            <a href="{{route('accounts.index')}}">
              <i class="bi bi-circle"></i><span>View Account</span>
            </a>
          </li>

        </ul>
      </li><!-- End Components Nav --> 
      @endif
      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#broker-components-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-menu-button-wide"></i><span>Brokers</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="broker-components-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <li>
            <a href="{{route('broker.create')}}">
              <i class="bi bi-circle"></i><span>Add Broker</span>
            </a>
          </li>
          <li>
            <a href="{{route('broker.index')}}">
              <i class="bi bi-circle"></i><span>View Broker</span>
            </a>
          </li>

        </ul>
      </li><!-- End Components Nav --> 

      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#payment-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-menu-button-wide"></i><span>Payment Methods</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="payment-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <li>
            <a href="{{route('payment.create')}}">
              <i class="bi bi-circle"></i><span>Add Payment Method</span>
            </a>
          </li>
          <li>
            <a href="{{route('payment.index')}}">
              <i class="bi bi-circle"></i><span>View Payment Method</span>
            </a>
          </li>

        </ul>
      </li><!-- End Components Nav -->
      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#forms-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-journal-text"></i><span>Users</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="forms-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <li>
            <a href="{{route('user.index')}}">
              <i class="bi bi-circle"></i><span>View Users</span>
            </a>
          </li>

        </ul>
      </li><!-- End Forms Nav -->
     
      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#verify_user_forms-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-journal-text"></i><span>Verify Booker User </span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="verify_user_forms-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <li>
            <a href="{{route('verify.user.booker')}}">
              <i class="bi bi-circle"></i><span>View</span>
            </a>
          </li>

        </ul>
      </li><!-- End Forms Nav -->
      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#export_forms-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-journal-text"></i><span>Import User Sheets</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="export_forms-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <li>
            <a href="{{route('Trade.index')}}">
              <i class="bi bi-circle"></i><span>Import</span>
            </a>
          </li>

        </ul>
      </li><!-- End Forms Nav -->
      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#transaction_forms-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-journal-text"></i><span>WithDraw Request</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="transaction_forms-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <li>
            <a href="{{route('transaction.index')}}">
              <i class="bi bi-circle"></i><span>View</span>
            </a>
          </li>

        </ul>
      </li><!-- End Forms Nav -->
      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#activity-forms-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-journal-text"></i><span>Activity Logs</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="activity-forms-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <li>
            <a href="{{route('activity-logs')}}">
              <i class="bi bi-circle"></i><span>View Activity Logs</span>
            </a>
          </li>

        </ul>
      </li><!-- End Forms Nav -->
      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#report-forms-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-journal-text"></i><span>Reports</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="report-forms-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <li>
            <a href="{{route('user-report-account')}}">
              <i class="bi bi-circle"></i><span>User Summary Report </span>
            </a>
          </li>

        </ul>
      </li><!-- End Forms Nav -->
      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#settings_forms-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-journal-text"></i><span>Settings</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="settings_forms-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <li>
            <a href="{{route('settings')}}">
              <i class="bi bi-circle"></i><span>Settings</span>
            </a>
          </li>

        </ul>
      </li><!-- End Forms Nav -->
    </ul>

</aside><!-- End Sidebar-->
