@extends('admin.layouts.app')

@section('content')
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Add Payment Method </h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('admin.home')}}">Home</a></li>
                <li class="breadcrumb-item">Payment Method</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->
    <section class="section">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Add Payment Method</h5>

                        <!-- Multi Columns Form -->
                        <form class="row g-3" method="post" enctype="multipart/form-data"
                            action="{{ route('payment.store') }}">
                            @csrf
                            <div class="col-md-12">
                                <label for="inputName1" class="form-label">Payment Title</label>
                                <input type="text" class="form-control" name="name" id="inputName1">
                            </div>
                            <div class="col-md-12">
                                <label for="inputEmail3" class="form-label">Payment Image</label>
                                <input type="file" class="form-control" name="image" id="inputEmail3">
                            </div>
                            <div class="text-end">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form><!-- End Multi Columns Form -->

                    </div>
                </div>
            </div>
        </div>
    </section>

</main><!-- End #main -->
@endsection
