@extends('admin.layouts.app')

@section('content')
<main id="main" class="main">

    <div class="pagetitle">
      <h1>View Payment Method</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{url('admin.home')}}">Home</a></li>
          <li class="breadcrumb-item">Tables</li>
          <li class="breadcrumb-item active">Data</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

    <section class="section">
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <div class="card-body">
              <h5 class="card-title">View Payment</h5>

            <a href="{{route('payment.create')}}"> Add payment</a>

              <!-- Table with stripped rows -->
              <table class="table datatable">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">images</th>
                    <th scope="col">Name</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($payment as $pay)
                  <tr>
                    <td> {{ $pay->id}}</td>
                    <td><img src="{{ url('').'/uploads/'. $pay->image}}" width="50" height="50" class="rounded-circle" /></td>
                    <td>{{ $pay->name}}</td>
                    <td>
                        <form action="{{ route('payment.destroy',$pay->id) }}" method="POST">

                            {{-- <a class="btn btn-info" href="{{ route('payment.show',$pay->id) }}">Show</a> --}}

                            {{-- <a class="btn btn-primary" href="{{ route('payment.edit',$pay->id) }}">Edit</a> --}}

                            @csrf
                            @method('DELETE')

                            <a class="btn btn-info" href="{{ route('payment.edit',$pay->id) }}">Edit</a>
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </td>

                  </tr>
                  @endforeach
                </tbody>
              </table>
              <!-- End Table with stripped rows -->

            </div>
          </div>

        </div>
      </div>
    </section>

  </main><!-- End #main -->

@endsection
