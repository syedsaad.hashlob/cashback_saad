@extends('admin.layouts.app')

@section('content')
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Edit Payment Method </h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('admin.home')}}">Home</a></li>
                <li class="breadcrumb-item">Payment Method</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->
<section class="section">

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('payment.update',$payment->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')

         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Name:</strong>
                    <input type="text" name="name" value="{{ $payment->name }}" class="form-control" placeholder="Name">
                </div>
            </div>
            <div class="col-md-12">
                <label for="inputEmail3" class="form-label">Payment Image</label>
                <input type="file" class="form-control" name="image" id="inputEmail3" value="{{ $payment->image }}">
                <br>
                <img src="{{url('').'/uploads/'.$payment->image}}" alt="" height="100px">
              </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>

    </form>
</section>
</main>
@endsection
