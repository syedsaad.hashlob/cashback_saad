@extends('admin.layouts.app')

@section('content')
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Edit Account </h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('admin.home')}}">Home</a></li>
                <li class="breadcrumb-item">Account</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->
<section class="section">

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('accounts.update',$account->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')

         <div class="row">
            <div class="col-md-6">
                <label for="inputName1" class="form-label">Name</label>
                <input type="text" class="form-control" name="name" placeholder="Enter Name" value="{{$account->name}}" id="inputName1">
            </div>
            <div class="col-md-6">
                <label for="inputEmail3" class="form-label">Email</label>
                <input type="text" class="form-control" name="email" placeholder="Enter Email" value="{{$account->email}}" id="inputEmail3">
            </div>
            <div class="col-md-6">
                <label for="inputEmail3" class="form-label">Password</label>
                <input type="password" class="form-control" placeholder="*********" name="password" id="inputEmail3">
            </div>
            <div class="col-md-6">
                <label for="inputEmail3" class="form-label">Confirm Passowrd</label>
                <input type="password" class="form-control" placeholder="*********" name="password_confirmation" id="inputEmail3">
            </div>
            <div class="col-md-6">
                <label for="inputEmail3" class="form-label">Select Role</label>
                <select class="form-select" required name="user_type" aria-label="Default select example">
                    <option value="0" {{$account->user_type == 0 ? 'selected' : null }}>Admin</option>
                    <option value="1" {{$account->user_type == 1 ? 'selected' : null }}>Super Admin</option>
                </select>
            </div>
            {{-- <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div> --}}
        </hr>
        <div class="text-end">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        </div>

    </form>
</section>
</main>
@endsection
