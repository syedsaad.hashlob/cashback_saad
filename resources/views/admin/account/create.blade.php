@extends('admin.layouts.app')

@section('content')
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Add Account </h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('admin.home')}}">Home</a></li>
                <li class="breadcrumb-item">Account</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->
    <section class="section">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Add Account</h5>

                        <!-- Multi Columns Form -->
                        <form class="row g-3" method="post" enctype="multipart/form-data"
                            action="{{ route('accounts.store') }}">
                            @csrf
                            <div class="col-md-6">
                                <label for="inputName1" class="form-label">Name</label>
                                <input type="text" class="form-control" placeholder="Enter Name" name="name" id="inputName1">
                            </div>
                            <div class="col-md-6">
                                <label for="inputEmail3" class="form-label">Email</label>
                                <input type="text" class="form-control" placeholder="Enter Email" name="email" id="inputEmail3">
                            </div>
                            <div class="col-md-6">
                                <label for="inputEmail3" class="form-label">Password</label>
                                <input type="password" class="form-control" placeholder="*********" name="password" id="inputEmail3">
                            </div>
                            <div class="col-md-6">
                                <label for="inputEmail3" class="form-label">Confirm Passowrd</label>
                                <input type="password" class="form-control" placeholder="*********" name="password_confirmation" id="inputEmail3">
                            </div>
                            <div class="col-md-6">
                                <label for="inputEmail3" class="form-label">Select Role</label>
                                <select class="form-select" required name="user_type" aria-label="Default select example">
                                    <option value="0" selected>Admin</option>
                                    <option value="1">Super Admin</option>
                                </select>
                            </div>
                        </hr>
                            <div class="text-end">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form><!-- End Multi Columns Form -->

                    </div>
                </div>
            </div>
        </div>
    </section>

</main><!-- End #main -->
@endsection
