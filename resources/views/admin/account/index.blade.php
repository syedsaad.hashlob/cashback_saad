@extends('admin.layouts.app')

@section('content')
<main id="main" class="main">

    <div class="pagetitle">
      <h1>View Accounts</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{url('admin.home')}}">Home</a></li>
          <li class="breadcrumb-item">Tables</li>
          <li class="breadcrumb-item active">Data</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

    <section class="section">
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <div class="card-body">
              <h5 class="card-title">View Accounts</h5>

            <a href="{{route('accounts.create')}}"> Add Accounts</a>

              <!-- Table with stripped rows -->
              <table class="table datatable">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($accounts as $key => $account)
                    @if ($key != 0)
                    <tr>
                      <td> {{ ++$key}}</td>
                      <td>{{ $account->name}}</td>
                      <td>{{ $account->email}}</td>
                      <td>
                          <form action="{{ route('accounts.destroy',$account->id) }}" method="POST">

                              {{-- <a class="btn btn-info" href="{{ route('account.show',$account->id) }}">Show</a> --}}

                              {{-- <a class="btn btn-primary" href="{{ route('account.edit',$account->id) }}">Edit</a> --}}

                              @csrf
                              @method('DELETE')

                              <a class="btn btn-info" href="{{ route('accounts.edit',$account->id) }}">Edit</a>
                              <button type="submit" class="btn btn-danger">Delete</button>
                          </form>
                      </td>

                    </tr>
                    @endif
                  @endforeach
                </tbody>
              </table>
              <!-- End Table with stripped rows -->

            </div>
          </div>

        </div>
      </div>
    </section>

  </main><!-- End #main -->

@endsection
