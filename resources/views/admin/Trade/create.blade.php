@extends('admin.layouts.app')

@section('content')
<main id="main" class="main">
    <div class="pagetitle">
      <h1>Trade Export </h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
          <li class="breadcrumb-item">Trade</li>
        </ol>
      </nav>
    </div>
    @if (\Session::has('success'))
        <div class="alert alert-success">
            <ul>
                <li>{!! \Session::get('success') !!}</li>
            </ul>
        </div>
    @endif
    <section class="section">
      <div class="row">

        <div class="col-lg-12">
          @if (session('error'))
            <p class="alert alert-danger">{{session('error')}}</p>
          @endif
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Trade Export</h5>
                <form class="row g-3" method="post" enctype="multipart/form-data" action="{{ route('Trade.store') }}">
                    @csrf
                    <div class="col-md-12">
                        <label for="inputEmail3" class="form-label"> File Upload</label>
                        <select class="form-select" required name="broker_id" aria-label="Default select example">
                            <option selected disabled>Select File Type</option>
                            @foreach ($brokers as $broker)
                              <option value="{{$broker->id}}">{{$broker->name}}</option>                                
                            @endforeach
                        </select>
                          <br>
                        <input type="file" class="form-control" name="file" id="inputEmail3">
                    </div>
                    <div class="text-end">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
              </form>
            </div>
          </div>
      </div>
    </section>

  </main>
@endsection
