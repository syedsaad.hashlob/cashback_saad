@extends('layouts.dashboard')

@section('content')

{{-- <section id="withdrawl">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <p class="title">
                        You have pending withdrawals
                    </p>
                    <span class="title_span">You've requested for withdrawals below. Is your withdrawal late? Learn more about our withdrawal process here.</span>

                    <div class="row">

                        <div class="col-md-6">
                            <div class="inner_shadow">
                                <p class="box_paragra"><strong> $520.00</strong> (fee: $18.20)<strong>SKRILL</strong> (omairlodhi@gmail.com) 2021.09.22 19:12:51 cancel withdrawal</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="inner_shadow">
                                <p class="box_paragra"><strong> $520.00</strong> (fee: $18.20)<strong>SKRILL</strong> (omairlodhi@gmail.com) 2021.09.22 19:12:51 cancel withdrawal</p>

                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</section> --}}
<div class="modal fade" id="paymentexampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                Transaction Amount
                            </div>
                            @php
                                // $user_account_id = \App\UserBrokerAccount::where([['user_id',Auth::user()->id]])->get();
                                $trade = getUserTotalEarning(Auth::user()->id);
                                // foreach ($user_account_id as $key => $account) {
                                //     $sum[] = $account['sum_amount'];
                                // }

                                // if(!empty($sum)){
                                //     $trade = array_sum($sum);
                                // }
                            @endphp
                            <div class="panel-body">
                                @if (session('error'))
                                    <p class="alert alert-danger">{{session('error')}}</p>
                                @endif
                                <form action="{{route('transaction.store')}}" method="post">
                                    @csrf
                                    <div class="form-group">
                                        {{-- <h2>Broker</h2> --}}
                                    </div>
                                        <span class="total_amount">Your Amount: ${{isset($trade) ?  $trade : 0}}</span>
                                        <hr>
                                        <div class="form-group">
                                            <input id="type_amount" name="amount" placeholder="Enter Amount" type="text" class="form-control">
                                            <input type="hidden" name="payment_method_id" class="payment_method_id"  value="">
                                        </div>

                                        <div class="form-group">
                                            <button id="signupSubmit" type="submit" class="btn btn-info btn-block">Submit</button>
                                        </div>
                                </form>


                            </div>
                        </div>
                    </div>
                </div>
            </div>

            </div>
        </div>
    </div>
</div>
<section id="manula_withdral_sec">
    @if (session('success'))
        <p class="alert alert-success">{{session('success')}}</p>
    @endif
        <div class="container">
            <div class="row">

                <div class="col-md-12">
                    <h3>Manual Withdrawl Method</h3>
                    <div class="box_inner_shadow_dt">
                        @foreach ($payment_methods as $payment_method)
                        <div class="row row_styling_in_box_dt">
                            <div class="col-md-2">
                                <img src="{{url('')}}/uploads/{{$payment_method->image}}" style="width: 110px;">

                            </div>

                            <div class="col-md-6">
                                <h2>Withdrawl to {{$payment_method->name}} Account</h2>
                                <span> ({{getScoringPercentage('transaction_fee_percentage')}} % transaction fee to be applied)</span>

                                {{-- <div class="row">
                                    <div class="col-md-4">
                                        <ul>
                                            <li><strong>Min/Max</strong></li>
                                            <li>{{$payment_method->min_max}}</li>
                                        </ul>
                                     </div>
                                    <div class="col-md-4">
                                        <ul>
                                            <li><strong>Transaction Fee</strong></li>
                                            <li>{{$payment_method->transaction_fee}}</li>
                                        </ul>
                                    </div>
                                    <div class="col-md-4">
                                        <ul>
                                            <li><strong>Withdrawl Time</strong></li>
                                            <li>{{$payment_method->withdraw_time}}</li>
                                        </ul>
                                    </div>
                                </div> --}}
                            </div>

                            <div class="col-md-4 btn_pos">
                                <button data-toggle="modal" data-target="#paymentexampleModal" type="button" class="btn btn-primary payment_method_area" data-id="{{$payment_method->id}}">SELECT</button>
                            </div>


                        </div>
                        @endforeach

                     
                </div>
                </div>

                {{-- <div class="col-md-4">
                    <h3>Automatic Withdrawl</h3>
                    <div class="box_inner_shadow_active_detail">
                        <p class="check"><i class="fas fa-check"></i></p>
                        <p class="heading_automatic_sec">Automatic withdrawal is active</p>
                        <p class="decrib">The next withdrawal will be on Sunday evening to your account if your balance exceeds $150.</p>
                        <p class="Link">Edit Preferences</p>
                        <p class="Link">Stop Automatic Withdrawl</p>

                    </div>
                </div> --}}

            </div>
        </div>
    </section>
    <!-- Bootstrap core JavaScript -->
    <script src="{{url('')}}/vendor/jquery/jquery.min.js"></script>
    <script src="{{url('')}}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script>
        $(document).ready(function(){
            $('#type_amount').change(function(){
                var type_amount = $(this).val();
                console.log(type_amount);
            });
            $(document).on('click','.payment_method_area',function(){
                var id = $(this).data('id');
                console.log("adfasdfa",id);
                $('.payment_method_id').val(id);
                console.log(id);            
            });
            // $('.payment_method_area').click(function(){
            //     var id = $(this).data('id');
            //     console.log("adfasdfa",id);
            //     $('.payment_method_id').val(id);
            //     console.log(id);            
            // });
        });
    </script>
    <script>
        @if (session('error'))
            $('#paymentexampleModal').modal('show');
        @endif
    </script>
@endsection
