@extends('layouts.dashboard')

@section('content')
<section id="myaccount_sec">


    <div class="container">
        <div class="container">



            <div class="row">
                <div class="col-md-2 bg_row"></div>
                <div class="col-md-2 mg_left text-center">
                    <img src="{{url('')}}/images/logo.png" width="100%">
                </div>
                <div class="col-md-8">
                    <div class="row bg-details ">
                        <div class="col-md-4">
                            <p class="company_name">FX Cashbacks</p>
                            <p class="account_id">Account  <strong class="strong_cl">ID: {{Auth::user()->user_account_id}}</strong></p>
                        </div>
                    </div>
                </div>
            </div>
            @foreach ($brokers as $broker)
            @php
                  //dd(Auth::user()->id,$broker->user_account_id, $broker->broker->id);
                  //dd($brokers);
            @endphp

                <div class="row">
                    <div class="col-md-2 bg_row"></div>
                    <div class="col-md-2 mg_left text-center">

                        <img src="{{url('').'/uploads/'.$broker->broker->image}}" width="100%">

                    </div>
                    <div class="col-md-8">
                        <div class="row bg-details ">



                            <div class="col-md-4">
                                <p class="company_name">{{$broker->broker->name}}</p>
                                <p class="account_id">Account  <strong class="strong_cl">ID: {{$broker->user_account_id}}</strong></p>
                                <td>Amount: <b>${{brokerWiseAmount(Auth::user()->id,$broker->user_account_id, $broker->broker->id)}}</b></td>
                                <td><br>Points: <b>{{brokerWisePoints(Auth::user()->id,$broker->user_account_id, $broker->broker->id)}}</b></td>
                                {{-- <p class="account_archive"> Archive this account</p> --}}
                            </div>


                            {{-- <div class="col-md-4 offset-md-4">
                                <p class="account_archive"><i class="fas fa-flag"></i>&nbsp;Action required send request to the broker!</p>
                                <p class="strong_cl">? What does this means</p>

                            </div> --}}
                        </div>
                    </div>
                </div>
                <hr>
            @endforeach
        </div>
    </div>
</section>
@endsection
