@extends('layouts.dashboard')

@section('content')

<section id="referd-sec">
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="profile_box">
                    <p class="profile_box_p_ra">Your Referred Users- All Time</p>


                </div>
                <div class="referd_box">
                    <table class="table table-responsive">
                        <thead>
                            <tr>
                                <th>EMAIL</th>
                                <th>REFERER</th>
                                <th>ACTUAL LEVEL</th>
                                <th>REGISTERED</th>
                                <th>ACTIVE</th>
                                <th>ACCOUNT</th>
                            </tr>
                        </thead>
                    </table>

                </div>


                <div class="profile_box">
                    <table class="table">

                        <tbody>
                            <tr style="background: aliceblue;">
                                <td class="title">Tier {{$refered_users->count()}} Referral</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><strong>${{getReferalTotalEarning(Auth::user()->referral)}}</strong></td>
                            </tr>
                            @foreach ($refered_users as $user)
                            <tr>
                                <td style="width: 100px;">{{$user->referralUser->email}}</td>
                                <td>{{$user->referralUser->name}}</td>
                                <td>Bronze</td>
                                <td>{{$user->referralUser->created_at->format('Y-m-d')}}</td>
                                <td>{{$user->referralUser->status == 1 ? 'Yes' : 'NO'}}</td>
                                <td><strong>${{getUserTotalEarning($user->referralUser->id)}}</strong></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            {{-- <div class="col-md-4">
                <div class="profile_box">
                    <p class="profile_box_p_ra">Referral Statistics</p>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="ref_box_p_ra">Referral Statistics</p>
                        </div>
                        <div class="col-md-3 offset-md-3">
                            <span class="amount_ref_stat">7</span>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-6">
                            <p class="ref_box_p_ra">Referral Statistics</p>
                        </div>
                        <div class="col-md-3 offset-md-3">
                            <span class="amount_ref_stat">7</span>
                        </div>
                    </div>



                    <div class="row">
                        <div class="col-md-6">
                            <p class="ref_box_p_ra">Referral Statistics</p>
                        </div>
                        <div class="col-md-3 offset-md-3">
                            <span class="amount_ref_stat">7</span>
                        </div>
                    </div>



                    <div class="row">
                        <div class="col-md-12">
                            <p class="ref_detail_cp">If you have any problem with your referred users please contact us at support@cashbackcloud.co. We will be happy to help you in any form.</p>
                        </div>

                    </div>


                </div>
            </div> --}}



        </div>

    </div>

</section>


@endsection
