@extends('layouts.dashboard')

@section('content')

<section id="leader_board">
    <h2>Monthly price pool: $2,000</h2>
    <div class="container">

        <div class="row pd_top">
           
            <div class="col-md-9 offset-md-2">
                <div class="inner_shadow">
                    <div class="row pad-ext_row">
                        <div class="col-md-6"></div>
                        <div class="col-md-3">
                            <p class="Cbc_points">Cbc points in the month</p>
                        </div>
                        <div class="col-md-3">
                            <p class="Cbc_points">Reward in {{\Carbon\Carbon::now()->format('y/m')}}</p>
                        </div>

                    </div>
                    @foreach ($users as $user)
                        
                    
                    <div class="row border_st">

                        <div class="col-md-6">

                            <p class="details">{{$user->name}} &nbsp;</p>
                        </div>



                        <div class="col-md-3">
                        @php
                        $trading_amount         = \App\Trader_trades::where([['user_id',$user->id]])->pluck('Total_Comm')->toArray();   
                        $user_total_earnings    = array_sum($trading_amount);
                        
                        if(intval($user_total_earnings) >= 50){
                            $points = intval($user_total_earnings) / 50;
                            if(intval($user_total_earnings) == 50){
                                $points = 1;
                            }
                        }else{
                            $points = 0;
                        }   
                        @endphp
                            <p class="details">{{$points}} &nbsp;CBC POINTS</p>

                        </div>

                        {{-- <div class="col-md-3">
                            <p class="details">$500 &nbsp;<span class="Cbc_points_mobile ">Reward in 2021/5</span></p>
                        </div> --}}

                    </div>
                    @endforeach

                    <!-- two row -->
                    {{-- <div class="row border_st">

                        <div class="col-md-6">

                            <p class="details"><img src="images/SURINAME.svg"> John******s</p>
                        </div>



                        <div class="col-md-3">
                            <p class="details">0</p>

                        </div>

                        <div class="col-md-3">
                            <p class="details">$500</p>
                        </div>

                    </div>

                    <!-- three -->
                    <div class="row border_st">

                        <div class="col-md-6">

                            <p class="details"><img src="images/SURINAME.svg"> John******s</p>
                        </div>



                        <div class="col-md-3">
                            <p class="details">0</p>

                        </div>

                        <div class="col-md-3">
                            <p class="details">$500</p>
                        </div>

                    </div> --}}

                </div>
            </div>


        </div>
    </div>

</section>

@endsection
