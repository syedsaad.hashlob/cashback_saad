<div class="modal" id="myModal">
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-body">
      <div class="hide" data-step="1" data-title="This is the first step!">
        <h3 class="heading-model text-center" >Welcome to FXCASHBACKS</h3>
        <hr class="bottom-width">
        <p class="sub-text">Learn more about us from our video which explains our service in details.<br> It's a short one, we promise. </p>
      </div>
      <div class="hide" data-step="2" data-title="This is the second step!">
        <h3 class="heading-model text-center" >Welcome to FXCASHBACKS</h3>
        <hr class="bottom-width">
        <p class="sub-text"> Click here to add your Unique Broker ID to FxCashbacks </p>
        <a href="{{route('mainpage')}}" target="_blank"> <button type="button" class="btn btn-success">Add Unique ID</button></a>
      </div>
    </div>
 <input type="hidden" class="final">
    <div class="modal-footer">
      <button type="button" class="btn btn-default js-btn-step pull-left" data-orientation="cancel"
        data-dismiss="modal"></button>

      <button type="button" class="btn btn-warning js-btn-step" data-orientation="previous"></button>
      <button type="button"  class="status btn btn-success js-btn-step" data-final=""  data-orientation="next"></button>
    </div>
  </div>
</div>
</div>
<!-- The Modal -->

<script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{url('')}}/assets/axios/axios.min.js"></script>

<script>
    $('.status').click(function () {
      
        var val = $(this).data('step');
        var val2 = $('.final').val();
        if(val2 === null || val2 === ""){
          $('.final').val('done');
          console.log("adfasdf");
        }
        if(val2 == "done"){
          axios
              .post('{{route("popup_update_status")}}', {
                  _token: '{{csrf_token()}}',
                  _method: 'post',
              })
              .then(function (responsive) {
                  console.log(responsive);
              })
              .catch(function (error) {
                  console.log(error);
              });
        }        
    });
</script>
<script src="{{url('')}}/js/modal-steps.min.js"></script>
<script>
  $('#myModal').modalSteps();
  $('#myModal').modal('show');
</script>