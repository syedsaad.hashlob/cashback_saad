<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/modern_business.css') }}" rel="stylesheet">
    <link href="{{ asset('css/media_query.css') }}" rel="stylesheet">
    <link href="{{ url('')}}/assets/img/favicon.png" rel="icon">
    <link href="{{ url('')}}/assets/img/apple-touch-icon.png" rel="apple-touch-icon">
    
    
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
<!-- Favicon -->
<link href="{{ url('')}}/assets/img/favicon.png" rel="icon">
<link href="{{ url('')}}/assets/img/apple-touch-icon.png" rel="apple-touch-icon">
<link rel="stylesheet" href="{{url('')}}/homepage/css/style.css">
   <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-249312996-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'UA-249312996-1');
    </script>

</head>
<body>

    <!-- Navigation -->
    <header>
        <nav class="navbar custom-navbar navbar-expand-lg navbar-dark fixed-top" data-spy="affix" data-offset-top="10">
            <div class="container-fluid">
                <a class="navbar-brand" href="#">
                    <img src="{{url('')}}/homepage/imgs/logo.png" alt="">
                </a>
                <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active {{ (request()->routeIs('Homepage')) ? 'active' : '' }}">
                            <a class="nav-link {{ (request()->routeIs('Homepage')) ? 'active' : '' }}"
                                href="{{ route('Homepage') }}">HOME</a>
                        </li>
    
                        <li class="nav-item {{ (request()->routeIs('how-its-work')) ? 'active' : '' }}">
                            <a class="nav-link {{ (request()->routeIs('how-its-work')) ? 'active' : '' }}"
                                href="{{ route('how-its-work') }}">HOW IT WORKS</a>
                        </li>
    
    
                        <li class="nav-item {{ (request()->routeIs('features')) ? 'active' : '' }}">
                            <a class="nav-link {{ (request()->routeIs('features')) ? 'active' : '' }}"
                                href="{{ route('features') }}">FEATURES</a>
                        </li>
    
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                BROKERS
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
                                @foreach ($all_brokers as $all_broker)
                                <a class="dropdown-item"
                                    href="{{route('web_broker',$all_broker->id)}}">{{$all_broker->name}}</a>
    
                                @endforeach
                            </div>
                        </li>
    
                        <li class="nav-item {{ (request()->routeIs('support')) ? 'active' : '' }}">
                            <a class="nav-link {{ (request()->routeIs('support')) ? 'active' : '' }}"
                                href="{{ route('support') }}">SUPPORT</a>
                        </li>
    
    
                        <li class="nav-item {{ (request()->routeIs('contact')) ? 'active' : '' }}">
                            <a class="nav-link {{ (request()->routeIs('contact')) ? 'active' : '' }}"
                                href="{{ route('contact') }}">CONTACT</a>
                        </li>
                        <!-- Button trigger modal -->
                    </ul>
                </div>
            </div>
        </nav>
    </header>

    {{-- <section class="dashborad_page_top_menu">
        <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
            <div class="container">
                <a class="navbar-brand" href="{{url('')}}">
                    <img class='logo_f' src="{{url('')}}/images/logo.png" >
                </a>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    @if (Auth::check())

                        <ul class="navbar-nav ml-auto">

                            <li class="nav-item">
                                <a class="nav-link" href="about.html">>FOREX</a>
                            </li>
                        </ul>
                    @endif
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="services.html"><i class="fas fa-bell"></i></a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            EN
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">

                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                REFERAL
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
                            </div>
                        </li>
                        <li class="nav-item">
                            <p class="dynamic_per_perpage">80%</p>
                            </a>
                        </li>
                        @auth
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ Auth::user()->name }}
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
                                <a class="dropdown-item" href="pricing.html" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    Logout
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </li>
                        @endauth

                    </ul>

                </div>
            </div>
        </nav>
    </section> --}}
    <section id="main_page" style="margin-top: 36px"></section>



        <nav class="navbar navbar-expand-lg navbar-light bg-light">


            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav w-100 justify-content-center px-3">
                    <li class="nav-item {{ (request()->routeIs('mainpage')) ? 'active' : '' }}">
                        <a class="nav-link btn {{ (request()->routeIs('mainpage')) ? 'active' : '' }}" href="{{ route('mainpage') }}">Homepage</a>
                    </li>
                    <li class="nav-item {{ (request()->routeIs('mainpage')) ? 'active' : '' }}">
                        <a class="nav-link btn {{ (request()->routeIs('mainpage')) ? 'active' : '' }}" href="{{ route('mainpage') }}">Homepage</a>
                    </li>  <li class="nav-item {{ (request()->routeIs('mainpage')) ? 'active' : '' }}">
                        <a class="nav-link btn {{ (request()->routeIs('mainpage')) ? 'active' : '' }}" href="{{ route('mainpage') }}">Homepage</a>
                    </li>  <li class="nav-item {{ (request()->routeIs('mainpage')) ? 'active' : '' }}">
                        <a class="nav-link btn {{ (request()->routeIs('mainpage')) ? 'active' : '' }}" href="{{ route('mainpage') }}">Homepage</a>
                    </li>  <li class="nav-item {{ (request()->routeIs('mainpage')) ? 'active' : '' }}">
                        <a class="nav-link btn {{ (request()->routeIs('mainpage')) ? 'active' : '' }}" href="{{ route('mainpage') }}">Homepage</a>
                    </li>

                </ul>

            </div>
          </nav>









    </section>

    @yield('content')


    <footer class="section footer-classic context-dark bg-image bg_footer_gra">
        <div class="container">
            <div class="row">

                <div class="col-md-7">
                    <h5 class="hed_foter"><a class="{{ (request()->routeIs('terms')) ? 'active' : '' }}" href="{{ route('terms') }}"> Terms of Service </a></h5>
                    <h5 class="hed_foter"><a class="{{ (request()->routeIs('privacy')) ? 'active' : '' }}" href="{{ route('privacy') }}"> Privacy Policy </a></h5>
                    <h5 class="hed_foter"><a class="{{ (request()->routeIs('support')) ? 'active' : '' }}" href="{{ route('support') }}"> FAQ </a></h5>
                    <h5 class="hed_foter"> <a class=" {{ (request()->routeIs('contact')) ? 'active' : '' }}"
                        href="{{ route('contact') }}"> Contact Us </a></h5>
                    {{-- <p class="para_foter">Reporting of Rebates High Rebate Rates<br> Highly Efficient custome</p> --}}
                </div>

                <div class="col-md-5">
                    <h5 class="hed_foter">Join Our Newsletter</h5>
                    <div class="input-group">
                        <input type="search" class="form-control rounded" placeholder="Search" aria-label="Search"
                            aria-describedby="search-addon" />
                        <button type="button" class="btn btn-outline-primary">Submit</button>
                    </div>
                </div>


                {{-- <div class="container">
                    <div class="row pd">
                        <div class="seprater"></div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">

                        <div class="col-md-6">
                            <a {{ (request()->routeIs('terms')) ? 'active' : '' }}" href="{{ route('terms') }}">
                                <p class="text-center">Terms of Service</p>

                        </div>



                        <div class="col-md-6">
                            <a {{ (request()->routeIs('privacy')) ? 'active' : '' }}" href="{{ route('privacy') }}">
                                <p class="text-center">Privacy Policy</p>


                        </div>
                    </div>
                </div> --}}

            </div>
        </div>

    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
</body>

</html>
