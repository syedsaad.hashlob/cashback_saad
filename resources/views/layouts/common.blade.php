<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>FX CASHBACKS</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">


    <!-- Bootstrap core CSS -->
    <link href="{{url('')}}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="{{url('')}}/css/modern_business.css" rel="stylesheet">
    <link href="{{url('')}}/css/media_query.css" rel="stylesheet">
    <link href="{{url('')}}/css/forex.css" rel="stylesheet">
    <link href="{{ url('')}}/assets/img/favicon.png" rel="icon">
    <link href="{{ url('')}}/assets/img/apple-touch-icon.png" rel="apple-touch-icon">
    
    
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-249312996-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'UA-249312996-1');
    </script>



</head>


<body>
@php
    $all_brokers = getBrokers();
@endphp
    <!-- Navigation -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                <img class='logo_f' src="{{url('')}}/images/logo.png" >
            </a>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link {{ (request()->routeIs('Homepage')) ? 'active' : '' }}" href="{{ route('Homepage') }}">HOME</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link {{ (request()->routeIs('how-its-work')) ? 'active' : '' }}" href="{{ route('how-its-work') }}">HOW IT WORKS</a>
                    </li>


                    <li class="nav-item">
                        <a class="nav-link {{ (request()->routeIs('features')) ? 'active' : '' }}" href="{{ route('features') }}">FEATURES</a>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            BROKERS
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
                            @foreach ($all_brokers as $all_broker)
                                <a class="dropdown-item" href="{{route('web_broker',$all_broker->id)}}">{{$all_broker->name}}</a>
                            
                            @endforeach
                        </div>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link {{ (request()->routeIs('support')) ? 'active' : '' }}" href="{{ route('support') }}">SUPPORT</a>
                    </li>


                    <li class="nav-item">
                        <a class="nav-link {{ (request()->routeIs('contact')) ? 'active' : '' }}" href="{{ route('contact') }}">CONTACT</a>
                    </li>
                    @if (Auth::user())
                        
                    <li class="nav-item">
                        <a href="{{ route('home') }}"><button style="margin: 0px" type="button" class="btn btn-primary btn_sign_modal_nav dashboard_btn" >
                            DASHBOARD
                        </button></a>
                    </li>
                    @endif



     <!-- Button trigger modal -->


                </ul>
            
            </div>
        </div>
    </nav>
      <!-- Login Modal -->
      <section id="login_modal">


        <div class="modal fade" id="loginexampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                <div class="panel panel-primary">
                                    <div class="panel-body">
                                        <form method="POST" action="{{ route('login') }}" role="form">
                                            @csrf
                                            <div class="form-group">
                                                <h2>SIGN IN</h2>
                                            </div>
                                                <div class="form-group">
                                                    <input id="email" placeholder="Email Address or username" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                                    @error('email')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <input id="password" placeholder="Enter Your Password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                                    @error('password')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-6 ">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                                            <label class="form-check-label" for="remember">
                                                                {{ __('Remember Me') }}
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-6 offset-md-3">
                                                    <button id="signupSubmit" type="submit"
                                                        class="btn btn-info btn-block">{{ __('Login') }}</button>
                                                        @if (session('error'))
                                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                                            {{ session('error') }}
                                                        </a>
                                                        @endif
                                                    @if (Route::has('password.request'))
                                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                                            {{ __('Forgot Your Password?') }}
                                                        </a>
                                                    @endif
                                                    </div>
                                                </div>
                                                <p class="text-Center">Use Social Network to Sign In</p>
                                                <div class="row">

                                                    <div class="col-md-6">
                                                    <div class="form-group">
                                                        <button  type="submit"
                                                            class="facebook-btn btn btn-info btn-block"><i class="fab fa-facebook-f"></i> FaceBook Login</button>
                                                    </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <button  type="submit"
                                                            class="gmail-btn btn btn-info btn-block"><img src="{{url('')}}/images/gmail.svg"> Gmail</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            <hr>
                                            <p></p>Already have an account?
                                                @if (Route::has('register'))
                                                <a href="{{ route('register') }}"
                                                    class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">Register</a>
                                                @endif
                                            </p>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        </div>
    </section>
    <!-- Login Modal end -->
    <section id="sign-up">

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <div class="row">
                                <div class="panel panel-primary">
                                    <div class="panel-body">
                                        <form method="POST" action="#" role="form">
                                            <div class="form-group">
                                                <h2>SIGN UP</h2>
                                            </div>
                                            <div class="form-group">
                                                <input id="signupName" type="text" maxlength="50" class="form-control"
                                                    placeholder="Your name">
                                            </div>
                                            <div class="form-group">
                                                <input id="signupEmail" type="email" maxlength="50" class="form-control"
                                                    placeholder="Email">
                                            </div>

                                            <div class="form-group">
                                                <input id="signupPassword" type="password" maxlength="25"
                                                    class="form-control" placeholder="at least 6 characters"
                                                    length="40">
                                            </div>

                                            <div class="form-group">
                                                <button id="signupSubmit" type="submit"
                                                    class="btn btn-info btn-block">Create your account</button>
                                            </div>
                                            <p class="form-group">By creating an account, you agree to our <a
                                                    href="#">Terms of Use</a> and our <a href="#">Privacy Policy</a>.
                                            </p>
                                            <hr>
                                            <p></p>Already have an account? <a href="#">Sign in</a></p>



                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-primary btn_sign_modal_nav" data-toggle="modal"
                                data-target="#exampleModal">
                                Sign in
                            </button></li>
                        <a href="{{ route('login') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Login</a>


                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </section>
    <footer class="section footer-classic context-dark bg-image bg_footer_gra">
        <div class="container">
            <div class="row">

                <div class="col-md-7">
                    <h5 class="hed_foter"><a class="{{ (request()->routeIs('terms')) ? 'active' : '' }}" href="{{ route('terms') }}"> Terms of Service </a></h5>
                    <h5 class="hed_foter"><a class="{{ (request()->routeIs('privacy')) ? 'active' : '' }}" href="{{ route('privacy') }}"> Privacy Policy </a></h5>
                    <h5 class="hed_foter"><a class="{{ (request()->routeIs('support')) ? 'active' : '' }}" href="{{ route('support') }}"> FAQ </a></h5>
                    <h5 class="hed_foter"> <a class=" {{ (request()->routeIs('contact')) ? 'active' : '' }}"
                        href="{{ route('contact') }}"> Contact Us </a></h5>
                    {{-- <p class="para_foter">Reporting of Rebates High Rebate Rates<br> Highly Efficient custome</p> --}}
                </div>

                <div class="col-md-5">
                    <h5 class="hed_foter">Join Our Newsletter</h5>
                    <div class="input-group">
                        <input type="search" class="form-control rounded" placeholder="Search" aria-label="Search"
                            aria-describedby="search-addon" />
                        <button type="button" class="btn btn-outline-primary">Submit</button>
                    </div>
                </div>


                {{-- <div class="container">
                    <div class="row pd">
                        <div class="seprater"></div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">

                        <div class="col-md-6">
                            <a {{ (request()->routeIs('terms')) ? 'active' : '' }}" href="{{ route('terms') }}">
                                <p class="text-center">Terms of Service</p>

                        </div>



                        <div class="col-md-6">
                            <a {{ (request()->routeIs('privacy')) ? 'active' : '' }}" href="{{ route('privacy') }}">
                                <p class="text-center">Privacy Policy</p>


                        </div>
                    </div>
                </div> --}}

            </div>
        </div>

    </footer>
    <!-- Bootstrap core JavaScript -->
    <script src="{{url('')}}/vendor/jquery/jquery.min.js"></script>
    <script src="{{url('')}}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script>
    @if (count($errors) > 0)
    $('#loginexampleModal').modal('show');
    @endif
</script>
</body>

</html>
