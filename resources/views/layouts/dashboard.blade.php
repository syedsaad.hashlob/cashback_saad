<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.7/flatly/bootstrap.min.css"> --}}

    <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/modern_business.css') }}" rel="stylesheet">
    <link href="{{ asset('css/media_query.css') }}" rel="stylesheet">
    <link href="{{ url('')}}/assets/img/favicon.png" rel="icon">
    <link href="{{ url('')}}/assets/img/apple-touch-icon.png" rel="apple-touch-icon">
    
    <title>FxCashbacks</title>

    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
  <!-- Favicon -->
  <link href="{{ url('')}}/assets/img/favicon.png" rel="icon">
  <link href="{{ url('')}}/assets/img/apple-touch-icon.png" rel="apple-touch-icon">
    <style>
    a.nav-link.active {
        color: #000000cc !important;
    }
  </style>
  </head>
<body>

    <!-- Navigation -->
    <section class="dashborad_page_top_menu">
        <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
            <div class="container">
                <a class="navbar-brand" href="{{url('')}}">
                    <img class='logo_f' src="{{url('')}}/images/logo.png" >
                </a>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item {{ (request()->routeIs('mainpage')) ? '' : '' }}">
                            <a class="text-center nav-link {{ (request()->routeIs('mainpage')) ? '' : '' }}"
                                href="{{ route('mainpage') }}">FOREX</a>
                                  {{-- <p style="color: white;font-size:11px;font-weight: bold;" class="text-center">()</p> --}}
                            </li>
                            {{-- <li class="nav-item {{ (request()->routeIs('binary')) ? 'active' : '' }}">
                                <a class="nav-link {{ (request()->routeIs('binary')) ? 'active' : '' }}"
                                    href="{{ route('binary') }}">>BINARY</a>
                                </li> --}}
                    </ul>
                    <ul class="navbar-nav ml-auto">
                        {{-- <li class="nav-item">
                            <a class="nav-link" href="#"><i class="fas fa-bell"></i></a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            EN
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">

                            </div>
                        </li>
                        --}}
                     
                        @auth
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="{{route('mainpage')}}" id="navbarDropdownBlog" >
                                Brokers
                            </a>
                        </li> 
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ Auth::user()->name }}
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
                                <a class="dropdown-item" href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    Logout
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    Logout
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        
                        </li> 
                        @endauth

                    </ul>

                </div>
            </div>
        </nav>
    </section>
    <section id="nav_dashboard" style="margin-top: 67px;"></section>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav w-100 justify-content-center px-3">
                    <li class="nav-item {{ (request()->routeIs('home')) ? 'active' : '' }}">
                        <a class="nav-link btn {{ (request()->routeIs('home')) ? 'active' : '' }}" href="{{ route('home') }}">DASHBOARD</a>
                    </li>
                    <li class="nav-item {{ (request()->routeIs('my_account')) ? 'active' : '' }}">
                        <a class="nav-link {{ (request()->routeIs('my_account')) ? 'active' : '' }}" href="{{ route('my_account') }}">MY ACCOUNTS</a></a>
                    </li>
                    <li class="nav-item {{ (request()->routeIs('refered_user')) ? 'active' : '' }}">
                        <a class="nav-link {{ (request()->routeIs('refered_user')) ? 'active' : '' }}" href="{{ route('refered_user') }}">REFERRED USERS</a>
                    </li>
                    <li class="nav-item {{ (request()->routeIs('withdrawl')) ? 'active' : '' }}">
                        <a class="nav-link {{ (request()->routeIs('withdrawl')) ? 'active' : '' }}" href="{{ route('withdrawl') }}">WITHDRAWL</a>
                    </li>
                    <li class="nav-item {{ (request()->routeIs('leaderboard')) ? 'active' : '' }}">
                        <a class="nav-link {{ (request()->routeIs('leaderboard')) ? 'active' : '' }}" href="{{ route('leaderboard') }}">LEADERBOARD</a>
                    </li>

                </ul>
            </div>
        </nav>
    </section>

    @yield('content');


    
    <footer class="section footer-classic context-dark bg-image bg_footer_gra">
        <div class="container">
            <div class="row">

                <div class="col-md-7">
                    <h5 class="hed_foter"><a class="{{ (request()->routeIs('terms')) ? 'active' : '' }}" href="{{ route('terms') }}"> Terms of Service </a></h5>
                    <h5 class="hed_foter"><a class="{{ (request()->routeIs('privacy')) ? 'active' : '' }}" href="{{ route('privacy') }}"> Privacy Policy </a></h5>
                    <h5 class="hed_foter"><a class="{{ (request()->routeIs('support')) ? 'active' : '' }}" href="{{ route('support') }}"> FAQ </a></h5>
                    <h5 class="hed_foter"> <a class=" {{ (request()->routeIs('contact')) ? 'active' : '' }}"
                        href="{{ route('contact') }}"> Contact Us </a></h5>
                    {{-- <p class="para_foter">Reporting of Rebates High Rebate Rates<br> Highly Efficient custome</p> --}}
                </div>

                <div class="col-md-5">
                    <h5 class="hed_foter">Join Our Newsletter</h5>
                    <div class="input-group">
                        <input type="search" class="form-control rounded" placeholder="Search" aria-label="Search"
                            aria-describedby="search-addon" />
                        <button type="button" class="btn btn-outline-primary">Submit</button>
                    </div>
                </div>


                {{-- <div class="container">
                    <div class="row pd">
                        <div class="seprater"></div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">

                        <div class="col-md-6">
                            <a {{ (request()->routeIs('terms')) ? 'active' : '' }}" href="{{ route('terms') }}">
                                <p class="text-center">Terms of Service</p>

                        </div>



                        <div class="col-md-6">
                            <a {{ (request()->routeIs('privacy')) ? 'active' : '' }}" href="{{ route('privacy') }}">
                                <p class="text-center">Privacy Policy</p>


                        </div>
                    </div>
                </div> --}}

            </div>
        </div>

    </footer>


    <!-- Bootstrap core JavaScript -->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            // $(".chartContainer").CanvasJSChart({
            //     title: {
            //         // text: "Monthly Rainfall in Columbus - 1996"
            //     },
            //     axisY: {
            //         // title: "Rainfall in mm",
            //         includeZero: false
            //     },
            //     axisX: {
            //         interval: 1
            //     },
            //     data: [
            //     {
            //         type: "line", //try changing to column, area
            //         toolTipContent: "{label}: {y} mm",
            //         dataPoints: [
            //             { label: "Jan",  y: 5.28 },
            //             { label: "Feb",  y: 3.83 },
            //             { label: "March",y: 6.55 },
            //             { label: "April",y: 4.81 },
            //             { label: "May",  y: 2.37 },
            //             { label: "June", y: 2.33 },
            //             { label: "July", y: 3.06 },
            //             { label: "Aug",  y: 2.94 },
            //             { label: "Sep",  y: 5.41 },
            //             { label: "Oct",  y: 2.17 },
            //             { label: "Nov",  y: 2.17 },
            //             { label: "Dec",  y: 2.80 }
            //         ]
            //     }
            //     ]
            // });
        });
        </script>
        <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
        {{-- <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script> --}}
</body>

</html>
