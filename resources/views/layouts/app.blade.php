<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/modern_business.css') }}" rel="stylesheet">

    <link href="{{ asset('css/media_query.css') }}" rel="stylesheet">
    <!-- Favicon -->
    <link href="{{ url('')}}/assets/img/favicon.png" rel="icon">
    <link href="{{ url('')}}/assets/img/apple-touch-icon.png" rel="apple-touch-icon">



    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />

</head>

<body>

    <!-- Navigation -->
    <section id="dashborad_page_top_menu">
        <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
            <div class="container">
                <a class="navbar-brand" href="{{url('')}}">
                    <img class='logo_f' src="{{url('')}}/images/logo.png">
                </a>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                    data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    @if (Auth::check())
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item {{ (request()->routeIs('mainpage')) ? 'active' : '' }}">
                            <a class="nav-link {{ (request()->routeIs('mainpage')) ? 'active' : '' }}"
                                href="{{ route('mainpage') }}">>FOREX </a>
                        </li>
                        <li class="nav-item {{ (request()->routeIs('binary')) ? 'active' : '' }}">
                            <a class="nav-link {{ (request()->routeIs('binary')) ? 'active' : '' }}"
                                href="{{ route('binary') }}">>BINARY</a>
                        </li>
                    </ul>
                    @endif

                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="#"><i class="fas fa-bell"></i></a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                EN
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">

                                <a class="dropdown-item" href="#">Pricing Table</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                REFERAL
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
                                <a class="dropdown-item" href="#">Pricing Table</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <p class="dynamic_per_perpage">
                                @if(Auth::check())
                                @if (currentPoints(Auth::user()->id) < getScoringPoints('bronze')['to'])
                                    {{currentPoints(Auth::user()->id) * 100 / getScoringPoints('bronze')['to']}}%
                                    @elseif(currentPoints(Auth::user()->id) > getScoringPoints('bronze')['to'] &&
                                    currentPoints(Auth::user()->id) < getScoringPoints('silver')['to'])
                                        {{currentPoints(Auth::user()->id) * 100 / getScoringPoints('silver')['to']}}%
                                        @elseif(currentPoints(Auth::user()->id) > getScoringPoints('silver')['to'] &&
                                        currentPoints(Auth::user()->id) < getScoringPoints('gold')['to'])
                                            {{currentPoints(Auth::user()->id) * 100 / getScoringPoints('gold')['to']}}%
                                            @elseif(currentPoints(Auth::user()->id) > getScoringPoints('gold')['to'] &&
                                            currentPoints(Auth::user()->id) < getScoringPoints('platinum')['to'])
                                                {{currentPoints(Auth::user()->id) * 100 / getScoringPoints('platinum')['to']}}%
                                                @elseif(currentPoints(Auth::user()->id) >
                                                getScoringPoints('platinum')['to'] && currentPoints(Auth::user()->id) <
                                                    getScoringPoints('diamond')['to'])
                                                    {{currentPoints(Auth::user()->id) * 100 / getScoringPoints('diamond')['to']}}%
                                                    @else
                                                    {{currentPoints(Auth::user()->id) * 100 / getScoringPoints('emerald')['to']}}%
                                                    @endif @endif {{-- @if(Auth::check())
                                    @if (currentPoints(Auth::user()->id) < 499)
                                        {{currentPoints(Auth::user()->id) * 100 / 500}}%
                                                    @elseif(currentPoints(Auth::user()->id) > 499 &&
                                                    currentPoints(Auth::user()->id) < 2499)
                                                        {{currentPoints(Auth::user()->id) * 100 / 2500}}%
                                                        @elseif(currentPoints(Auth::user()->id) > 2499 &&
                                                        currentPoints(Auth::user()->id) < 9999)
                                                            {{currentPoints(Auth::user()->id) * 100 / 10000}}%
                                                            @elseif(currentPoints(Auth::user()->id) > 9999 &&
                                                            currentPoints(Auth::user()->id) < 24999)
                                                                {{currentPoints(Auth::user()->id) * 100 / 25000}}%
                                                                @elseif(currentPoints(Auth::user()->id) > 24999 &&
                                                                currentPoints(Auth::user()->id) < 99999)
                                                                    {{currentPoints(Auth::user()->id) * 100 / 100000}}%
                                                                    @else 100% @endif @endif --}} </p> </a> </li> @auth
                                                                    <li class="nav-item">
                                                                    <p class="dynamic_per_perpage">
                                                                        {{ Auth::user() ? Auth::user()->name : '' }}</p>
                                                                    <p class="dynamic_per_perpage">
                                                                        {{ Auth::user() ? Auth::user()->user_account_id : ''}}
                                                                    </p>

                                                                    </a>
                        </li>
                        @endauth

                    </ul>

                </div>
            </div>
        </nav>
    </section>

    @auth
    <section id="nav_dashboard" style="margin-top: 67px;"></section>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav w-100 justify-content-center px-3">
                <li class="nav-item {{ (request()->routeIs('home')) ? 'active' : '' }}">
                    <a class="nav-link btn {{ (request()->routeIs('home')) ? 'active' : '' }}"
                        href="{{ route('home') }}">DASHBOARD</a>
                </li>
                <li class="nav-item {{ (request()->routeIs('my_account')) ? 'active' : '' }}">
                    <a class="nav-link {{ (request()->routeIs('my_account')) ? 'active' : '' }}"
                        href="{{ route('my_account') }}">MY ACCOUNTS</a></a>
                </li>
                <li class="nav-item {{ (request()->routeIs('refered_user')) ? 'active' : '' }}">
                    <a class="nav-link {{ (request()->routeIs('refered_user')) ? 'active' : '' }}"
                        href="{{ route('refered_user') }}">REFERRED USERS</a>
                </li>
                <li class="nav-item {{ (request()->routeIs('withdrawl')) ? 'active' : '' }}">
                    <a class="nav-link {{ (request()->routeIs('withdrawl')) ? 'active' : '' }}"
                        href="{{ route('withdrawl') }}">WITHDRAWL</a>
                </li>
                <li class="nav-item {{ (request()->routeIs('leaderboard')) ? 'active' : '' }}">
                    <a class="nav-link {{ (request()->routeIs('leaderboard')) ? 'active' : '' }}"
                        href="{{ route('leaderboard') }}">LEADERBOARD</a>
                </li>

            </ul>
        </div>
    </nav>
    </section>
    @else
    <section style="height: 100px"></section>
    @endauth

    @yield('content');


    <footer class="section footer-classic context-dark bg-image bg_footer_gra">
        <div class="container">
            <div class="row">

                <div class="col-md-7">
                    <h5 class="hed_foter"><a class="{{ (request()->routeIs('terms')) ? 'active' : '' }}" href="{{ route('terms') }}"> Terms of Service </a></h5>
                    <h5 class="hed_foter"><a class="{{ (request()->routeIs('privacy')) ? 'active' : '' }}" href="{{ route('privacy') }}"> Privacy Policy </a></h5>
                    <h5 class="hed_foter"><a class="{{ (request()->routeIs('support')) ? 'active' : '' }}" href="{{ route('support') }}"> FAQ </a></h5>
                    <h5 class="hed_foter"> <a class=" {{ (request()->routeIs('contact')) ? 'active' : '' }}"
                        href="{{ route('contact') }}"> Contact Us </a></h5>
                    {{-- <p class="para_foter">Reporting of Rebates High Rebate Rates<br> Highly Efficient custome</p> --}}
                </div>

                <div class="col-md-5">
                    <h5 class="hed_foter">Join Our Newsletter</h5>
                    <div class="input-group">
                        <input type="search" class="form-control rounded" placeholder="Search" aria-label="Search"
                            aria-describedby="search-addon" />
                        <button type="button" class="btn btn-outline-primary">Submit</button>
                    </div>
                </div>


                {{-- <div class="container">
                    <div class="row pd">
                        <div class="seprater"></div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">

                        <div class="col-md-6">
                            <a {{ (request()->routeIs('terms')) ? 'active' : '' }}" href="{{ route('terms') }}">
                                <p class="text-center">Terms of Service</p>

                        </div>



                        <div class="col-md-6">
                            <a {{ (request()->routeIs('privacy')) ? 'active' : '' }}" href="{{ route('privacy') }}">
                                <p class="text-center">Privacy Policy</p>


                        </div>
                    </div>
                </div> --}}

            </div>
        </div>

    </footer>


    <!-- Bootstrap core JavaScript -->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
</body>

</html>
