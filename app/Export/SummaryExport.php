<?php

namespace App\Export;

use App\ActivityLog;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithMapping;
use App\User;
use App\Trader_trades;
use DB;

class SummaryExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $count = 1;

    public function collection()
    {
        $user = User::withCount(['tradings as actual_comm' => function($query) {
            $query->select(DB::raw('sum(Actual_Comm)'));
        }])->withCount(['tradings as total_comm' => function($query) {
            $query->select(DB::raw('sum(Total_Comm)'));
        }])->get();
        return $user;
    }

    public function map($user): array
    {
        $data = [];
      
        return [
            $user->id,
            $user->current_points,
            $user->cashback_rate,
            $user->actual_comm,
            $user->total_comm,
            // $data,
            // $activity->p_status,
            // $activity->s_status,
            // $activity->total
        ];
    }

    public function headings(): array
    {
        return
        [
            ['Total User Summary'],
            [],
            ['S.NO',
            'CURRENT POINTS',
            'CASHBACK RATE',
            'AMOUNT',
            'EARN AMOUNT',
            ]
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:I1'; // All headers
                $cellRange1 = 'A3:I3'; // All headers
                $last_row = $this->count + 4;
                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange1)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->mergeCells($cellRange);
                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getFont()->setSize(11);
                for ($i=3; $i < $last_row ; $i++) {
                    $event->sheet->getStyle('A'.$i.':I'.$i)->applyFromArray([
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['argb' => '000000'],
                            ],
                        ],
                    ]);
                }
            },
        ];
    }
}
