<?php

namespace App\Export;

use App\ActivityLog;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithMapping;
use App\User;
use App\Trader_trades;

class ActivityExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $count = 1;
    public $user_id;

    public function __construct($user_id)
    {
        $this->user_id = $user_id;
    }

    public function collection()
    {
        // $activity = User::with(['tradings' => function($query) {
        //     $avg = $query->select('user_id','MT4_MT5_ID','date','Total_Comm')->get();
        // },'withdraw' => function($query) {
        //     $avg = $query->select('user_id','id','created_at','total_amount')->get();
        // }
        // ])
        // ->first();
        $activity = Trader_trades::with('broker')->where('user_id', $this->user_id)->get();
        // $this->count = count($activity);
        return $activity;
    }

    public function map($activity): array
    {
        $data = [];
        // if(isset($activity->tradings) && count($activity->tradings) > 0){
        //     foreach ($activity->tradings as $key => $trading) {
        //         $data['MT4_MT5_ID'][$key] = $trading->MT4_MT5_ID;
        //         $data['Total_Comm'][$key] = $trading->Total_Comm ?? $activity->withdraw->total_amount;
        //         $data['date'][$key] = $trading->date ?? $activity->withdraw->created_at;
        //         $data['type'][$key] = "Trading";
        //     }
        // }
        // $count = $key;
        // if(isset($activity->withdraw) && count($activity->withdraw) > 0){
        //     foreach ($activity->withdraw as $key2 => $withdraw) {
        //         $data['total_amount'][$count] = $withdraw->total_amount;
        //         $data['created_at'][$count] = $withdraw->created_at;
        //         $data['type'][$count] = "Withdraw";
        //         $count++;
        //     }
        // }

        // dd($data);
        return [
            $activity->id,
            $activity->MT4_MT5_ID,
            $activity->Total_Comm,
            $activity->date,
            'Transaction',
            // $data,
            // $activity->p_status,
            // $activity->s_status,
            // $activity->total
        ];
    }

    public function headings(): array
    {
        return
        [
            ['User Activity'],
            [],
            ['S.NO',
            'ACCOUNT ID',
            'TOTAL COMM / TRANSACTION AMOUNT',
            'DATE',
            'TYPE',
            ]
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:I1'; // All headers
                $cellRange1 = 'A3:I3'; // All headers
                $last_row = $this->count + 4;
                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange1)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->mergeCells($cellRange);
                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getFont()->setSize(11);
                for ($i=3; $i < $last_row ; $i++) {
                    $event->sheet->getStyle('A'.$i.':I'.$i)->applyFromArray([
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['argb' => '000000'],
                            ],
                        ],
                    ]);
                }
            },
        ];
    }
}
