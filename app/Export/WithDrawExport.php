<?php

namespace App\Export;

use App\ActivityLog;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithMapping;
use App\User;
use App\Transaction;

class WithDrawExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $count = 1;
    public $user_id;

    public function __construct($user_id)
    {
        $this->user_id = $user_id;
    }

    public function collection()
    {
        // $activity = User::with(['tradings' => function($query) {
        //     $avg = $query->select('user_id','MT4_MT5_ID','date','Total_Comm')->get();
        // },'withdraw' => function($query) {
        //     $avg = $query->select('user_id','id','created_at','total_amount')->get();
        // }
        // ])
        // ->first();
        $withdraw = Transaction::with('paymentMethod')->where('user_id',$this->user_id)->get();
        // $this->count = count($withdraw);
        return $withdraw;
    }

    public function map($withdraw): array
    {
        $data = [];
     
        // dd($data);
        return [
            $withdraw->id,
            $withdraw->paymentMethod->name,
            $withdraw->amount,
            $withdraw->transaction_fee,
            $withdraw->total_amount,
            $withdraw->created_at,
            // $data,
            // $activity->p_status,
            // $activity->s_status,
            // $activity->total
        ];
    }

    public function headings(): array
    {
        return
        [
            ['User WithDraw History'],
            [],
            ['S.NO',
            'PAYMENT METHOD',
            'AMOUNT',
            'TRANSACTION FEE',
            'TOTAL AMOUNT',
            'DATE',
            ]
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:I1'; // All headers
                $cellRange1 = 'A3:I3'; // All headers
                $last_row = $this->count + 4;
                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange1)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->mergeCells($cellRange);
                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getFont()->setSize(11);
                for ($i=3; $i < $last_row ; $i++) {
                    $event->sheet->getStyle('A'.$i.':I'.$i)->applyFromArray([
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['argb' => '000000'],
                            ],
                        ],
                    ]);
                }
            },
        ];
    }
}
