<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Referal extends Model
{
    protected $table = 'referral';
    protected $fillable = ['referral','user_account_id','referral_account_id'];

    /**
     * Get the user associated with the Referal
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function userForReferal()
    {
        return $this->hasOne(User::class, 'referral', 'referral');
    }

    /**
     * Get the user associated with the Referal
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function referralUser()
    {
        return $this->hasOne(User::class, 'id', 'user_account_id');
    }
  
    
}
