<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\PaymentMethod;
use Illuminate\Support\Facades\Storage;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $payment = PaymentMethod::get();
        activityLog('View Payment Method');
        return view('admin.payment.index', compact('payment'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.payment.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required',
            'image' => 'required',
        ]);
        if ($request->image) {
            
            $data['image']  = Storage::disk('uploads')->putFile('', $request->image);
        }

        PaymentMethod::create($data);
        activityLog('Add Payment Method');
        return redirect()->route('payment.index')
                        ->with('success','Payment created successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $payment = PaymentMethod::find($id);
        return view('admin.payment.edit', compact('payment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PaymentMethod $payment)
    {
        $data = $request->validate([
            'name' => 'required',
        ]);
        if ($request->image) {
            Storage::disk('uploads')->delete($payment->image);
            $data['image']  = Storage::disk('uploads')->putFile('',$request->image);
        }
        // return $data;
        $payment->update($data);
        activityLog('Update Payment Method');
        return redirect()->route('payment.index')
                        ->with('success','Payment created successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $payment = PaymentMethod::find($id);
        Storage::disk('uploads')->delete($payment->image);
        $payment->delete();
        activityLog('Delete Payment Method');
        return redirect()->route('payment.index')
                        ->with('success','payment deleted successfully');
    }
}
