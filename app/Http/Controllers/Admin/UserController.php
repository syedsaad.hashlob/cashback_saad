<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Referal;
use App\Broker;
use App\ActivityLog;
use App\UserBrokerAccount;
use App\Trader_trades;
use App\Transaction;
use Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Export\ActivityExport;
use App\Export\WithDrawExport;
use App\Export\SummaryExport;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $data = [
            'users' =>  User::get(),
        ];

        return view('admin.users.index',$data);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
    */
    public function actionHistory(User $user)
    {
        $data = [
            'action_historys' =>  $user,
            'brokers' =>  Broker::where('status',1)->get(),
        ];
        activityLog('View Action History');
        return view('admin.users.user_action_history',$data);
    }

    public function actionHistoryExport($user)
    {
        return Excel::download(new ActivityExport($user), 'Activity.xlsx');
    }

    public function withdrawHistoryExport($user)
    {
        return Excel::download(new WithDrawExport($user), 'Withdraw.xlsx');
    }

    public function withUserSiteSummaryExport()
    {
        return Excel::download(new SummaryExport, 'Withsummary.xlsx');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function status(User $user)
    {
        $status = $user->status == 1 ? 0 : 1;
        $user->update(['status'=>$status]);
        activityLog('Active User');
        return redirect()->back();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function referal(User $user)
    {
        $user_data = User::with('referrals.referralUser')->find($user->id);
        // return Referal::with('referralUser')->get();
        $data = [
            'referrals' => $user_data
        ];
        return view('admin.users.referals',$data);
    }
    public function summary(User $user)
    {
        $trades = Trader_trades::where('user_id',$user->id)->get();
        // $withdraw = Transaction::where('user_id',$user->id)->where('status','Successful')->sum('total_amount');
        $data = [
            'summaries' => $trades,
            // 'withdraw' => $withdraw,
        ];
        return view('admin.users.summary',$data);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(User $user)
    {
        UserBrokerAccount::where('user_id',$user->id)->delete();
        Trader_trades::where('user_id',$user->id)->delete();
        $user->update(['email'=>$user->email.' - deleted']);
        $user->delete();
        activityLog('Delete User');
        return redirect()->back()->with('success','User Deleted Successfully');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function transactiondelete(User $user)
    {
        Trader_trades::where('user_id',$user->id)->delete();
        activityLog('Delete User Transactions');
        return redirect()->back()->with('success','User Transactions Deleted Successfully');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function activityLogs()
    {
        if(Auth::guard('admin')->user()->user_type == 1){
            $data = [
                'activity_logs' =>  ActivityLog::with('user')->orderBy('created_at','DESC')->get()
            ];
        }else{
            $data = [
                'activity_logs' =>  ActivityLog::with('user')->where('user_id',Auth::guard('admin')->user()->id)->orderBy('created_at','DESC')->get()
            ];
        }
        return view('admin.users.activity-logs',$data);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function userSummaryReport()
    {
        $data = [
            'users' =>  User::where('status',1)->get()
        ];
        
        return view('admin.users.user-summary',$data);
    }

     /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function monthlyLevelDown()
    {
        $user_trades = Trader_trades::get()->groupBy('user_id');
        foreach ($user_trades as $user_id => $trades) {
            $user_trade             = User::find($user_id);
            $trading_amount         = Trader_trades::where([['user_id',$user_id]])->pluck('Total_Comm')->toArray();   
            $user_total_earnings    = array_sum($trading_amount);
            
            // if(intval($user_total_earnings) >= 50){
            //     $points = intval($user_total_earnings) / 50;
            //     if(intval($user_total_earnings) == 50){
            //         $points = 1;
            //     }
            // }else{
            //     $points = 0;
            // }  
            
            // if ($points <= getScoringPoints('bronze')['to'])
            // {
            //     $points = getScoringPoints('bronze')['from'];
            // }

            // elseif($points > getScoringPoints('bronze')['to'] && $points < getScoringPoints('silver')['to']){
            //     $points = getScoringPoints('bronze')['to'];

            // }
            // elseif($points > getScoringPoints('silver')['to'] && $points < getScoringPoints('gold')['to']){
            //     $points = getScoringPoints('silver')['from'];
            // }

            // elseif($points > getScoringPoints('gold')['to'] && $points < getScoringPoints('platinum')['to']){
            //     $points = getScoringPoints('silver')['to'];

            // }

            // elseif($points > getScoringPoints('platinum')['to'] && $points < getScoringPoints('diamond')['to']){
            //     $points = getScoringPoints('platinum')['from'];
            // }

            // elseif($points > getScoringPoints('diamond')['to'] && $points < getScoringPoints('emerald')['to']){
            //     $points = getScoringPoints('platinum')['to'];
            // }
            // else
            // {
            //     $points = getScoringPoints('diamond')['from'];

            // }
            // dd($user_trade->cashback_rate,getScoringPercentage('percentage_bronze'),$user_trade->cashback_rate,getScoringPercentage('percentage_silver'));

            if ($user_trade->cashback_rate <= getScoringPercentage('percentage_bronze'))
            {
                $percent = getScoringPercentage('percentage_bronze');
                $points  = 0;
            }

            elseif($user_trade->cashback_rate > getScoringPercentage('percentage_bronze') && $user_trade->cashback_rate <= getScoringPercentage('percentage_silver')){
                $percent = getScoringPercentage('percentage_bronze');
                $points  = getScoringPoints('bronze')['from'];
            }
            elseif($user_trade->cashback_rate > getScoringPercentage('percentage_silver') && $user_trade->cashback_rate <= getScoringPercentage('percentage_gold')){
                $percent = getScoringPercentage('percentage_silver');
                $points  = getScoringPoints('silver')['from'];

            }

            elseif($user_trade->cashback_rate > getScoringPercentage('percentage_gold') && $user_trade->cashback_rate <= getScoringPercentage('percentage_diamond')){
                $percent = getScoringPercentage('percentage_gold');
                $points  = getScoringPoints('gold')['from'];
            }

            elseif($user_trade->cashback_rate > getScoringPercentage('percentage_diamond') && $user_trade->cashback_rate <= getScoringPercentage('percentage_platinum')){
                $percent = getScoringPercentage('percentage_diamond');
                $points  = getScoringPoints('diamond')['from'];
            }

            elseif($user_trade->cashback_rate > getScoringPercentage('percentage_platinum') && $user_trade->cashback_rate <= getScoringPercentage('percentage_emerald')){
                $percent = getScoringPercentage('percentage_platinum');
                $points  = getScoringPoints('platinum')['from'];
            }
            else
            {
                $percent = getScoringPercentage('percentage_emerald');
                $points  = getScoringPoints('emerald')['from'];
            }
            

            $user_trade->update(['cashback_rate' => $percent,'current_points'=>$points]);
            
            foreach ($trades as $key => $trade) {
                $user_total_comm  =   ($trade->Actual_Comm * $percent) / 100;
                $trade->update(['Total_Comm'=>$user_total_comm]);
            }
        }
        
        return redirect()->back()->with('success','Level Down All Users Successfully');
    }
}
