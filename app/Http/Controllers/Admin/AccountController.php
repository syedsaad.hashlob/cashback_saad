<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Admin;
use App\ActivityLog;
use Illuminate\Support\Facades\Storage;
use Hash;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accounts = Admin::get();
        activityLog('View Admin Accounts');
        return view('admin.account.index', compact('accounts'));
    }

    /** 
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.account.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name'                   =>  'required',
            'email'                  =>  'required',
            'password'               =>  'required|confirmed',
            'user_type'               =>  'required',
        ]);
        $data['password'] = Hash::make($data['password']);
        Admin::create($data);
        activityLog('Store Admin Account');
        return redirect()->route('accounts.index')
                        ->with('success','Account created successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $account = Admin::find($id);
        return view('admin.account.edit', compact('account'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admin $account)
    {
        $data = $request->validate([
            'name'                   =>  'required',
            'email'                  =>  'required',
            'password'               =>  'nullable|confirmed',
            'user_type'               =>  'required',
        ]);
        if ($request->password) {
            $data['password'] = Hash::make($data['password']);
        }   
        
        $account->update($data);
        activityLog('Update Admin Account');
        return redirect()->route('accounts.index')
                        ->with('success','Account updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $account = Admin::find($id);
        $account->delete();
        ActivityLog::where('user_id',$id)->delete();
        activityLog('Delete Admin Account');
        return redirect()->route('accounts.index')
                        ->with('success','Account deleted successfully');
    }
}
