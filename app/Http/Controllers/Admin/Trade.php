<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Trader_trades;
use App\Lot_rebate;
use App\Broker;
use App\User;
use App\UserBrokerAccount;
use DB;

class Trade extends Controller
{
    public $data = [];
    public function index(){
        $data = [
            'brokers'   =>  Broker::get()
        ];
        return view('admin.Trade.create',$data);
    }

    public function store(Request $request){

        $points = 0;
        $user_trade = '';
        $new_percent = 0; 
        $file = $request->file('file');
        if ($file) {
            // if ($request->input('types') == 1) {
                $filename = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension(); //Get extension of uploaded file
                $tempPath = $file->getRealPath();
                $fileSize = $file->getSize(); //Get size of uploaded file in bytes
                //Check for file extension and size
                // $this->checkUploadedFileProperties($extension, $fileSize);
                //Where uploaded file will be stored on the server
                $location = storage_path().'/app/uploads'; //Created an "uploads" folder for that
                // Upload file
                $file->move($location, $filename);
                // In case the uploaded file path is to be stored in the database
                $filepath = storage_path("app/uploads/" . $filename);
                // $filepath = storage_path()."/app/uploads/lotRebateReport (1)(1).csv";
                // Reading file
                $file = fopen($filepath, "r");
                $importData_arr = array(); // Read through the file and store the contents as an array
                $i = 0;
                // dd($filepath);
                //Read the contents of the uploaded file
                while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                    $num = count($filedata);
                    // Skip first row (Remove below comment if you want to skip the first row)
                    if ($i == 0) {
                        $i++;
                        continue;
                    }
                    for ($c = 0; $c < $num; $c++) {
                        $importData_arr[$i][] = $filedata[$c];
                    }
                    $i++;
                }
                fclose($file); //Close after reading
                // dd($importData_arr);
                $j = 0;
                $total_comm_count = 0;
                $count = 0;
                
                $collection  =collect($importData_arr);
                $users = $collection->groupBy(1);
                foreach ($users as $key1 => $user) {
                    $user_booker_account = UserBrokerAccount::where([['broker_id',$request->broker_id],['user_account_id',$key1],['approved',1]])->first();
                    if(empty($user_booker_account)){
                        return redirect()->back()->with('error','Could not find account of this broker '.$user[0][1] );
                    }
                    $user_trade = User::find($user_booker_account->user_id);
                    // $percent = ;
                    if($count == 0){
                        if($user_trade->current_points > 0){
                            
                            if ($user_trade->current_points < getScoringPoints('bronze')['to'])
                                $percent = getScoringPercentage('percentage_bronze');
        
                            elseif($user_trade->current_points > getScoringPoints('bronze')['to'] && $user_trade->current_points < getScoringPoints('silver')['to'])
                                $percent = getScoringPercentage('percentage_silver');
        
                            elseif($user_trade->current_points > getScoringPoints('silver')['to'] && $user_trade->current_points < getScoringPoints('gold')['to'])
                                $percent = getScoringPercentage('percentage_gold');
        
                            elseif($user_trade->current_points > getScoringPoints('gold')['to'] && $user_trade->current_points < getScoringPoints('platinum')['to'])
                                $percent = getScoringPercentage('percentage_platinum');
        
                            elseif($user_trade->current_points > getScoringPoints('platinum')['to'] && $user_trade->current_points < getScoringPoints('diamond')['to'])
                                $percent = getScoringPercentage('percentage_diamond');

                            elseif($user_trade->current_points > getScoringPoints('diamond')['to'] && $user_trade->current_points < getScoringPoints('emerald')['to'])
                                $percent = getScoringPercentage('percentage_emerald');
                            else
                                $percent = 90;
                        }else{
                            $percent = getScoringPercentage('percentage_bronze');
                        }
                    }
                    // dd($percent);
                    // dd($user_trade->current_points);

                    foreach ($user as $key => $importData) {
                        // dd($importData);
                        // dd();
                        // $user   = User::where('user_account_id',$importData[1])->first();
                        // dd($user_booker_account->user->parent);
                        // dd($user_booker_account->user->parent_rec);
                        // 
                        // if(!empty($user_booker_account->user->parent_rec)){
                            

                        //     // dd($user_booker_account->user);
                        //     $data_parent = $this->allParentReferals($user_booker_account->user->parent_rec);
                        //     dd($data_parent);
                        // }

                        // foreach ($user_trade->referrals as $key => $referal) {
                            
                        //     dd($referal->parent);
                        // }
                        // $date = \Carbon\Carbon::parse($importData[0])->format('Y-m-d');
                        $date = \Carbon\Carbon::createFromFormat('d/m/Y', $importData[0])->format('Y-m-d');
                        $exist_trade_date = Trader_trades::where([['date',$date],['user_id',$user_trade->id]])->first();
                        // if(empty($exist_trade_date)){
                            // dd($importData);
                            if (strpos($importData[2], '$') !== false)
                            {
                                $total_comm                         =   (double) trim(str_replace('$','',$importData[2]));
                                $actual_comm                         =   (double) trim(str_replace('$','',$importData[2]));
                            }else{
                                $total_comm     = $importData[2];
                                $actual_comm    = $importData[2];
                            }
                            // var_dump($percent);
                            $total_comm                         =   ($total_comm * $percent) / 100;
    
                            $trader_trades                      =   new Trader_trades;
                            $trader_trades->user_id             =   $user_trade->id;
                            $trader_trades->date                =   $date;
                            $trader_trades->MT4_MT5_ID          =   $importData[1];
                            $trader_trades->Total_Comm          =   $total_comm;
                            $trader_trades->Actual_Comm         =   $actual_comm;
                            $trader_trades->status              =   "1";
                            $trader_trades->broker_id           =   $request->broker_id;
                            
                            $trader_trades->save();
                            $total_comm_count += $total_comm;
                            if($key == 1){
                                $trade_acc = $trader_trades->MT4_MT5_ID;
                            }
                            
                            $count++;
                        // }
                    }

                    $pkg_points = $user_trade->current_points;
               
                    $trading_amount         = Trader_trades::where([['user_id',$user_trade->id]])->pluck('Total_Comm')->toArray();   
                    $user_total_earnings    = array_sum($trading_amount);
                    
                    if(intval($user_total_earnings) >= 50){
                        $points = intval($user_total_earnings) / 50;
                        if(intval($user_total_earnings) == 50){
                            $points = 1;
                        }
                    }else{
                        $points = 0;
                    }   

                    // dd($pkg_points);
                    // $user_total_earnings = getUserTotalEarning($user_trade->id);
                    
                    // if(intval($user_total_earnings) >= 50){
                    //     $points = intval($user_total_earnings) / 50;
                    //     if(intval($user_total_earnings) == 50){
                    //         $points = 1;
                    //     }
                    // }else{
                    //     $points = 0;
                    // }
                    // $points = intval($user_total_earnings) >= 50 ? intval($user_total_earnings) == 50 ? 1 : intval($user_total_earnings) / 50 : 0;
    
                    
                    // $points = ` + $pkg_points;
                    $user_trade->update(['current_points' => $points,'cashback_rate' => $percent]);
                    
                    if($points > 0){
                                
                        if ($points < getScoringPoints('bronze')['to'])
                            $new_percent = getScoringPercentage('percentage_bronze');

                        elseif($points > getScoringPoints('bronze')['to'] && $points < getScoringPoints('silver')['to'])
                            $new_percent = getScoringPercentage('percentage_silver');

                        elseif($points > getScoringPoints('silver')['to'] && $points < getScoringPoints('gold')['to'])
                            $new_percent = getScoringPercentage('percentage_gold');

                        elseif($points > getScoringPoints('gold')['to'] && $points < getScoringPoints('platinum')['to'])
                            $new_percent = getScoringPercentage('percentage_platinum');

                        elseif($points > getScoringPoints('platinum')['to'] && $points < getScoringPoints('diamond')['to'])
                            $new_percent = getScoringPercentage('percentage_diamond');

                        elseif($points > getScoringPoints('diamond')['to'] && $points < getScoringPoints('emerald')['to'])
                            $new_percent = getScoringPercentage('percentage_emerald');
                        else
                            $new_percent = 90;
                    }
                    if(isset($new_percent)){
                        if($new_percent > $user_trade->cashback_rate){
                            // $user_trade->update(['cashback_rate' => $new_percent]);

                            
                            $user_trade->update(['cashback_rate' => $new_percent]);

                            $trader_trades =  Trader_trades::where([['user_id',$user_trade->id]])->get();
                            foreach ($trader_trades as $key => $trades) {
                                $user_total_comm  =   ($trades->Actual_Comm * $new_percent) / 100;
                                // dd($user_total_comm);
                                $trades->update(['Total_Comm'=>$user_total_comm]);

                            }
                        }
                    }
                }
                
                // dd($total_comm_count);

                

            // }   
            // else{
            //     $filename = $file->getClientOriginalName();
            //     $extension = $file->getClientOriginalExtension(); //Get extension of uploaded file
            //     $tempPath = $file->getRealPath();
            //     $fileSize = $file->getSize(); //Get size of uploaded file in bytes
            //     //Check for file extension and size
            //     // $this->checkUploadedFileProperties($extension, $fileSize);
            //     //Where uploaded file will be stored on the server
            //     $location = storage_path().'/app/uploads'; //Created an "uploads" folder for that
            //     // Upload file
            //     $file->move($location, $filename);
            //     // In case the uploaded file path is to be stored in the database
            //     $filepath = storage_path("app\uploads\\" . $filename);
            //     // Reading file
            //     $file = fopen($filepath, "r");
            //     $importData_arr = array(); // Read through the file and store the contents as an array
            //     $i = 0;
            //     //Read the contents of the uploaded file
            //     while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
            //         $num = count($filedata);
            //         // Skip first row (Remove below comment if you want to skip the first row)
            //         if ($i == 0) {
            //             $i++;
            //             continue;
            //         }
            //         for ($c = 0; $c < $num; $c++) {
            //             $importData_arr[$i][] = $filedata[$c];
            //         }
            //         $i++;
            //     }
            //     fclose($file); //Close after reading
            //     // dd($importData_arr);
            //     $j = 0;
            //     foreach ($importData_arr as $importData) {
            //         $Lot_rebate = new Lot_rebate;
            //         $Lot_rebate->user_id = "1";
            //         $Lot_rebate->Period = $importData[0];
            //         $Lot_rebate->Clicks = $importData[1];
            //         $Lot_rebate->sub_aff_reg= $importData[2];
            //         $Lot_rebate->Real_Account = $importData[3];
            //         $Lot_rebate->NDAs= $importData[4];
            //         $Lot_rebate->NCR= $importData[5];
            //         $Lot_rebate->UNDC= $importData[6];
            //         $Lot_rebate->TNCR= $importData[7];
            //         $Lot_rebate->TNDC= $importData[8];
            //         $Lot_rebate->Conversion= $importData[9];
            //         $Lot_rebate->APC_Rate= $importData[10];
            //         $Lot_rebate->Active_Traders = $importData[11];
            //         $Lot_rebate->Lots=$importData[12];
            //         $Lot_rebate->Lot_Rebate=$importData[13];
            //         $Lot_rebate->Sub_Aff_Comm=$importData[14];
            //         $Lot_rebate->Adjustments=$importData[15];
            //         $Lot_rebate->Total_Comm=$importData[16];

            //         $Lot_rebate->status = "1";
            //         $Lot_rebate->save();
            //     }
            // }
            activityLog('Import File');
            return redirect()->back()->with('success', " records successfully uploaded");
        } else {
            //no file was uploaded
            throw new \Exception('No file was uploaded', Response::HTTP_BAD_REQUEST);
        }


        // $filename = storage_path().'/app/traderTrades.csv';
        // $delimiter = ',';
        // if (!file_exists($filename) || !is_readable($filename))
        // return false;
        // $header = null;
        // $data = array();
        // if (($handle = fopen($filename, 'r')) !== false)
        // {
        //     while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
        //     {
        //         if (!$header)
        //             $header = $row;
        //         else
        //         {
        //             $data[] = array_combine($header, $row);
        //         }
        //     }
        //     fclose($handle);
        // }
        // dd($data);
        // for ($i = 0; $i < count($data); $i ++)
        // {

        // }
    }

    public function allParentReferals($user){
        if(!empty($user)){
            $this->data = $user->id;
            // $this->allParentReferals($user->parent_rec);
            return $this->data;
        }else{
            return $this->data;
        }
    }

}
