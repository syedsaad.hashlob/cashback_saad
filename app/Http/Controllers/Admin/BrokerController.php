<?php

namespace App\Http\Controllers\Admin;

use App\Broker;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\UserBrokerAccount;

class BrokerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brokers = Broker::get();
        activityLog('View Broker');
        return view('admin.broker.index')->with('brokers',$brokers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'isEdit'    =>  false,
        ];
        return view('admin.broker.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name'          =>  'required',
            'description'   =>  'required',
            'image'         =>  'required',
            'web_link'      =>  'required',
        ]);
        if ($request->image) {
            $data['image']  = Storage::disk('uploads')->putFile('',$request->image);
        }

        Broker::create($data);
        activityLog('Add Broker');
        return redirect()->route('broker.index')
                        ->with('success','Broker created successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function edit(Broker $broker)
    {
        $data = [
            'isEdit'    =>  true,
            'broker'    =>  $broker,
        ];
        return view('admin.broker.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function update(Request $request, Broker $broker)
    {
        $data = $request->validate([
            'name' => 'required',
            'description' => 'required',
            'image' => 'nullable',
            'web_link' => 'required',
        ]);

        if($request->image){
            Storage::disk('uploads')->delete($broker->image);
            $data['image']  = Storage::disk('uploads')->putFile('',$request->image);
        }
        $broker->update($data);
        activityLog('Update Broker');
        return redirect()->route('broker.index')->with('success','Broker updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(broker $broker)
    {
        Storage::disk('uploads')->delete($broker->image);
        $broker->delete();
        activityLog('Delete Broker');
        return redirect()->route('broker.index')
                        ->with('success','Product deleted successfully');
    }

    public function verifyBooker(){
        
        $unverify_users = UserBrokerAccount::with('user','broker')->get();
        $data = [
            'unverify_users' => $unverify_users
        ];
        return view('admin.broker.verify_user',$data);   
    }

    public function approvedBookerAccount(UserBrokerAccount $user_account){
        
        $status = $user_account->approved == 0 ? "1" : "0";
     
        $user_account->update(['approved' => $status]);
        activityLog('Approved User Account');
        return redirect()->back();
    }

}