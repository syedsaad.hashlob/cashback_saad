<?php

namespace App\Http\Controllers\admin\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Referal;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**9
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'account_id' => ['nullable'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        // dd($data);
        $user_detail = [
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ];
        
        if($data['account_id']){
            // dd("asdsdfad");
            $referal_user = User::where('referral',$data['account_id'])->first();
            $user_detail['p_id'] = $referal_user->id;
            $user =  User::create($user_detail);
            Referal::create([
                'referral'            => $data['account_id'],
                'user_account_id'     => $user->id,
                'referral_account_id' => $referal_user->user_account_id,
            ]);
        }else{
            $user_detail['user_account_id']  = rand(1000000,9999999);
            $user_detail['referral']    = Hash::make($user_detail['user_account_id']);

            $user =  User::create($user_detail);
            Mail::send('email/welcome-email', ['data'=>$user ], function ($message) {
                $message->to($user->email)
                    ->from('info@fxcashbacks.com','FXCASHBACKS')
                    ->subject("Welcome to FxCashbacks");
            });
        }
        return $user; 
    }
}
