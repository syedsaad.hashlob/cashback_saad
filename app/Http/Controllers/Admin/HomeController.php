<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Broker;
use App\Transaction;
use App\Trader_trades;
use App\UserBrokerAccount;
use App\User;
use App\Setting;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $total_users = User::count();
        $trading_amount         =    Trader_trades::get()->pluck('Total_Comm')->toArray();
        $total_trading_amount   =    array_sum($trading_amount);
        $brokers                =    Broker::count();
        $recent_trading         =    Trader_trades::orderBy('created_at','desc')->get()->groupBy('MT4_MT5_ID');
        $trading_trade          =    array_keys($recent_trading->toArray());
        $user_brokers           =    UserBrokerAccount::with('user')->whereIn('user_account_id',$trading_trade)->get()->groupBy('user_id');
        $user_ids               =    array_keys($user_brokers->toArray());
        $users                  =    User::whereIn('id',$user_ids)->get();
        // dd($user_ids);

        $data = [
            'total_brokers'             =>  $brokers,
            'total_users'               =>  $total_users,
            'total_trading_ammount'     =>  $total_trading_amount,
            'recent_users'              =>  $users,
        ];
        return view('admin.home',$data);
    }

    public function transaction(){
        
        $data = [
            'transactions' => Transaction::get(),
        ];
        activityLog('View Transactions');
        return view('admin.transaction.index',$data);
    }

    public function Setting(){
        $data = [
            'settings' => Transaction::with('user','broker','paymentMethod')->get(),
        ];
        activityLog('View Settings');
        return view('admin.setting.setting',$data);
    }

    public function settingUpdate(Request $request)
    {
        $input = $request->except('_token');
        // return $input;
        foreach ($input['points'] as $key => $value) {
            $setting = Setting::where('key',$key)->first();
            if(empty($setting))
            {
                $setting = new Setting;
                $setting->key = $key;   
            }
            // return $value;
            $setting->value = json_encode($value);
            $setting->save();
        }
        foreach ($input['percentage'] as $key2 => $value2) {
            $setting_per = Setting::where('key',$key2)->first();
            if(empty($setting_per))
            {
                $setting_per = new Setting;
                $setting_per->key = $key2;   
            }
            $setting_per->value = $value2;
            $setting_per->save();
        }
        activityLog('Update Settings');
        return redirect()->back();
    }
}
 