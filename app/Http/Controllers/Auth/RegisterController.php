<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */



    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user_detail = [
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ];
        
        if(isset($data['account_id'])){
            // dd("asdsdfad");
            $referal_user = User::where('referral',$data['account_id'])->first();
            $user_detail['p_id'] = $referal_user->id;
            $user_detail['cashback_rate'] = getSetting('percentage_bronze');
            $user =  User::create($user_detail);
            Referal::create([
                'referral'            => $data['account_id'],
                'user_account_id'     => $user->id,
                'referral_account_id' => $referal_user->user_account_id,
            ]);
        }else{
            $user_account_id = User::orderBy('user_account_id','desc')->first();
            if(empty($user_account_id)){
                $incre_id = "1000001";    
            }else{
                $incre_id = $user_account_id->user_account_id + 1;
            }
            // dd($incre_id);
            $user_detail['cashback_rate'] = getSetting('percentage_bronze');
            $user_detail['user_account_id']  = $incre_id;
            $user_detail['referral']    = Hash::make($user_detail['user_account_id']);
            $user =  User::create($user_detail);
            Mail::send('email/welcome-email', ['data'=>$user ], function ($message) use ($user) {
                $message->to($user->email)
                    ->from('info@fxcashbacks.com','FXCASHBACKS')
                    ->subject("Welcome to FxCashbacks");
            });
        }
        return $user; 
    }
}
