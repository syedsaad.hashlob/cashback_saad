<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Broker;

class WelcomeController extends Controller
{
    public function index(){
        $broker = Broker::get();
        return view('welcome')->with('broker', $broker);
    }
}
