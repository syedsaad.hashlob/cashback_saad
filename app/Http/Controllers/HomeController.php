<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Referal;
use App\Trader_trades;
use App\Broker;
use App\User;
use App\Transaction;
use Carbon\Carbon;
use App\UserBrokerAccount;

class HomeController extends Controller

{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $count_data = 0;
        
        $trading_brokers        =    Trader_trades::get()->groupBy('broker_id');
        $trading_brokers        =    array_keys($trading_brokers->toArray());
        // $brokers                =    Broker::whereIn('id',$trading_brokers)->get();
        $referrals_id           =    Auth::user()->referrals->pluck('user_account_id')->toArray();
        $user_account_id        =    UserBrokerAccount::where('user_id',Auth::user()->id)->get()->pluck('user_account_id')->toArray();
        $referal_account_id     =    UserBrokerAccount::whereIn('user_id',$referrals_id)->get()->pluck('user_account_id')->toArray();
        $Trader_trades          =    Trader_trades::whereIn('MT4_MT5_ID', $user_account_id)->get();
        // dd($user_account_id);
        // dd($Trader_trades);
        foreach($Trader_trades as $trade){
            // dd($trade);
            $count_data += (float) $trade['Total_Comm'];
            // $count_data += (float)$int_trade[1];
        }
        $trading_referrals      =    Trader_trades::whereIn('MT4_MT5_ID',$user_account_id)->get()->pluck('Total_Comm')->toArray();
        $sum_trading_referrals  =    array_sum($trading_referrals);
        $transaction_amount     =    Transaction::where([['user_id',Auth::user()->id],['status','pending']])->get()->pluck('total_amount')->toArray();
        $transaction_amount     =    array_sum($transaction_amount);

        // $sum_trading_referrals = ($sum_trading_ref * $percent) / 100;
        // dd($count_data);
        // 
        $transaction_amoount = Transaction::where([['user_id',Auth::user()->id],['status','Successfull']])->get()->pluck('total_amount')->toArray(); 
        $transaction_amoount = array_sum($transaction_amoount);

        $total_balance_minus_trasaction = $sum_trading_referrals - $transaction_amoount;
        $brokers                =    UserBrokerAccount::with('user','broker')->where([['user_id',Auth::user()->id],['approved',1]])->get();
        $data = [
            'Trader_trades'     =>  $Trader_trades ,
            'trade_cont_value'  =>  $total_balance_minus_trasaction,
            'brokers'           =>  $brokers,
            'pending_withdraw'  =>  $transaction_amount,
            'referrals_id'      =>  count($referrals_id) + 6
        ];
        // return Auth::user();
        return view('home',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function Setting()
    {
        $data = [
            'setting' => Setting::get(),
        ];

        return view('admin.setting.setting', $data);
    }
    
    public function settingUpdate(Request $request)
    {
        $input = $request->except('_token');
        foreach ($input as $key => $value) {
            $setting = Setting::where('key',$key)->first();
            if(empty($setting))
            {
                $setting = new Setting;
                $setting->key = $key;   
            }
            $setting->value = $value;
            $setting->save();
        }

        return redirect()->back();
    }
}
