<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Broker;
use App\PaymentMethod;

class BrokerWebController extends Controller
{
    public function index(Broker $broker)
    {
        $data =  [
            'broker'            => $broker,
            'payment_methods'   => PaymentMethod::where('status',1)->get()
        ];
        
        return view('broker',$data);
    }
}
