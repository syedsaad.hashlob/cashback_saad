<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Broker;
use App\Transaction;
use Auth;

class MainpageController extends Controller
{
    public function index(){
        $data = [
            'brokers' => Broker::where('status',1)->get(),
        ];
        return view('mainpage',$data);
    }

    public function transactionStore(Request $request){

        $request->validate([
            'payment_method_id' => 'required',
            'amount'            => 'required',
        ]);
        $data = [
            'user_id'               => Auth::user()->id,             
            'payment_method_id'     => $request->payment_method_id,
            'amount'                => $request->amount ,
            'status'                => 'Pending'
        ];
        if($request->broker_id){
            $data['broker_id'] = $request->broker_id;
        }
        $user_total_earning = getUserTotalEarning(Auth::user()->id);
        
        if($user_total_earning < $request->amount){
            return redirect()->back()->with('error','Insufficient Balance');
        }elseif ($request->amount < getSetting('min_withdraw_amount')) {
            return redirect()->back()->with('error','you can minimum withdraw '.getSetting('min_withdraw_amount'));
        }
        $data['transaction_fee'] = getSetting('transaction_fee_percentage');
        $percent_amount = $request->amount * (getSetting('transaction_fee_percentage') / 100);
        $data['total_amount']    = $request->amount + $percent_amount;
        Transaction::create($data);
        $mail_data = [
            'name' => Auth::user()->name,    
            'request_amount' => $request->amount,    
            'date' => \Carbon\Carbon::now()->format('Y-m-d h:i a'),    
        ];
        $user = Auth::user();
        \Mail::send('email/withdrawl-request-email', ['data'=>$mail_data ], function ($message) use ($user) {
            $message->to($user->email)
            // $message->to('shehryarhaider0316@gmail.com')
                ->from('accounts@fxcashbacks.com','FXCASHBACKS')
                ->subject("Welcome to FxCashbacks");
        });
        return redirect()->back()->with('success','Withdraw request sent');
    }

    public function withDrawRequestStatus (Request $request){
        // return $request->all();
        $transaction = Transaction::find($request->id);
        $transaction->update(['status'=>$request->status]);
        $user = \App\User::find($transaction->user_id);
        if($request->status == "Successfull"){
            $mail_data = [
                'name' => $user->name,    
                'confirm_amount' => $transaction->amount,    
                'date' => \Carbon\Carbon::now()->format('Y-m-d h:i a'),    
            ];
            \Mail::send('email/withdrawl-confirm-email', ['data'=>$mail_data ], function ($message) use ($user) {
                $message->to($user->email)
                // $message->to('shehryarhaider0316@gmail.com')
                    ->from('accounts@fxcashbacks.com','FXCASHBACKS')
                    ->subject("Welcome to FxCashbacks");
            });
        }
        return response()->json([
            'success' => 'Success',
            'message'   =>  'Status Updated Successfully'
        ]);
        // return redirect()->back()->with('success','Request Submitted Successfully, Please check you email for more info');
    }

}


