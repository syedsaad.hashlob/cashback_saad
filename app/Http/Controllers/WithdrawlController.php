<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PaymentMethod;

class WithdrawlController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'payment_methods'   =>  PaymentMethod::where('status',1)->get()
        ];
        return view('dashboard.withdraw',$data);
    }
}
