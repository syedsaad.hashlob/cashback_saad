<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Referal; 
use Auth;

class ReferedUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'refered_users' => Referal::with('referralUser')->where('referral',Auth::user()->referral)->get(), 
        ];
        return view('dashboard.refered_user',$data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function referredView($account_id)
    {
        $data = [
            'account_id' => $account_id
        ];
        return view('auth.register',$data);
    }

}
