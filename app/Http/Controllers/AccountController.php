<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\UserBrokerAccount;
use App\Trader_trades;
use App\Broker;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $trading_brokers        =    Trader_trades::get()->groupBy('broker_id');
        $trading_brokers        =    array_keys($trading_brokers->toArray());
        // $brokers                =    Broker::whereIn('id',$trading_brokers)->get();
        $brokers                =    UserBrokerAccount::with('user','broker')->where([['user_id',Auth::user()->id],['approved',1]])->get();
        // $brokers=  $brokers->unique('broker_id');
        $data = [
            'brokers'   => $brokers
        ];
        return view('dashboard.myaccount',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateBrokerAccountId(Request $request)
    {
        $data = $request->validate([
            'user_account_id' => 'required',
            'broker_id'  => 'required'
        ]);
        $data['user_id'] = Auth::user()->id;
        $userbrokeraccount = UserBrokerAccount::where([['broker_id',$request->broker_id],['user_id',Auth::user()->id]])->first();
        // if(empty($userbrokeraccount)){
        UserBrokerAccount::create($data);
            // Auth::user()->update(['user_account_id'=>$request->account_id]);
        return redirect()->back()->with('success','Account Added Successfully');        
        // }
        // return redirect()->back()->with('error','Already Registered');
    }

    public function popUpStatusUpdate(Request $request){
        Auth::user()->update(['popup_visited'=>1]);
        return response()->json([
            'success' => 'Success',
            'message'   =>  'Status Updated Successfully'
        ]);
    }
}
