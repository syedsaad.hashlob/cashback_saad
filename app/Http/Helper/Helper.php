<?php
use App\Trader_trades;
use App\Referal;
use App\User;
use App\Broker;
use App\UserBrokerAccount;
use App\Setting;
use App\Transaction;
use App\ActivityLog;

function getBrockerByIdAndAccountId($account_id, $broker_id)
{
    $trading = Trader_trades::with('broker')->where([['MT4_MT5_ID',$account_id],['broker_id',$broker_id]])->get();
    $sum_all_amount = array_sum($trading->pluck('Total_Comm')->toArray());
    // dd($sum_all_amount);
    $data = [
        'trading'       =>  $trading->first(),
        'sum_amount'    =>  $sum_all_amount,
    ];

    return $data;
}

function getUserTotalEarning($user_id){

    $user_account_ids = UserBrokerAccount::where('user_id',$user_id)->get()->pluck('user_account_id')->toArray();
    $trading = Trader_trades::whereIn('MT4_MT5_ID',$user_account_ids)->get();
    $sum_all_amount = array_sum($trading->pluck('Total_Comm')->toArray());
    
    $transaction_amoount = Transaction::where([['user_id',$user_id],['status','Successfull']])->get()->pluck('total_amount')->toArray(); 
    $transaction_amoount = array_sum($transaction_amoount);
    // dd($sum_all_amount);
    return $sum_all_amount - $transaction_amoount;
}

function getReferalTotalEarning($referal){

    // $user_account_id    = Referal::where('referral',$referal)->get()->pluck('user_account_id');
    $user_id            =   Referal::where('referral',$referal)->get()->pluck('user_account_id');
    $user_account_ids   =   UserBrokerAccount::whereIn('user_id',$user_id)->get()->pluck('user_account_id')->toArray();

    // $user_account_id    =   User::whereIn('id',$user_id)->get()->pluck('user_account_id');   
    // dd($user_accounts);
    
    $sum_referal_amount = Trader_trades::whereIn('MT4_MT5_ID',$user_account_ids)->get()->pluck('Total_Comm')->toArray();
    $sum_referal_amount = array_sum($sum_referal_amount);
    return $sum_referal_amount;
}

function getBrokers(){
    return Broker::where('status',1)->get();
}

function getScoringPoints($type){

    $points =   Setting::where('key',$type)->first();
    $points =   json_decode($points->value);
    $data   = [
        'from'   =>  (int)$points[0] ?? '',
        'to'     =>  (int)$points[1] ?? '',
    ];
    return $data;
}

function getScoringPercentage($type){

    $percentage =   Setting::where('key',$type)->first();
    
    return $percentage->value;
}

function getSetting($key){
    return Setting::where('key',$key)->first()->value;
}

function activityLog($action){
    
    $data = [
        'action'    => $action,
        'user_id'   => Auth::guard('admin')->user()->id,
    ];  
    ActivityLog::create($data);
    return true;
}

function brokerWiseAmount($user_id, $user_account_id, $broker_id){

    $trading_amount = Trader_trades::where([['user_id',$user_id],['MT4_MT5_ID',$user_account_id],['broker_id',$broker_id]])->pluck('Total_Comm')->toArray();   
    return array_sum($trading_amount);
}

function brokerWisePoints($user_id, $user_account_id, $broker_id){

    $trading_amount         = Trader_trades::where([['user_id',$user_id],['MT4_MT5_ID',$user_account_id],['broker_id',$broker_id]])->pluck('Total_Comm')->toArray();   
    $user_total_earnings    = array_sum($trading_amount);
    
    if(intval($user_total_earnings) >= 50){
        $points = intval($user_total_earnings) / 50;
        if(intval($user_total_earnings) == 50){
            $points = 1;
        }
    }else{
        $points = 0;
    }   
    return $points;
}

function currentPoints($user_id){
    $trading_amount         = Trader_trades::where([['user_id',$user_id]])->pluck('Total_Comm')->toArray();   
    $user_total_earnings    = array_sum($trading_amount);
    
    if(intval($user_total_earnings) >= 50){
        $points = intval(intval($user_total_earnings) / 50);
        if(intval($user_total_earnings) == 50){
            $points = 1;
        }
    }else{
        $points = 0;
    }  
    return $points;
}

function getUserTransactionAmount($user_id){
    $transaction_trading_amount         = Trader_trades::where([['user_id',$user_id]])->pluck('Actual_Comm')->toArray();   
    return array_sum($transaction_trading_amount);
}

function getUserEarningAmount($user_id){
    $earning_trading_amount         = Trader_trades::where([['user_id',$user_id]])->pluck('Total_Comm')->toArray();   
    return array_sum($earning_trading_amount);
}