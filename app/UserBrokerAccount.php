<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserBrokerAccount extends Model
{
    use SoftDeletes;
    
    protected $table = 'user_broker_accounts';
    protected $fillable = ['user_id','broker_id','user_account_id','approved'];

    /**
     * Get the user associated with the UserBrokerAccount
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * Get the user associated with the UserBrokerAccount
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function broker()
    {
        return $this->hasOne(Broker::class, 'id', 'broker_id');
    }


}
