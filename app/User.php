<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','referral','current_points','user_account_id','cashback_rate','status','popup_visited'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get all of the referrals for the Referal
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function referrals()
    {
        return $this->hasMany(Referal::class, 'referral', 'referral');
    }

    /**
     * Get the parentReferal associated with the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function userReferal()
    {
        return $this->hasOne(Referal::class, 'user_account_id', 'id');
    }

      
    // parent
    public function parent()
    {
        return $this->morphMany(User::class,'p_id');
    }

    // all ascendants
    public function parent_rec()
    {
            return $this->parent()->with('parent_rec');
    }

    /**
     * Get all of the userBookerAccount for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userBookerAccount()
    {
        return $this->hasMany(UserBrokerAccount::class, 'user_id', 'id');
    }

    /**
     * Get all of the tradings for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tradings()
    {
        return $this->hasMany(Trader_trades::class, 'user_id', 'id');
    }

    /**
     * Get all of the tradings for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function withdraw()
    {
        return $this->hasMany(Transaction::class, 'user_id', 'id')->where('status','Successfull');
    }
}
