<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Trader_trades extends Model
{
    use SoftDeletes;
    
    protected $table = 'trader_trades';
    public $fillable = ['user_id','date','Trade','MT4_MT5_ID','Total_Comm','Actual_Comm','broker_id','status'];

    /**
     * Get the broker associated with the Trader_trades
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function broker()
    {
        return $this->hasOne(Broker::class, 'id', 'broker_id');
    }

    /**
     * Get the broker associated with the Trader_trades
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * Get the broker associated with the Trader_trades
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function userBrokerAccount()
    {
        return $this->hasOne(UserBrokerAccount::class, 'user_account_id', 'MT4_MT5_ID');
    }

    /**
     * Get the parentReferal associated with the Trader_trades
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function referal()
    {
        return $this->hasOne(Referal::class, 'referral_account_id', 'MT4_MT5_ID');
    }
}
